/*
	File:
		Scanner.h
	Description:
		Abstract base class to control scanners
	Copyright:
		Orisol Asia Ltd.
*/
#pragma once
#include <cstdint>
#include <string>
#include <vector>
#include <Poco/DynamicAny.h>
#include "Point3dUtils.hpp"

// Default values for Scanner parameter/recipe
#define DEFAULT_ENCODERSPACING 0.3
#define DEFAULT_EXPOSUREMODE 0
#define DEFAULT_EXPOSURE 60
#define DEFAULT_EXPOSURESTEPONE 30
#define DEFAULT_EXPOSURESTEPTWO 300
#define DEFAULT_DYNAMICEXPOSUREMIN 20
#define DEFAULT_DYNAMICEXPOSUREMAX 2000
#define DEFAULT_ENCODERTRIGGERSKIPPINGCOUNT 15
#define DEFAULT_CMOSSENSITIVITY 0
#define DEFAULT_EXPOSURETIME 2
#define DEFAULT_PEAKDETECTIONLEVEL 4
#define DEFAULT_PEAKSELECTION 0
#define DEFAULT_PEAKWIDTHFILTER 0

// Systemwise settings
#define DEFAULT_X_MAX 120
#define DEFAULT_X_MIN -120
#define DEFAULT_Y_MAX 410
#define DEFAULT_Y_MIN 0

// frame rate in Hz
#define DEFAULT_MAXFRAMERATE 400

#define INVALID_Z_FLOOR ((double)-200.0)

enum class ScanParameter : uint8_t
{
	ScanMode = 0,
	TriggerSource, 
	MaxFrameRate,
	EncoderTriggerMode,
	EncoderSpacing, 
	EncoderSpacingLimitMin,
	EncoderSpacingLimitMax,
	SpacingIntervalUsed,
	SpacingIntervalType, 
	SpacingInterval,
	SpacingIntervalLimitMin,
	SpacingIntervalLimitMax,
	XSubsamplingOptionCount,
	XSubsampling, 
	ZSubsamplingOptionCount,
	ZSubsampling,
	ExposureModeOptionCount,
	ExposureMode,
	Exposure,
	ExposureLimitMax,
	ExposureLimitMin,
	ExposureAddStep,
	ExposureClearStep,
	ExposureStepCount,
	ExposureStepOne,
	ExposureStepTwo,
	DynamicExposureMax,
	DynamicExposureMin,

//-----------------------
	XGapFillingUsed,
	XGapFillingEnabled,
	XGapFillingWindowLimitMin,
	XGapFillingWindowLimitMax,
	XGapFillingWindow,

	YGapFillingUsed,
	YGapFillingEnabled,
	YGapFillingWindowLimitMin,
	YGapFillingWindowLimitMax,
	YGapFillingWindow,

	XSmoothingUsed,
	XSmoothingEnabled,
	XSmoothingWindowLimitMin,
	XSmoothingWindowLimitMax,
	XSmoothingWindow,

	YSmoothingUsed,
	YSmoothingEnabled,
	YSmoothingWindowLimitMin,
	YSmoothingWindowLimitMax,
	YSmoothingWindow,

	XDecimationUsed,
	XDecimationEnabled,
	XDecimationWindowLimitMin,
	XDecimationWindowLimitMax,
	XDecimationWindow,

	YDecimationUsed,
	YDecimationEnabled,
	YDecimationWindowLimitMin,
	YDecimationWindowLimitMax,
	YDecimationWindow,

	XMedianUsed,
	XMedianEnabled,
	XMedianWindowLimitMin,
	XMedianWindowLimitMax,
	XMedianWindow,

	YMedianUsed,
	YMedianEnabled,
	YMedianWindowLimitMin,
	YMedianWindowLimitMax,
	YMedianWindow,

//-------------------------
	ActiveAreaHeightLimitMin,
	ActiveAreaHeightLimitMax,
	ActiveAreaHeight,
	
	ActiveAreaLengthLimitMin,
	ActiveAreaLengthLimitMax,
	ActiveAreaLength,

	ActiveAreaWidthLimitMin,
	ActiveAreaWidthLimitMax,
	ActiveAreaWidth,

	ActiveAreaXLimitMin,
	ActiveAreaXLimitMax,
	ActiveAreaX,

	ActiveAreaYLimitMin,
	ActiveAreaYLimitMax,
	ActiveAreaY,

	ActiveAreaZLimitMin,
	ActiveAreaZLimitMax,
	ActiveAreaZ,

	GenerationType,		
	FixedLengthStartTrigger,	
	FixedLengthLength,
	FixedLengthLengthLimitMax,
	FixedLengthLengthLimitMin,

//------- for simulator ------------
	ScanFromFile,

//------- for Keyence --------------
	OperationMode,
	SamplingFrequency,
	EncoderTriggerSkipping,
	EncoderTriggerSkippingCount,
	CMOSSensitivity,
	ExposureTime,
	ImagingMode,
	PeakDetectionLevel,
	InvalidDataInterpolationCount,
	PeakSelection,
	PeakWidthFilter
};

// Exposure Modes: Single, Multiple, Dynamic (as defined in GoSDK)
enum class ExposureMode : uint8_t
{
	Single = 0,
	Multiple = 1,
	Dynamic = 2
};

// CMOSSensitivity: High Precision, High Dynamic Range 1, High Dynamic Range 2, High Dynamic Range 3
enum class CMOSSensitivity : uint8_t
{
	High_Precision = 0,
	High_Dynamic_Range1 = 1,
	High_Dynamic_Range2 = 2,
	High_Dynamic_Range3 = 3
};

// ExposureTime: 15us, 30us, 60us, 120us, 240us, 480us, 960us, 1920us, 5ms, 10ms
enum class ExposureTime : uint8_t
{
	ET15us = 0,
	ET30us = 1,
	ET60us = 2,
	ET120us = 3,
	ET240us = 4,
	ET480us = 5,
	ET960us = 6,
	ET1920us = 7,
	ET5ms = 8,
	ET10ms = 9
};

// PeakDetectionLevel: 1, 2, 3, 4, 5
enum class PeakDetectionLevel : uint8_t
{
	Level1 = 1,
	Level2 = 2,
	Level3 = 3,
	Level4 = 4,
	Level5 = 5
};

// PeakSelection: Standard, Near, Far, Remove X MultiReflection, Remove Y MultiReflection, Make Invalid Data
enum class PeakSelection : uint8_t
{
	Standard = 0,
	Near = 1,
	Far = 2,
	Remove_X_MultiReflection = 3,
	Remove_Y_MultiReflection = 4,
	Make_Invalid_Data = 5
};

// PeakWidthFilter: Off, On
enum class PeakWidthFilter : uint8_t
{
	Off = 0,
	On = 1
};

// Encoder Trigger Input Mode: single phase 1 time, dual phase 1 time, dual phase 2 times, dual phase 4 times (as defined in Keyence LJV7 SDK)
enum class EncoderTriggerInput : uint8_t
{
	SinglePhase1X = 0,
	DualPhase1X = 1,
	DualPhase2X = 2,
	DualPhase4X = 3
};

// ScannerResult code -976 ~ -1000 are from kStatus of Gocator SDK, see kApiDef.h for kERROR
// NoError and ERR_General are also compatible to Gocator SDK.
enum class ScannerResult : int32_t
{
	NoError = 1,
	ERR_General = 0,
	ERR_IsConnect = -1,
	ERR_ReadFile = -2,
	ERR_WriteFile = -3,
/************************ Reused from Gocator SDK *******************************/
	ERR_InProgress = -976,     // Operation is in progress, but not yet complete.
	ERR_Full = -977,           // Resource is already fully utilized.
	ERR_Device = -978,         // Hardware device error.
	ERR_OS = -979,             // Generic error reported by underlying OS.
	ERR_Conflict = -980,       // State conflicts with another object.
	ERR_Busy = -981,           // Agent is busy (cannot service request).
	ERR_WriteOnly = -982,      // Object is write-only (cannot be read). 
	ERR_ReadOnly = -983,       // Object is read-only (cannot be written).
	ERR_Format = -984,         // Data parsing/formatting error.
	ERR_Heap = -985,           // Heap error (leak/double-free).
	ERR_Network = -986,        // Network setup/resource error.
	ERR_AlreadyExists = -987,  // Conflicts with existing item.
	ERR_Abort = -988,          // Operation aborted.
	ERR_Version = -989,        // Incompatible version.
	ERR_Closed = -990,         // Resource is no longer available. 
	ERR_Stream = -991,         // Error in stream.
	ERR_Incomplete = -992,     // Buffer insufficient for data.
	ERR_Timeout = -993,        // Action timed out.
	ERR_Memory = -994,         // Out of memory.
	ERR_Unimplemented = -996,  // Feature not implemented.
	ERR_Parameter = -997,      // Parameter is invalid.
	ERR_Command = -998,        // Command not recognized.
	ERR_NotFound = -999,       // Item is not found.
	ERR_State = -1000,         // Invalid state.
/************************ Reused from Keyence *******************************/
	ERR_OPEN = -4096,			// Failed to open the communication path
	ERR_NOT_OPEN = -4097,		// The communication path was not established.
	ERR_SEND = -4098,			// Failed to send the command.
	ERR_RECEIVE	= -4099,		// Failed to receive a response.
	ERR_TIMEOUT	= -4100,		// A timeout occurred while waiting for the response.
	ERR_NOMEMORY = -4101,		// Failed to allocate memory.
	ERR_PARAMETER = -4102,		// An invalid parameter was passed.
	ERR_RECV_FMT = -4103,		// The received response data was invalid
	ERR_HISPEED_NO_DEVICE = -4105,	// High-speed communication initialization could not be performed.
	ERR_HISPEED_OPEN_YET = -4106,	// High-speed communication was initialized.
	ERR_HISPEED_RECV_YET = -4107,	// Error already occurred during high-speed communication (for high-speed communication)
	ERR_BUFFER_SHORT = -4108	// The buffer size passed as an argument is insufficient. 
};

struct DataContext
{
	double xOffset;
	double yOffset;
	double zOffset;
	double xResolution;
	double yResolution;
	double zResolution;
	size_t profileWidth;  // number of point per profile
	size_t profileLength; // number of profile
};

class DataCollector
{
public:
	virtual uint32_t getProfileCount() = 0;
	virtual void operator() (std::vector<double>, DataContext) = 0;
	virtual void operator() (std::vector<Point3d>, DataContext) = 0;
};

class Scanner
{
public:
	virtual ~Scanner() {}
	virtual ScannerResult version(std::string & version) = 0;
	virtual ScannerResult initialize(Poco::DynamicAny & address, uint32_t deviceid = 0) = 0;
	virtual ScannerResult connect() = 0;
	virtual ScannerResult disconnect() = 0;
	virtual ScannerResult reset() = 0;
	virtual ScannerResult calibrate(Poco::DynamicAny exposure) = 0;
	virtual ScannerResult setDefaultParameter(std::string file = "") = 0;
	virtual ScannerResult setParameter(ScanParameter parameter, Poco::DynamicAny value) = 0;
	virtual ScannerResult getParameter(ScanParameter parameter, Poco::DynamicAny & value) = 0;
	virtual ScannerResult startScan(DataCollector & collector) = 0;
	virtual ScannerResult startScanAsync(DataCollector & collector) = 0;
	virtual ScannerResult stopScan() = 0;
};
