/*
    File:
        Vision.h
    Description:
        Vision algorithm related parameters and interface
    Copyright:
        Orisol Asia Ltd.
*/
#pragma once
#include <cstdint>

// default recipe values
#define DEFAULT_PATHTYPE 0x0D
#define DEFAULT_TOEHEELSECTIONLENGTH 30.0
#define DEFAULT_SPIKENOISESUPPRESSION true
#define DEFAULT_VIEWHEIGHT 0.0
#define DEFAULT_FEATURETHRESHOLD 0.7
#define DEFAULT_NUMBEROFDOWNSAMPLEPOINT 70
#define DEFAULT_SPRAYRADIUS1ST 3.0
#define DEFAULT_SPRAYRADIUSOTHER 7.5
#define DEFAULT_FILLHOLESWITHFIT true
#define DEFAULT_SMOOTHFACTOR 100.0
#define DEFAULT_ZTHRESHOLD 5.0
#define DEFAULT_CENTERNONEDGEWIDTH 30.0
#define DEFAULT_PATHORDER 0x11
#define DEFAULT_OVERLAPPOINTSOFLAYER1 2
#define DEFAULT_HOLEFILLLAMBDA 2.8284271247461900976033774484194
#define DEFAULT_XSECTIONARCHPITCH 1.2
#define DEFAULT_CONVEYOR_PLANE_Z_THRESHOLE 5
#define DEFAULT_CONVEYOR_PLANE_Z_THRESHOLE_FOR_THIN_SOLE 3

enum class VisionRecipe : uint8_t
{
    // these parameters are open to user to set from MMI
    PathType = 0,
    ToeHeelSectionLength,
    SpikeNoiseSuppression,
	ThinSole,
    ViewHeight,
    FeatureThreshold,
    NumberOfDownSamplePoint,
    LambdaX,                 // NOT USED
    LambdaY,                 // NOT USED
    LambdaZ,                 // NOT USED
    SprayRadius1st,
    SprayRadiusOther,
    SprayAngle,              // NOT USED
    NumberOfAngleLayer,      // NOT USED
    FillHolesWithFit,
    SmoothFactor,
	ToeCutting,
	FilterWideWall,
	AutoPoints,
	EdgeSprayCycles, // 0: auto, 1~5 cycles
	AutoViewHeight,
	AutoToeCutting,
	AutoSprayAngle,
    // Following parameters are not open to users
    CenterNonEdgeWidth,
    OverlapPointsOfLayer1,
    PathOrder,
	// ------------------------------------------
	ToeCuttingDir
};

enum class PathOrder : uint8_t
{
    Clockwise = 0x00,
    FromSec1 = 0x01,
    FromSec2 = 0x02,
    FromSec3 = 0x04,
    FromSec4 = 0x08,
    CounterClockwise = 0x10,
    FromSec1CCW = 0x11,
    FromSec2CCW = 0x12,
    FromSec3CCW = 0x14,
    FromSec4CCW = 0x18
};
