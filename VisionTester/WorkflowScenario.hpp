/*
    File:
        WorkflowScenario.hpp
    Description:
        Definition of workflow messages exchanged between Workers
        This file is worker part of OMLP v1.0 and should be shared over all worker projects
    Copyright:
        Orisol Asia Ltd.
*/
#pragma once
#include <cstdint>
#include <vector>
#include <algorithm>
#include <unordered_map>
#include <utility>
#include <zmq_addon.hpp>
#include <Poco/DynamicAny.h>
#include <Eigen/Core>
#include "Point3dUtils.hpp"

// The Orisol Module Link Protocol
namespace omlp
{
    typedef std::unordered_map<std::string, Poco::DynamicAny> Scenario;

    // the default address for pushers
    const std::string Default_Dio_PushTo = "tcp://127.0.0.1:6810";
    const std::string Default_Slider_PushTo = "tcp://127.0.0.1:6820";
    const std::string Default_Conveyor_PushTo = "tcp://127.0.0.1:6830";
    const std::string Default_Camera_PushTo = "tcp://127.0.0.1:6840";
    const std::string Default_Scanner_PushTo = "tcp://127.0.0.1:6850";
    const std::string Default_Vision_PushTo = "tcp://127.0.0.1:6860";
    const std::string Default_Robot_PushTo = "tcp://127.0.0.1:6870";

    enum class FlowId : uint8_t
    {
        DigitalInputChanges  = 0x00,  // Dio worker send this whenever any DIO changes detected
        ScanPrepare          = 0x01,  // signal Scanner worker to get scanner hardware ready for scanning
        ScanStart            = 0x02,  // signal Scanner worker to start scan
        ScanStop             = 0x03,  // signal Scanner worker to stop scan
        ScanData             = 0x04,  // Scanner worker push out scanned raw data
        SliderPrepare        = 0x05,  // signal Slider worker to get ready for the next move
        SliderMove           = 0x06,  // signal Slider worker to move slider 
        SliderBack			 = 0x07,  // signal Slider worker to move slider back, added by Nadeesh
        ScanZAlignmentStart  = 0x08,  // signal Scanner worker to do Z origin alignment 
        ScanZAlignmentDone   = 0x09,  // signal Slider worker that Z origin alignment is done
        VisionData           = 0x0A,  // Vision worker push out processed data
        RobotStart           = 0x0B,  // signal Robot worker to start doing its job
        ConveyorStandby      = 0x0C,  // signal Conveyor worker to get ready for the next move
        ConveyorMoving       = 0x0D,  // inform MMI that conveyor is moving for cycle time calculation
        JigData              = 0x0E,  // Vision worker push out pin position data of calibration jig
        ToPreStationDisconnect  = 0x0F,
        ToNextStationDisconnect = 0x10,
        ToPreStationConnect     = 0x11,
        ToNextStationConnectOK  = 0x12,
        ToPreStationConveyorPausedTimeoutAlarm       = 0x13,
        ToPreStationConveyorPausedTimeoutAlarmCancel = 0x14,
        ToPreStationSetupOvenConveyorTime            = 0x15,
        ToPreStationConveyorStarted                  = 0x16,
        ToPreStationConveyorPaused                   = 0x17,
		ToPreStationUpdateRecipe			= 0x18,
		ToNextStationUpdateRecipeOK			= 0x19,
		ToNextStationUpdateRecipeFailure	= 0x1A,
		ToNextStationUpdateRecipe			= 0x1B,
		ToPreStationUpdateRecipeOK			= 0x1C,
		ToPreStationUpdateRecipeFailure		= 0x1D,
		RobotStartDone						= 0x1E,
		ToPreStationTest					= 0x1F,
		ToPreStationSyncCleanGun			= 0x20,
		ToPreStationDoCleanGun				= 0x21,
		RobotDoSyncCleanGun					= 0x22,
		RobotBeginSyncCleanGun				= 0x23,
		RobotEndSyncCleanGun				= 0x24,
        InvalidFlow                  // end of FlowId type
    };

    // Check the FlowId type byte.
    inline FlowId checkWorkflowType(zmq::multipart_t & flow)
    {
        if (flow.empty())
            return FlowId::InvalidFlow;

        try
        {
            uint8_t flowid = flow.poptyp<uint8_t>();
            if (flowid < (uint8_t)FlowId::InvalidFlow)
                return (FlowId)flowid;
            else
                return FlowId::InvalidFlow;
        }
        catch (...)
        {
            return FlowId::InvalidFlow;
        }
    }

    // flush received message in queue
    inline void flushReceivedMessage(zmq::socket_t & puller)
    {
        try
        {
            zmq::multipart_t flowIncoming;
            // flush at most 10 flow message, no single worker need 10 messages in a flow 
            for (uint32_t i = 0; i < 10; ++i)
                if (!flowIncoming.recv(puller, ZMQ_DONTWAIT))
                    break;
        }
        catch (...)
        {
            return;
        }
    }

    // helper function to make workflow message DigitalInputChanges
    inline void makeWork_DigitalInputChanged(zmq::multipart_t & flow, uint32_t di_status, uint32_t di_changed)
    {
        flow.addtyp<uint8_t>((uint8_t)FlowId::DigitalInputChanges);
        flow.addtyp<uint32_t>(di_status);
        flow.addtyp<uint32_t>(di_changed);
    }

    // helper function to make workflow message ScanPrepare
    // mmScanDistance is the scanning distance in unit mm
    // profileInterval is the space between each profile in unit 1/100 mm
    // pulseSamplingInterval = 0: sampling by slider, others: sampling by scanner
	// exposureGroup is the detected scanner exposure group by camera
    inline void makeWork_ScanPrepare(zmq::multipart_t & flow, uint8_t lineid, uint32_t mmScanDistance, uint32_t profileInterval, uint32_t pulseSamplingInterval, int32_t exposureGroup, bool cameraimage)
    {
        flow.addtyp<uint8_t>((uint8_t)FlowId::ScanPrepare);
        flow.addtyp<uint8_t>(lineid);
        flow.addtyp<uint32_t>(mmScanDistance);
        flow.addtyp<uint32_t>(profileInterval);
        flow.addtyp<uint32_t>(pulseSamplingInterval);
		flow.addtyp<int32_t>(exposureGroup);
		flow.addtyp<uint8_t>(cameraimage ? 1 : 0);
    }

    // helper function to extract workflow message ScanPrepare
    inline void takeJob_ScanPrepare(zmq::multipart_t & flow, uint8_t & lineid, uint32_t & mmScanDistance, double & profileInterval, uint32_t & pulseSamplingInterval, int32_t & exposureGroup, bool & cameraimage)
    {
        if (flow.size() < 6)
            return;
        try
        {
            lineid = flow.poptyp<uint8_t>();
            mmScanDistance = flow.poptyp<uint32_t>();
            profileInterval = (double)flow.poptyp<uint32_t>() / 100.0;
            pulseSamplingInterval = flow.poptyp<uint32_t>();
			exposureGroup = flow.poptyp<int32_t>();
			cameraimage = flow.poptyp<uint8_t>() == 1 ? true : false;
        }
        catch (...)
        {
            return;
        }
    }

    // helper function to make workflow message ScanStart
    inline void makeWork_ScanStart(zmq::multipart_t & flow, uint8_t lineid, std::string jobTakenId)
    {
        flow.addtyp<uint8_t>((uint8_t)FlowId::ScanStart);
        flow.addtyp<uint8_t>(lineid);
		flow.addstr(jobTakenId);
    }

    // helper function to extract workflow message ScanStart
    inline void takeJob_ScanStart(zmq::multipart_t & flow, uint8_t & lineid, std::string & jobTakenId)
    {
        if (flow.size() < 2)
            lineid = 0;
		else {
			lineid = flow.poptyp<uint8_t>();
			jobTakenId = flow.popstr();
		}
    }

    // helper function to make workflow message ScanStop
    inline void makeWork_ScanStop(zmq::multipart_t & flow, uint8_t lineid)
    {
        flow.addtyp<uint8_t>((uint8_t)FlowId::ScanStop);
        flow.addtyp<uint8_t>(lineid);
    }

    // helper function to extract workflow message ScanStop
    inline void takeJob_ScanStop(zmq::multipart_t & flow, uint8_t & lineid)
    {
        if (flow.size() < 1)
            lineid = 0;
        else
            lineid = flow.poptyp<uint8_t>();
    }

    // helper function to make workflow message ScanData
    inline void makeWork_ScanData(zmq::multipart_t & flow, uint8_t lineid, bool cameraimage, CloudMeta & meta, const std::vector<Point3d> & pt3dvector)
    {
        flow.addtyp<uint8_t>((uint8_t)FlowId::ScanData);
        flow.addtyp<uint8_t>(lineid);
		flow.addtyp<uint8_t>(cameraimage ? 1 : 0);
        flow.addstr(meta.datetime);
        flow.addtyp<uint32_t>(meta.grid_width);
        flow.addtyp<uint32_t>(meta.grid_length);
        flow.addtyp<double>(meta.x_resolution);
        flow.addtyp<double>(meta.y_resolution);
        flow.addtyp<double>(meta.z_resolution);
        flow.addmem(pt3dvector.data(), sizeof(Point3d) * meta.vertex_size);
    }

    // helper function to extract workflow message ScanData
    // Note: before taking this job, call checkWorkflowType() to pop out the FlowId
    inline std::vector<Point3d> takeJob_ScanData(zmq::multipart_t & flow, uint8_t & lineid, bool & cameraimage, CloudMeta & meta)
    {
        if (flow.size() < 9)
        {
            meta = { 0, 0, 0, .0, .0, .0, std::string("") };
            return std::vector<Point3d>();
        }

        try
        {
            lineid = flow.poptyp<uint8_t>();
			cameraimage = flow.poptyp<uint8_t>() == 1 ? true : false;
            meta.datetime = flow.popstr();
            meta.grid_width = flow.poptyp<uint32_t>();
            meta.grid_length = flow.poptyp<uint32_t>();
            meta.vertex_size = meta.grid_width * meta.grid_length;
            meta.x_resolution = flow.poptyp<double>();
            meta.y_resolution = flow.poptyp<double>();
            meta.z_resolution = flow.poptyp<double>();
            std::vector<Point3d> pt3dvector(meta.vertex_size);
            zmq::message_t frame = flow.pop();
            std::copy((Point3d *)frame.data(), ((Point3d *)frame.data()) + meta.vertex_size, pt3dvector.begin());
            return pt3dvector;
        }
        catch (...)
        {
            return std::vector<Point3d>();
        }
    }

    // helper function to make workflow message SliderPrepare
    // maxFrameRate is the maximum sampling rate in Hz, Slider needs to calculate the max velocity from this
    inline void makeWork_SliderPrepare(zmq::multipart_t & flow, uint8_t lineid, double maxFrameRate)
    {
        flow.addtyp<uint8_t>((uint8_t)FlowId::SliderPrepare);
        flow.addtyp<uint8_t>(lineid);
        flow.addtyp<double>(maxFrameRate);
    }

    // helper function to extract workflow message ScanStart
    inline void takeJob_SliderPrepare(zmq::multipart_t & flow, uint8_t & lineid, double & maxFrameRate)
    {
        if (flow.size() < 2)
            return;
        try
        {
            lineid = flow.poptyp<uint8_t>();
            maxFrameRate = flow.poptyp<double>();
        }
        catch (...)
        {
            return;
        }
    }

    // helper function to make workflow message SliderMove
    inline void makeWork_SliderMove(zmq::multipart_t & flow, uint8_t lineid)
    {
        flow.addtyp<uint8_t>((uint8_t)FlowId::SliderMove);
        flow.addtyp<uint8_t>(lineid);
    }

    // helper function to extract workflow message SliderMove
    inline void takeJob_SliderMove(zmq::multipart_t & flow, uint8_t & lineid)
    {
        if (flow.size() < 1)
            lineid = 0;
        else
            lineid = flow.poptyp<uint8_t>();
    }

    // helper function to make workflow message SliderBack
    inline void makeWork_SliderBack(zmq::multipart_t & flow, uint8_t lineid)
    {
        flow.addtyp<uint8_t>((uint8_t)FlowId::SliderBack);
        flow.addtyp<uint8_t>(lineid);
    }

    // helper function to extract workflow message SliderBack
    inline void takeJob_SliderBack(zmq::multipart_t & flow, uint8_t & lineid)
    {
        if (flow.size() < 1)
            lineid = 0;
        else
            lineid = flow.poptyp<uint8_t>();
    }

    // helper function to make workflow message ScanZAlignmentStart
    inline void makeWork_ScanZAlignmentStart(zmq::multipart_t & flow, uint8_t lineid)
    {
        flow.addtyp<uint8_t>((uint8_t)FlowId::ScanZAlignmentStart);
        flow.addtyp<uint8_t>(lineid);
    }

    // helper function to extract workflow message ScanZAlignmentStart
    inline void takeJob_ScanZAlignmentStart(zmq::multipart_t & flow, uint8_t & lineid)
    {
        if (flow.size() < 1)
            lineid = 0;
        else
            lineid = flow.poptyp<uint8_t>();
    }

    // helper function to make workflow message ScanZAlignmentDone
    inline void makeWork_ScanZAlignmentDone(zmq::multipart_t & flow, uint8_t lineid)
    {
        flow.addtyp<uint8_t>((uint8_t)FlowId::ScanZAlignmentDone);
        flow.addtyp<uint8_t>(lineid);
    }

    // helper function to extract workflow message ScanZAlignmentDone
    inline void takeJob_ScanZAlignmentDone(zmq::multipart_t & flow, uint8_t & lineid)
    {
        if (flow.size() < 1)
            lineid = 0;
        else
            lineid = flow.poptyp<uint8_t>();
    }

    // helper function to make workflow message VisionData 
    inline void makeWork_VisionData(zmq::multipart_t & flow,
        uint8_t lineid,
		const std::string sole_type_name,
        const std::vector<Eigen::MatrixX3d>& edges,
        const std::vector<Eigen::VectorXd>& angles,
        const std::vector<Eigen::MatrixX3d>& normals,
        const std::string & datetime)
    {
        flow.addtyp<uint8_t>((uint8_t)FlowId::VisionData);
        flow.addtyp<uint8_t>(lineid);
		flow.addstr(sole_type_name);
        // dateitme string as job ID
        flow.addstr(datetime);

        // the number of edge layers
        flow.addtyp<uint32_t>((uint32_t)edges.size());
		// the number of angles layers
		flow.addtyp<uint32_t>((uint32_t)angles.size());
		// the number of normal layers
		flow.addtyp<uint32_t>((uint32_t)normals.size());

        // occupy one frame for each layer of edge data
        if (edges.empty())
        {
			;//flow.addmem(edges.data(), 0);
        }
        else
        {
            for (const auto& mat3d : edges)
                flow.addmem(mat3d.data(), sizeof(double) * mat3d.size());
        }

        if (angles.empty())
        {
			;//flow.addmem(angles.data(), 0);
        }
        else
        {
            // occupy one frame for each layer of angle data
            for (const auto& vecxd : angles)
                flow.addmem(vecxd.data(), sizeof(double) * vecxd.size());
        }

        if (normals.empty())
        {
			;//flow.addmem(normals.data(), 0);
        }
        else
        {
            // occupy one frame for each layer of normal data
            for (const auto& mat3d : normals)
                flow.addmem(mat3d.data(), sizeof(double) * mat3d.size());
        }
    }

    // helper function to extract workflow message VisionData
    // Note: before taking this job, call checkWorkflowType() to pop out the FlowId
    inline size_t takeJob_VisionData(zmq::multipart_t & flow,
        uint8_t & lineid,
		std::string & sole_type_name,
        std::vector<Eigen::MatrixX3d>& edges,
        std::vector<Eigen::VectorXd>& angles,
        std::vector<Eigen::MatrixX3d>& normals,
        std::string & datetime)
    {
        // the minimum requirement is 6 frames
		if (flow.size() >= 3) {
			lineid = flow.poptyp<uint8_t>();
			sole_type_name = flow.popstr();
			// datetime string as job id
			datetime = flow.popstr();
		}

        if (flow.size() < 7)
            return 0;

        try
        {            
            size_t numoflayers = (size_t)flow.poptyp<uint32_t>();
			size_t numofangles = (size_t)flow.poptyp<uint32_t>();
			size_t numofnormals = (size_t)flow.poptyp<uint32_t>();

			// edge layers
            edges.clear();
            if (numoflayers == 0)
                return numoflayers;
            for (size_t i = 0; i < numoflayers; ++i)
            {
                zmq::message_t frame = flow.pop();
                Eigen::Map<Eigen::MatrixX3d> mat3d((double *)frame.data(), frame.size() / (sizeof(double) * 3), 3);
                edges.push_back(std::move(mat3d));
            }

            // angle layers
            angles.clear();
            for (size_t i = 0; i < numofangles; ++i)
            {
                zmq::message_t frame = flow.pop();
                Eigen::Map<Eigen::VectorXd> vecxd((double *)frame.data(), frame.size() / sizeof(double));
                angles.push_back(std::move(vecxd));
            }

            // normal layers
            normals.clear();
            for (size_t i = 0; i < numofnormals; ++i)
            {
                zmq::message_t frame = flow.pop();
                Eigen::Map<Eigen::MatrixX3d> mat3d((double *)frame.data(), frame.size() / (sizeof(double) * 3), 3);
                normals.push_back(std::move(mat3d));
            }

            return numoflayers;
        }
        catch (...)
        {
            return 0;
        }
    }

    // helper function to make workflow message RobotStart
    inline void makeWork_RobotStart(zmq::multipart_t & flow, uint8_t lineid, bool beginrun)
    {
        flow.addtyp<uint8_t>((uint8_t)FlowId::RobotStart);
        flow.addtyp<uint8_t>(lineid);
        uint8_t begin = beginrun ? 1 : 0;
        flow.addtyp<uint8_t>(begin);
    }

    // helper function to extract workflow message RobotStart
    inline void takeJob_RobotStart(zmq::multipart_t & flow, uint8_t & lineid, bool & beginrun)
    {
        lineid = flow.poptyp<uint8_t>();
        uint8_t begin = flow.poptyp<uint8_t>();
        beginrun = begin == 1 ? true : false;
    }

	// helper function to make workflow message RobotStartDone
	inline void makeWork_RobotStartDone(zmq::multipart_t & flow, uint8_t lineid, bool robotmove)
	{
		flow.addtyp<uint8_t>((uint8_t)FlowId::RobotStartDone);
		flow.addtyp<uint8_t>(lineid);
		uint8_t move = robotmove ? 1 : 0;
		flow.addtyp<uint8_t>(move);
	}

	// helper function to extract workflow message RobotStartDone
	inline void takeJob_RobotStartDone(zmq::multipart_t & flow, uint8_t & lineid, bool & robotmove)
	{
		lineid = flow.poptyp<uint8_t>();
		uint8_t move = flow.poptyp<uint8_t>();
		robotmove = move == 1 ? true : false;
	}

	// helper function to make workflow message RobotDoSyncCleanGun
	inline void makeWork_RobotDoSyncCleanGun(zmq::multipart_t & flow)
	{
		flow.addtyp<uint8_t>((uint8_t)FlowId::RobotDoSyncCleanGun);
	}

	// helper function to make workflow message RobotBeginSyncCleanGun
	inline void makeWork_RobotBeginSyncCleanGun(zmq::multipart_t & flow)
	{
		flow.addtyp<uint8_t>((uint8_t)FlowId::RobotBeginSyncCleanGun);
	}

	// helper function to make workflow message RobotEndSyncCleanGun
	inline void makeWork_RobotEndSyncCleanGun(zmq::multipart_t & flow)
	{
		flow.addtyp<uint8_t>((uint8_t)FlowId::RobotEndSyncCleanGun);
	}

    // helper function to make workflow message ConveyorStandby 
    inline void makeWork_ConveyorStandby(zmq::multipart_t & flow, uint8_t lineid)
    {
        flow.addtyp<uint8_t>((uint8_t)FlowId::ConveyorStandby);
        flow.addtyp<uint8_t>(lineid);
    }

    // helper function to extract workflow message ConveyorStandby
    inline void takeJob_ConveyorStandby(zmq::multipart_t & flow, uint8_t & lineid)
    {
        if (flow.size() < 1)
            lineid = 0;
        else
            lineid = flow.poptyp<uint8_t>();
    }

    // helper function to make workflow message ConveyorMoving
    inline void makeWork_ConveyorMoving(zmq::multipart_t & flow, uint8_t lineid, int32_t exposure_group)
    {
        flow.addtyp<uint8_t>((uint8_t)FlowId::ConveyorMoving);
        flow.addtyp<uint8_t>(lineid);
		flow.addtyp<int32_t>(exposure_group);
    }

    // helper function to make workflow message JigData 
    inline void makeWork_JigData(zmq::multipart_t & flow, uint8_t lineid, const std::vector<Point3d> & jigpoints)
    {
        flow.addtyp<uint8_t>((uint8_t)FlowId::JigData);
        flow.addtyp<uint8_t>(lineid);
        flow.addtyp<uint32_t>((uint32_t)jigpoints.size());
        flow.addmem(jigpoints.data(), sizeof(Point3d) * jigpoints.size());
    }

    // helper function to extract workflow message JigData
    // Note: before taking this job, call checkWorkflowType() to pop out the FlowId
    inline std::vector<Point3d> takeJob_JigData(zmq::multipart_t & flow, uint8_t & lineid)
    {
        if (flow.size() < 3) {
            lineid = 0;
            return std::vector<Point3d>();
        }

        try
        {
            // the number of jig pins
            lineid = flow.poptyp<uint8_t>();
            size_t numofpins = (size_t)flow.poptyp<uint32_t>();
            std::vector<Point3d> jigpins(numofpins);
            zmq::message_t frame = flow.pop();
            std::copy((Point3d *)frame.data(), ((Point3d *)frame.data()) + numofpins, jigpins.begin());
            return jigpins;
        }
        catch (...)
        {
            return std::vector<Point3d>();
        }
    }

	// helper function to make workflow message ToPreStationDoCleanGun 
	inline void makeWork_ToPreStationDoCleanGun(zmq::multipart_t & flow)
	{
		flow.addtyp<uint8_t>((uint8_t)FlowId::ToPreStationDoCleanGun);
	}

	// helper function to make workflow message ToPreStation2DoCleanGun 
	inline void makeWork_ToPreStation2DoCleanGun(zmq::multipart_t & flow)
	{
		flow.addtyp<uint8_t>((uint8_t)FlowId::ToPreStationDoCleanGun);
	}

	// helper function to make workflow message ToPreStationSyncCleanGun 
	inline void makeWork_ToPreStationSyncCleanGun(zmq::multipart_t & flow, bool sync_clean_gun)
	{
		flow.addtyp<uint8_t>((uint8_t)FlowId::ToPreStationSyncCleanGun);
		uint8_t sync = sync_clean_gun ? 1 : 0;
		flow.addtyp<uint8_t>(sync_clean_gun);
	}

	// helper function to extract workflow message ToPreStationSyncCleanGun
	inline void takeJob_ToPreStationSyncCleanGun(zmq::multipart_t & flow, bool & sync_clean_gun)
	{
		if (flow.size() < 1)
			sync_clean_gun = false;
		else {
			uint8_t sync = flow.poptyp<uint8_t>();
			sync_clean_gun = sync == 1 ? true : false;
		}
	}

	// helper function to make workflow message ToPreStation2SyncCleanGun 
	inline void makeWork_ToPreStation2SyncCleanGun(zmq::multipart_t & flow, bool sync_clean_gun)
	{
		flow.addtyp<uint8_t>((uint8_t)FlowId::ToPreStationSyncCleanGun);
		uint8_t sync = sync_clean_gun ? 1 : 0;
		flow.addtyp<uint8_t>(sync_clean_gun);
	}

	// helper function to extract workflow message ToPreStation2SyncCleanGun
	inline void takeJob_ToPreStation2SyncCleanGun(zmq::multipart_t & flow, bool & sync_clean_gun)
	{
		if (flow.size() < 1)
			sync_clean_gun = false;
		else {
			uint8_t sync = flow.poptyp<uint8_t>();
			sync_clean_gun = sync == 1 ? true : false;
		}
	}

    // helper function to make workflow message ToPreStationDisconnect 
    inline void makeWork_ToPreStationDisconnect(zmq::multipart_t & flow)
    {
        flow.addtyp<uint8_t>((uint8_t)FlowId::ToPreStationDisconnect);
    }

    // helper function to make workflow message ToNextStationDisconnect 
    inline void makeWork_ToNextStationDisconnect(zmq::multipart_t & flow)
    {
        flow.addtyp<uint8_t>((uint8_t)FlowId::ToNextStationDisconnect);
    }

    // helper function to make workflow message ToPreStationConnect 
    inline void makeWork_ToPreStationConnect(zmq::multipart_t & flow)
    {
        flow.addtyp<uint8_t>((uint8_t)FlowId::ToPreStationConnect);
    }

    // helper function to make workflow message ToNextStationConnectOK 
    inline void makeWork_ToNextStationConnectOK(zmq::multipart_t & flow)
    {
        flow.addtyp<uint8_t>((uint8_t)FlowId::ToNextStationConnectOK);
    }

	// helper function to make workflow message ToPreStationTest
	inline void makeWork_ToPreStationTest(zmq::multipart_t & flow)
	{
		flow.addtyp<uint8_t>((uint8_t)FlowId::ToPreStationTest);
	}

    // helper function to make workflow message ToPreStationConveyorPausedTimeoutAlarm 
    inline void makeWork_ToPreStationConveyorPausedTimeoutAlarm(zmq::multipart_t & flow)
    {
        flow.addtyp<uint8_t>((uint8_t)FlowId::ToPreStationConveyorPausedTimeoutAlarm);
    }

    // helper function to make workflow message ToPreStationConveyorPausedTimeoutAlarmCancel 
    inline void makeWork_ToPreStationConveyorPausedTimeoutAlarmCancel(zmq::multipart_t & flow)
    {
        flow.addtyp<uint8_t>((uint8_t)FlowId::ToPreStationConveyorPausedTimeoutAlarmCancel);
    }

    // helper function to make workflow message ToPreStationSetupOvenConveyorTime 
    inline void makeWork_ToPreStationSetupOvenConveyorTime(zmq::multipart_t & flow, uint32_t time, uint32_t soledistance, uint32_t ovendistance, uint32_t extratime)
    {
        flow.addtyp<uint8_t>((uint8_t)FlowId::ToPreStationSetupOvenConveyorTime);
        flow.addtyp<uint32_t>(time);
        flow.addtyp<uint32_t>(soledistance);
		flow.addtyp<uint32_t>(ovendistance);
        flow.addtyp<uint32_t>(extratime);
    }

    // helper function to extract workflow message ToPreStationSetupOvenConveyorTime
    inline void takeJob_ToPreStationSetupOvenConveyorTime(zmq::multipart_t & flow, uint32_t & time, uint32_t & soledistance, uint32_t & ovendistance, uint32_t & extratime)
    {
        time = flow.poptyp<uint32_t>();
        soledistance = flow.poptyp<uint32_t>();
		ovendistance = flow.poptyp<uint32_t>();
        extratime = flow.poptyp<uint32_t>();
    }

    // helper function to make workflow message ToPreStationConveyorStarted 
    inline void makeWork_ToPreStationConveyorStarted(zmq::multipart_t & flow)
    {
        flow.addtyp<uint8_t>((uint8_t)FlowId::ToPreStationConveyorStarted);
    }

    // helper function to make workflow message ToPreStationConveyorPaused 
    inline void makeWork_ToPreStationConveyorPaused(zmq::multipart_t & flow)
    {
        flow.addtyp<uint8_t>((uint8_t)FlowId::ToPreStationConveyorPaused);
    }

	// helper function to make workflow message ToPreStationUpdateRecipe
	inline void makeWork_ToPreStationUpdateRecipe(zmq::multipart_t & flow, std::string soletype, std::vector<std::string> & settings)
	{
		flow.addtyp<uint8_t>((uint8_t)FlowId::ToPreStationUpdateRecipe);
		flow.addstr(soletype);
		uint32_t items = (uint32_t)settings.size();
		flow.addtyp<uint32_t>(items);
		for (uint32_t i = 0; i < items; i++)
			flow.addstr(settings[i]);
	}

	// helper function to extract workflow message ToPreStationUpdateRecipe
	inline void takeJob_ToPreStationUpdateRecipe(zmq::multipart_t & flow, std::string & soletype, std::vector<std::string> & settings)
	{
		soletype = flow.popstr();
		uint32_t items = flow.poptyp<uint32_t>();
		settings.clear();
		for (uint32_t i = 0; i < items; i++) {
			std::string value = flow.popstr();
			settings.push_back(value);
		}
	}

	// helper function to make workflow message ToNextStationUpdateRecipe
	inline void makeWork_ToNextStationUpdateRecipe(zmq::multipart_t & flow, std::string soletype, std::vector<std::string> & settings)
	{
		flow.addtyp<uint8_t>((uint8_t)FlowId::ToNextStationUpdateRecipe);
		flow.addstr(soletype);
		uint32_t items = (uint32_t)settings.size();
		flow.addtyp<uint32_t>(items);
		for (uint32_t i = 0; i < items; i++)
			flow.addstr(settings[i]);
	}

	// helper function to extract workflow message ToNextStationUpdateRecipe
	inline void takeJob_ToNextStationUpdateRecipe(zmq::multipart_t & flow, std::string & soletype, std::vector<std::string> & settings)
	{
		soletype = flow.popstr();
		uint32_t items = flow.poptyp<uint32_t>();
		settings.clear();
		for (uint32_t i = 0; i < items; i++) {
			std::string value = flow.popstr();
			settings.push_back(value);
		}
	}

	// helper function to extract workflow message ToPreStationSetupOvenConveyorTime

	// helper function to make workflow message ToNextStationUpdateRecipeOK
	inline void makeWork_ToNextStationUpdateRecipeOK(zmq::multipart_t & flow)
	{
		flow.addtyp<uint8_t>((uint8_t)FlowId::ToNextStationUpdateRecipeOK);
	}

	// helper function to make workflow message ToNextStationUpdateRecipeFailure
	inline void makeWork_ToNextStationUpdateRecipeFailure(zmq::multipart_t & flow)
	{
		flow.addtyp<uint8_t>((uint8_t)FlowId::ToNextStationUpdateRecipeFailure);
	}

	// helper function to make workflow message ToPreStationUpdateRecipeOK
	inline void makeWork_ToPreStationUpdateRecipeOK(zmq::multipart_t & flow)
	{
		flow.addtyp<uint8_t>((uint8_t)FlowId::ToPreStationUpdateRecipeOK);
	}

	// helper function to make workflow message ToPreStationUpdateRecipeFailure
	inline void makeWork_ToPreStationUpdateRecipeFailure(zmq::multipart_t & flow)
	{
		flow.addtyp<uint8_t>((uint8_t)FlowId::ToPreStationUpdateRecipeFailure);
	}
}