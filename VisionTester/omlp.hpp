/*
    File:
        omlp.hpp
    Description:
        OMLP (Orisol Module Link Protocol) version 1.0 definitions and helper functions.
        This file should be shared over all projects that use OMLP v1.0 to communicate.
    Copyright:
        Orisol Asia Ltd.
*/
#pragma once
#include <string>
#include <utility>
#include <unordered_map>
#include <zmq.hpp>
#include <zmq_addon.hpp>
#include <Poco/Notification.h>
#include <Poco/DynamicAny.h>
#include "Point3dUtils.hpp"
#include "Scanner.h"
#include "Vision.h"
#include "Robot.h"
#include "Dio.h"

// The Orisol Module Link Protocol
namespace omlp
{
    // possible Worker fields:
    // ["name"] - string name of the worker application
    // ["pid"] - Process::PID of the worker application
    // ["linkup"] - true/false link state
    using Worker = std::unordered_map<std::string, Poco::DynamicAny>;
    // Workers use OMLP link id as key to locate a worker
    using Workers = std::unordered_map<std::string, Worker>;

    // the default address to Unit Manager
    const std::string Default_Manager_Address = "tcp://127.0.0.1:6801";
    // TODO: need a stronger authentication mechanism
    const std::string Default_Authentication_Key = "Q29weXJpZ2h0KGMpIDIwMTcsIE9yaXNvbCBBc2lhIEx0ZC4K";

    #define HEADER_LENGTH 4
    // Header frame (4 bytes)
    // All header fields shall be defined in printable characters whenever possible.
    enum class HeaderField : char
    {
    /* all message header start with one byte signature 0x4F = 'O' */
        Signature = 0x4F,
    /* and protocol major version number 0x31 = '1' */
        Version = 0x31,
    /* then one byte of reserved field padding with 0x20 = ' ' */
        Padding = 0x20,
    /* followed by one byte of target ID */
        // System Chief ID : 0x21 = '!'
        IdChief = 0x21,
        // Default MMI shell ID : 0x24 = '$'
        IdShell = 0x24,
        // Unit manager ID : 0x41 = 'A'
        IdManagerA = 0x41,
        // Unit manager ID : 0x42 = 'B'
        IdManagerB = 0x42,
        // Unit manager ID : 0x43 = 'C'
        IdManagerC = 0x43,
        // Unit manager ID : 0x44 = 'D'
        IdManagerD = 0x44,
        // Unit manager ID : 0x45 = 'E'
        IdManagerE = 0x45,
        // Unit manager ID : 0x46 = 'F'
        IdManagerF = 0x46,
        // Unit manager ID : 0x5A = 'Z', for Standalone
        IdManagerZ = 0x5A,
        // Dio module ID : 0x61 = 'a'
        IdDio = 0x61,
        // Slider module ID : 0x62 = 'b'
        IdSlider = 0x62,
        // Scanner module ID : 0x63 = 'c'
        IdScanner = 0x63,
        // Conveyor module ID : 0x64 = 'd'
        IdConveyor = 0x64,
        // Vision algorithm module ID : 0x65 = 'e'
        IdVision = 0x65,
        // Robot module ID : 0x66 = 'f'
        IdRobot = 0x66,
        // DioMotion module ID : 0x67 = 'g'
        IdDioMotion = 0x67,
    /* No such field value exist, a special invalid value indicator */
        NotExist = 0x7F
    };

    // Message ID frame (1 byte)
    // All message shall be define in sequence
	enum class MessageId : uint8_t
	{
		/* Notification Acknowledgement type */
		Nack = 0x00,
		Ack = 0x01,
		/* System Command type : these types affect states of modules and overall systemi, must confirm in two-way by Ack/Nack */
		CommandLink = 0x02,
		CommandInit = 0x03,
		CommandRun = 0x04,
		CommandStop = 0x05,
		CommandHalt = 0x06,
		CommandResume = 0x07,
		CommandReset = 0x08,
		CommandUnlink = 0x09,
		/* Heartbeat message pair : return a Pong whenever a Ping is received */
		HeartbeatPing = 0x0A,
		HeartbeatPong = 0x0B,
		/* Notification type : one-way system-wide general notification */
		NotifyCommandResult = 0x0C,                         // notify Shell or Chief the result of CommandXXX message
		NotifyError = 0x0D,                                 // error notification, from Workers to Manager, and from Manager to Shell
	/* Shell/MMI request type : these requests are from MMI to Manager and handle by Manager only */
		ShellReqCommand = 0x0E,                             // with CommandXXX wrapped inside
		ShellReqLinkSetup = 0x0F,                           // trigger manager to link + init workers in specific scenario
	/* System Information Get/Reply pair : from Shell/Chief to get info from Manager */
		GetSystemStatus = 0x10,
		ReplySystemStatus = 0x11,
		/* Version info Get/Reply pair : from Shell/Chief to Manager and propagate to all workers */
		GetVersion = 0x12,
		ReplyVersion = 0x13,
		/* Setup type : from Shell pass-thru Manager to Workers */
		SetOutputDevice = 0x14,                             // to Dio
		GetBigConveyorMinimumCycleTime = 0x15,				// to Conveyor
		GetOvenConveyorLengthTime = 0x16,                   // to Conveyor
		SetStationLinkPrev = 0x17,                          // to Conveyor
		SetClearSoleInProgress = 0x18,                      // to Conveyor
		SetOvenConveyorTime = 0x19,                         // to Conveyor
		ResetConveyorPausedTimeoutAlarm = 0x1A,             // to Conveyor
		SetSoleFeedingMode = 0x1B,                          // to Conveyor
		SetBigConveyorCycleTime = 0x1C,                     // to Conveyor
		SetBigConveyorCleaning = 0x1D,                      // to Conveyor
		SetCalibrationLineID = 0x1E,						// to Conveyor
		SetSliderMovingSpeed = 0x1F,                        // to Slider
		SetSliderHoming = 0x20,                             // to Slider
		SetZoriginAlignment = 0x21,                         // to Slider
		SetScannerParameter = 0x22,                         // to Scanner
		SetVisionRecipe = 0x23,                             // to Vision
		GetRobotPosition = 0x24,                            // to Robot
		GetSprayCounter = 0x25,                             // to Robot
		SetPathPlanningRecipe = 0x26,                       // to Robot
		SetRobotHoming = 0x27,                              // to Robot
		SetCleanGun = 0x28,                                 // to Robot
		SetSprayLine = 0x29,                                // to Robot
		SetRobotIdleTime = 0x2A,                            // to Robot
		SetRobotSprayCount = 0x2B,                          // to Robot
		SetReleaseGlue = 0x2C,                              // to Robot
		SetRobotMovingSpeed = 0x2D,                         // to Robot
		ResetDailySprayCounter = 0x2E,                      // to Robot
		SetRobotAction = 0x2F,                              // to Robot
		SetRobotMove = 0x30,								// to Robot
		GetJigPointsError = 0x31,                           // to Robot
	/* Notification type : from Workers pass-thru Manager to Shell */
		ReplyBigConveyorMinimumCycleTime = 0x32,			// to Shell
		ReplyOvenConveyorLengthTime = 0x33,                 // to Shell
		ReplyRobotPosition = 0x34,                          // to Shell
		ReplySprayCounter = 0x35,                           // to Shell
		ReplyJigPointsError = 0x36,                         // to Shell
		NotifySetBigConveyorCycleTimeToMinimum = 0x37,		// to Shell
		NotifyStationLinkFromNext = 0x38,                   // to Shell
		NotifyStationLinkFromPrev = 0x39,                   // to Shell
		NotifyConveyorPausedTimeoutAlarm = 0x3A,            // to Shell
		NotifyResetConveyorPausedTimeoutAlarmByNext = 0x3B, // to Shell
		NotifyJigPoints = 0x3C,                             // to Shell
		NotifySoleContour = 0x3D,                           // to Shell
		NotifyJobTaken = 0x3E,                              // to Shell
		NotifyUncheckClearSole = 0x3F,						// to Shell
		NotifyZoriginAlignmentDone = 0x40,                  // to Shell
		SetAlarmLevel = 0x41,								// to Dio
		A3_SetConveyorPosition = 0x42,						// to Conveyor
		UpdateRecipeToSyncStation = 0x43,					// to Conveyor
		NotifyStationUpdateRecipe = 0x44,					// to Shell
		NotifyStationUpdateRecipeOK = 0x45,					// to Shell
		NotifyStationUpdateRecipeFailure = 0x46,			// to Shell
		RunFrontOvenConveyor = 0x47,						// to Conveyor
		SetAutoExposure = 0x48,								// to Conveyor
		SetAutoSoleType = 0x49,								// to Vision
		SetUsingCommonSpraySettingForVision = 0x4A,			// to Vision
		SetUsingCommonSpraySettingForRobot = 0x4B,			// to Robot
		SetRobotSoakNozzle = 0x4C,							// to Robot
		SetRobotMaintainGun = 0x4D,							// to Robot
		SetRobotMeasureGlueAmount = 0x4E,					// to Robot
		SetDioMeasureGlueAmount = 0x4F,						// to Dio
		ReplyMeasureGlueAmount = 0x50,						// to Shell
		SetConveyorMaintainGun = 0x51,						// to Conveyor
		SetRobotActionToConveyor = 0x52,					// to Conveyor
		SetUseAutoIfPatternNotFound = 0x53,					// to Vision
		SetRobotCollisionSimulation = 0x54,                 // to Robot
		SetUseCustomizedPath		= 0x55,					// to Vision
		SetPlasmaControl			= 0x56,                 // to Robot
		ReplyPlasmaControl			= 0x57,                 // to Shell
		SetUsingCustomizedPathAlgorithmForVision = 0x58,	// to Vision
		SetUsingCustomizedPathAlgorithmForRobot = 0x59,		// to Robot
		NotifyCustomizedPathAlgorithmContour = 0x5A,		// to Shell
    /* Special type : invalid type indicator, and no other type has ID >= this */
        InvalidType
    };

    // Message Parameter: Operation mode (1 byte)
    enum class OpMode : uint8_t
    {
        No_Operation = 0,
        Dio_Diagnostic = 1,
        DioMotion_Diagnostic = 2,
        Scanning_Diagnostic = 3,
        ScanToVision_Diagnostic = 4,
        Vision_Diagnostic = 5,
        Robot_Diagnostic = 6,
        Standalone_Operation = 7,
        Ensemble_Operation = 8,
        Calibration = 9
    };

    // Message Parameter: Machine model (1 byte)
    enum class MachineModel : uint8_t
    {
        Unknown = 0,
        OSR301_K = 1,
        LegoSole_Y = 2,
        OSR2RS_K = 3,
        OSR301_Y = 4,
        LegoSole_Y_Plasma = 5,
        LegoSole_F = 6
    };

    // Message Parameter: Command tone (1 byte)
    enum class Tone : uint8_t
    {
        Soft = 0x00,
        Hard = 0x01,
        NotExist
    };

    // Message Parameter: codes for overall system error types (signed 1 byte)
    enum class ResultCode : int8_t
    {
		ERR_VisionConveyorAlarm = -52,
		ERR_VisionSoleColorNotFound = -51,
		ERR_UploadRobotFileTooLong = -50,
		ERR_SettingFileNotExist = -49,
		ERR_VisionSolePatternNotMatch = -48,
		ERR_RobotCrashToProcess = -47,
		ERR_VisionCrashToProcess = -46,
		ERR_SliderError = -45,
		ERR_VisionConveyorPlaneNotFound = -44,
		ERR_VisionConveyorLowerThanLimit = -43,
		ERR_VisionSolePatternNotFound = -42,
		ERR_DetectExposureFailure = -41,
		ERR_CameraFailure = -40,
		ERR_RobotPathUploadingFailure = -39,
		ERR_NoGlue_Warning = -38,
		ERR_ScannerNoResponse = -37,
		ERR_LineHaltCancel = -36,
		ERR_LineHalt = -35,
		ERR_Level1AlarmCancel = -34,
		ERR_Level1AlarmTrigger = -33,
		ERR_Level3AlarmTrigger = -32,
		ERR_NoGlue = -31,
		ERR_A3PLCConveyorAlarm = -30,
		ERR_Level2AlarmCancel = -29,
		ERR_Level2AlarmTrigger = -28,
		ERR_RS485CommunicationFailure = -27,
        ERR_VisionSoleOverScanArea = -26,
        ERR_ScannerStartScanFailed = -25,
        ERR_A3PLCConveyorHomingFailed = -24,
        ERR_A3PLCConnectionFailed = -23,
        ERR_WorkerDioInitFailed = -22,
        ERR_WorkerSliderInitFailed = -21,
        ERR_WorkerScannerInitFailed = -20,
        ERR_WorkerConveyorInitFailed = -19,
        ERR_WorkerVisionInitFailed = -18,
        ERR_WorkerRobotInitFailed = -17,
        ERR_WorkerDioMotionInitFailed = -16,
        ERR_RearOvenConnectionFailed = -15,
        ERR_FrontOvenConnectionFailed = -14,
        ERR_WorkerDioDown = -13,
        ERR_WorkerSliderDown = -12,
        ERR_WorkerScannerDown = -11,
        ERR_WorkerConveyorDown = -10,
        ERR_WorkerVisionDown = -9,
        ERR_WorkerRobotDown = -8,
        ERR_WorkerDioMotionDown = -7,
        ERR_WorkerDown = -6,
        ERR_VisionFailedToGeneratePath = -5,
        ERR_CantProduceRobotPath = -4,
        ERR_RobotInterfaceFailed = -3,
        ERR_SprayCoverNotOpen = -2,
        ERR_WrongState = -1,
        ERR_SystemGeneral = 0,
        OK = 1
    };

    // Message Parameter: start/stop action types (1 byte)
    enum class Action : uint8_t
    {
        Stop = 0x00,
        Start = 0x01,
        NotExist
    };

    // Message Parameter: link state types (1 byte)
    enum class LinkAction : uint8_t
    {
        Disconnect = 0x00,
        Connect = 0x01,
        NoResponse = 0x02,
        NotExist
    };

    // Message Parameter: sole feeding modes (1 byte)
    enum class Feeding : uint8_t
    {
        Auto = 0x00,
        Sensor = 0x01,
        NotExist
    };

    // Message Parameter: sliding speed levels (1 byte)
    enum class SlidingSpeed : uint8_t
    {
        Low = 0x00,
        Middle = 0x01,
        High = 0x02,
        NotExist
    };

    /*************************************************************************************
     * Notification/Event types for the incoming/outgoing message
     *************************************************************************************/
     // MessageEventBase is the base type of all incoming/outgoing message events
    class MessageEventBase : public Poco::Notification
    {
    public:
        MessageEventBase(MessageId msgid, std::string linkid = "") : _msgid(msgid), _linkid(std::move(linkid)) {}
        MessageId msgid() const { return _msgid; }
        std::string linkid() const { return _linkid; }
    private:
        MessageId _msgid;
        std::string _linkid;
    };

    //-------- Acknowledgement Types -----------------------------------------------------------------------
    class MessageEvent_Ack : public MessageEventBase
    {
    public:
        MessageEvent_Ack(MessageId msgid2ack, std::string linkid = "") : MessageEventBase(MessageId::Ack, linkid), _msgid2ack(msgid2ack) {}
        MessageId msgid2ack() const { return _msgid2ack; }
    private:
        MessageId _msgid2ack;
    };

    class MessageEvent_Nack : public MessageEventBase
    {
    public:
        MessageEvent_Nack(MessageId msgid2nack, std::string strerror = "", std::string linkid = "")
            : MessageEventBase(MessageId::Nack, linkid)
            , _msgid2nack(msgid2nack)
            , _strerror(strerror)
        {}
        MessageId msgid2nack() const { return _msgid2nack; }
        std::string strerror() const { return _strerror; }
    private:
        MessageId _msgid2nack;
        std::string _strerror;
    };

    //-------------- System Command Types -----------------------------------------------------------------------
    // TODO: add password exchange mechanism between CommandLink & Ack
    class MessageEvent_CommandLink : public MessageEventBase
    {
    public:
        MessageEvent_CommandLink(std::string linkid = "") : MessageEventBase(MessageId::CommandLink, linkid) {}
    };

    class MessageEvent_CommandInit : public MessageEventBase
    {
    public:
        MessageEvent_CommandInit(OpMode opmode, MachineModel model, std::string linkid = "")
            : MessageEventBase(MessageId::CommandInit, linkid)
            , _opmode(opmode)
            , _model(model)
        {}
        OpMode opmode() const { return _opmode; }
        MachineModel model() const { return _model; }
    private:
        OpMode _opmode;
        MachineModel _model;
    };

    class MessageEvent_CommandRun : public MessageEventBase
    {
    public:
        MessageEvent_CommandRun(std::string linkid = "") : MessageEventBase(MessageId::CommandRun, linkid) {}
    };

    class MessageEvent_CommandStop : public MessageEventBase
    {
    public:
        MessageEvent_CommandStop(Tone tone, std::string linkid = "") : MessageEventBase(MessageId::CommandStop, linkid), _tone(tone) {}
        Tone tone() const { return _tone; }
    private:
        Tone _tone;
    };

    class MessageEvent_CommandHalt : public MessageEventBase
    {
    public:
        MessageEvent_CommandHalt(Tone tone, std::string linkid = "") : MessageEventBase(MessageId::CommandHalt, linkid), _tone(tone) {}
        Tone tone() const { return _tone; }
    private:
        Tone _tone;
    };

    class MessageEvent_CommandResume : public MessageEventBase
    {
    public:
        MessageEvent_CommandResume(std::string linkid = "") : MessageEventBase(MessageId::CommandResume, linkid) {}
    };

    class MessageEvent_CommandReset : public MessageEventBase
    {
    public:
        MessageEvent_CommandReset(std::string linkid = "") : MessageEventBase(MessageId::CommandReset, linkid) {}
    };

    class MessageEvent_CommandUnlink : public MessageEventBase
    {
    public:
        MessageEvent_CommandUnlink(std::string linkid = "") : MessageEventBase(MessageId::CommandUnlink, linkid) {}
    };

    //----------- Heartbeat message pair ------------------------------------------------------------------
    class MessageEvent_HeartbeatPing : public MessageEventBase
    {
    public:
        MessageEvent_HeartbeatPing(std::string linkid = "") : MessageEventBase(MessageId::HeartbeatPing, linkid) {}
    };

    class MessageEvent_HeartbeatPong : public MessageEventBase
    {
    public:
        MessageEvent_HeartbeatPong(std::string linkid = "") : MessageEventBase(MessageId::HeartbeatPong, linkid) {}
    };

    //------------- Notification Types ---------------------------------------------------------------------------------
    class MessageEvent_NotifyCommandResult : public MessageEventBase
    {
    public:
        MessageEvent_NotifyCommandResult(MessageId cmdtype, ResultCode code, std::string description = "", std::string linkid = "")
            : MessageEventBase(MessageId::NotifyCommandResult, linkid)
            , _cmdtype(cmdtype)
            , _code(code)
            , _description(std::move(description))
        {}
        MessageId cmdtype() const { return _cmdtype; }
        ResultCode code() const { return _code; }
        std::string description() const { return _description; }
    private:
        MessageId _cmdtype;
        ResultCode _code;
        std::string _description;
    };

    class MessageEvent_NotifyError : public MessageEventBase
    {
    public:
        MessageEvent_NotifyError(ResultCode code, std::string description, std::string linkid = "")
            : MessageEventBase(MessageId::NotifyError, linkid)
            , _code(code)
            , _description(std::move(description))
        {}
        ResultCode code() const { return _code; }
        std::string description() const { return _description; }
    private:
        ResultCode _code;
        std::string _description;
    };

    //----------------- Shell/MMI request type : these requests are from MMI to Manager and handle by Manager only --------------
    // ShellReqCommand is a wrapper for command message requested by shell/MMI
    class ShellReq_Command : public MessageEventBase
    {
    public:
        ShellReq_Command(MessageId reqid, uint8_t parameter, std::string linkid)
            : MessageEventBase(MessageId::ShellReqCommand, linkid)
            , _reqid(reqid)
            , _parameter(parameter)
        {}
        MessageId reqid() const { return _reqid; }
        uint8_t parameter() const { return _parameter; }
    private:
        MessageId _reqid;
        uint8_t _parameter;
    };

    class ShellReq_LinkSetup : public MessageEventBase
    {
    public:
        ShellReq_LinkSetup(std::string authkey, OpMode opmode, MachineModel model, std::string linkid)
            : MessageEventBase(MessageId::ShellReqLinkSetup, linkid)
            , _authkey(authkey)
            , _opmode(opmode)
            , _model(model)
        {}
        std::string authkey() const { return _authkey; }
        OpMode opmode() const { return _opmode; }
        MachineModel model() const { return _model; }
    private:
        std::string _authkey;
        OpMode _opmode;
        MachineModel _model;
    };

    //----------------- System Information Get/Reply pair : from Shell/Chief to get info from Manager -----------------------
    class MessageEvent_GetSystemStatus : public MessageEventBase
    {
    public:
        MessageEvent_GetSystemStatus(std::string linkid = "") : MessageEventBase(MessageId::GetSystemStatus, linkid) {}
    };

    class MessageEvent_ReplySystemStatus : public MessageEventBase
    {
    public:
        MessageEvent_ReplySystemStatus(uint8_t state, OpMode opmode, std::string linkid = "")
            : MessageEventBase(MessageId::ReplySystemStatus, linkid)
            , _state(state)
            , _opmode(opmode)
        {}
        uint8_t state() const { return _state; }
        OpMode opmode() const { return _opmode; }
    private:
        uint8_t _state;
        OpMode _opmode;
    };

    //--------------- Version info Get/Reply pair : from Shell/Chief to Manager and propagate to all workers ----------------
    class MessageEvent_GetVersion : public MessageEventBase
    {
    public:
        MessageEvent_GetVersion(std::string linkid = "") : MessageEventBase(MessageId::GetVersion, linkid) {}
    };

    class MessageEvent_ReplyVersion : public MessageEventBase
    {
    public:
        MessageEvent_ReplyVersion(std::string version, std::string linkid = "")
            : MessageEventBase(MessageId::ReplyVersion, linkid)
            , _version(version)
        {}
        std::string version() const { return _version; }
    private:
        std::string _version;
    };

    //--------------------- Setup Types : from Shell pass-thru Manager to Workers ------------------------------------
    class MessageEvent_SetOutputDevice : public MessageEventBase
    {
    public:
        MessageEvent_SetOutputDevice(uint32_t device, uint8_t ioswitch, std::string linkid)
            : MessageEventBase(MessageId::SetOutputDevice, linkid)
            , _device(device)
            , _ioswitch(ioswitch)
        {}
        uint32_t device() const { return _device; }
        uint8_t ioswitch() const { return _ioswitch; }
    private:
        uint32_t _device;
        uint8_t _ioswitch;
    };

    class MessageEvent_GetBigConveyorMinimumCycleTime : public MessageEventBase
    {
    public:
        MessageEvent_GetBigConveyorMinimumCycleTime(std::string linkid = "") : MessageEventBase(MessageId::GetBigConveyorMinimumCycleTime, linkid) {}
    };

    class MessageEvent_GetOvenConveyorLengthTime : public MessageEventBase
    {
    public:
        MessageEvent_GetOvenConveyorLengthTime(uint8_t ovenid, std::string linkid = "")
            : MessageEventBase(MessageId::GetOvenConveyorLengthTime, linkid)
            , _ovenid(ovenid)
        {}
        uint8_t ovenid() const { return _ovenid; }
    private:
        uint8_t _ovenid;
    };

    class MessageEvent_SetStationLinkPrev : public MessageEventBase
    {
    public:
        MessageEvent_SetStationLinkPrev(LinkAction action, std::string linkid = "")
            : MessageEventBase(MessageId::SetStationLinkPrev, linkid)
            , _action(action)
        {}
        LinkAction action() const { return _action; }
    private:
        LinkAction _action;
    };

	class MessageEvent_UpdateRecipeToSyncStation : public MessageEventBase
	{
	public:
		MessageEvent_UpdateRecipeToSyncStation(std::string soletype, std::vector<std::string> settings, std::string linkid = "")
			: MessageEventBase(MessageId::UpdateRecipeToSyncStation, linkid)
			, _soletype(std::move(soletype))
			, _settings(std::move(settings))
		{}
		std::string soletype() const { return _soletype; }
		std::vector<std::string> settings() const { return _settings; }
	private:
		std::string _soletype;
		std::vector<std::string> _settings;
	};

    class MessageEvent_SetClearSoleInProgress : public MessageEventBase
    {
    public:
        MessageEvent_SetClearSoleInProgress(bool doclear, std::string linkid = "")
            : MessageEventBase(MessageId::SetClearSoleInProgress, linkid)
            , _doclear(doclear)
        {}
        bool doclear() const { return _doclear; }
    private:
        bool _doclear;
    };

	class MessageEvent_SetAutoExposure : public MessageEventBase
	{
	public:
		MessageEvent_SetAutoExposure(bool autoexposure, std::string linkid = "")
			: MessageEventBase(MessageId::SetAutoExposure, linkid)
			, _autoexposure(autoexposure)
		{}
		bool autoexposure() const { return _autoexposure; }
	private:
		bool _autoexposure;
	};

	class MessageEvent_RunFrontOvenConveyor : public MessageEventBase
	{
	public:
		MessageEvent_RunFrontOvenConveyor(std::string linkid = "")
			: MessageEventBase(MessageId::RunFrontOvenConveyor, linkid)
		{}
	};

    class MessageEvent_SetOvenConveyorTime : public MessageEventBase
    {
    public:
        MessageEvent_SetOvenConveyorTime(uint8_t ovenid, uint32_t second, uint32_t soledistance, std::string linkid = "")
            : MessageEventBase(MessageId::SetOvenConveyorTime, linkid)
            , _ovenid(ovenid)
            , _time(second)
            , _soledistance(soledistance)
        {}
        uint8_t ovenid() const { return _ovenid; }
        // return the time in the unit of second
        uint32_t time() const { return _time; }
        uint32_t soledistance() const { return _soledistance; }
    private:
        uint8_t _ovenid;
        uint32_t _time;
        uint32_t _soledistance;
    };

    class MessageEvent_ResetConveyorPausedTimeoutAlarm : public MessageEventBase
    {
    public:
        MessageEvent_ResetConveyorPausedTimeoutAlarm(std::string linkid = "") : MessageEventBase(MessageId::ResetConveyorPausedTimeoutAlarm, linkid) {}
    };

    class MessageEvent_SetSoleFeedingMode : public MessageEventBase
    {
    public:
        MessageEvent_SetSoleFeedingMode(Feeding mode, std::string linkid = "")
            : MessageEventBase(MessageId::SetSoleFeedingMode, linkid)
            , _mode(mode)
        {}
        Feeding mode() const { return _mode; }
    private:
        Feeding _mode;
    };

    class MessageEvent_SetBigConveyorCycleTime : public MessageEventBase
    {
    public:
        MessageEvent_SetBigConveyorCycleTime(uint32_t millisecond, std::string linkid = "")
            : MessageEventBase(MessageId::SetBigConveyorCycleTime, linkid)
            , _time(millisecond)
        {}
        // return the time in the unit of millisecond
        uint32_t time() const { return _time; }
    private:
        uint32_t _time;
    };

    class MessageEvent_SetBigConveyorCleaning : public MessageEventBase
    {
    public:
        MessageEvent_SetBigConveyorCleaning(Action action, std::string linkid = "")
            : MessageEventBase(MessageId::SetBigConveyorCleaning, linkid)
            , _action(action)
        {}
        Action action() const { return _action; }
    private:
        Action _action;
    };

    class MessageEvent_SetCalibrationLineID : public MessageEventBase
    {
    public:
        MessageEvent_SetCalibrationLineID(uint8_t lineid, std::string linkid = "")
            : MessageEventBase(MessageId::SetCalibrationLineID, linkid)
            , _lineid(lineid)
        {}
        uint8_t lineid() const { return _lineid; }
    private:
        uint8_t _lineid;
    };

    class MessageEvent_SetSliderMovingSpeed : public MessageEventBase
    {
    public:
        MessageEvent_SetSliderMovingSpeed(SlidingSpeed speed, std::string linkid = "")
            : MessageEventBase(MessageId::SetSliderMovingSpeed, linkid)
            , _speed(speed)
        {}
        SlidingSpeed speed() const { return _speed; }
    private:
        SlidingSpeed _speed;
    };

    class MessageEvent_SetSliderHoming : public MessageEventBase
    {
    public:
        MessageEvent_SetSliderHoming(std::string linkid = "") : MessageEventBase(MessageId::SetSliderHoming, linkid) {}
    };

	class MessageEvent_SetAlarmLevel : public MessageEventBase
	{
	public:
		MessageEvent_SetAlarmLevel(uint8_t level, std::string linkid)
			: MessageEventBase(MessageId::SetAlarmLevel, linkid)
			, _level(level)
		{}
		uint8_t level() const { return _level; }
	private:
		uint8_t _level;
	};

	class MessageEvent_A3_SetConveyorPosition : public MessageEventBase
	{
	public:
		// position_id: 0 - home position, 1 - spray position
		MessageEvent_A3_SetConveyorPosition(uint8_t position_id, std::string linkid)
			: MessageEventBase(MessageId::A3_SetConveyorPosition, linkid)
			, _position_id(position_id)
		{}
		uint8_t position_id() const { return _position_id; }
	private:
		uint8_t _position_id;
	};

	class MessageEvent_SetConveyorMaintainGun : public MessageEventBase
	{
	public:
		MessageEvent_SetConveyorMaintainGun(MaintainGun status, std::string linkid = "")
			: MessageEventBase(MessageId::SetConveyorMaintainGun, linkid)
			, _status(status)
		{}
		MaintainGun status() const { return _status; }
	private:
		MaintainGun _status;
	};

	class MessageEvent_SetRobotActionToConveyor : public MessageEventBase
	{
	public:
		MessageEvent_SetRobotActionToConveyor(RobotAction action, std::string linkid = "")
			: MessageEventBase(MessageId::SetRobotActionToConveyor, linkid)
			, _action(action)
		{}
		RobotAction action() const { return _action; }
	private:
		RobotAction _action;
	};

	class MessageEvent_SetDioMeasureGlueAmount : public MessageEventBase
	{
	public:
		MessageEvent_SetDioMeasureGlueAmount(DioMeasureGunAmountType measure_type, uint32_t seconds, std::string linkid = "")
			: MessageEventBase(MessageId::SetDioMeasureGlueAmount, linkid)
			,_measure_type(measure_type)
			,_seconds(seconds)
		{}
		DioMeasureGunAmountType measure_type() const { return _measure_type; }
		uint32_t seconds() const { return _seconds; }
	private:
		DioMeasureGunAmountType _measure_type;
		uint32_t _seconds;
	};

    class MessageEvent_SetZoriginAlignment : public MessageEventBase
    {
    public:
        MessageEvent_SetZoriginAlignment(std::string linkid = "") : MessageEventBase(MessageId::SetZoriginAlignment, linkid) {}
    };

    class MessageEvent_SetScannerParameter : public MessageEventBase
    {
    public:
        MessageEvent_SetScannerParameter(ScanParameter parameter, std::string value, std::string linkid = "")
            : MessageEventBase(MessageId::SetScannerParameter, linkid)
            , _parameter(parameter)
            , _value(std::move(value))
        {}
        ScanParameter parameter() const { return _parameter; }
        std::string value() const { return _value; }
    private:
        ScanParameter _parameter;
        std::string _value;
    };

    class MessageEvent_SetVisionRecipe : public MessageEventBase
    {
    public:
        MessageEvent_SetVisionRecipe(VisionRecipe parameter, std::string value, std::string linkid = "")
            : MessageEventBase(MessageId::SetVisionRecipe, linkid)
            , _parameter(parameter)
            , _value(std::move(value))
        {}
        VisionRecipe parameter() const { return _parameter; }
        std::string value() const { return _value; }
    private:
        VisionRecipe _parameter;
        std::string _value;
    };

	class MessageEvent_SetAutoSoleType : public MessageEventBase
	{
	public:
		MessageEvent_SetAutoSoleType(bool autosoletype, std::string linkid = "")
			: MessageEventBase(MessageId::SetAutoSoleType, linkid)
			, _autosoletype(autosoletype)
		{}
		bool autosoletype() const { return _autosoletype; }
	private:
		bool _autosoletype;
	};

	class MessageEvent_SetUseCustomizedPath : public MessageEventBase
	{
	public:
		MessageEvent_SetUseCustomizedPath(bool usecustomizedpath, std::string soletypename, std::string linkid = "")
			: MessageEventBase(MessageId::SetUseCustomizedPath, linkid)
			, _usecustomizedpath(usecustomizedpath)
			, _soletypename(soletypename)
		{}
		bool usecustomizedpath() const { return _usecustomizedpath; }
		std::string soletypename() const { return _soletypename; }
	private:
		bool _usecustomizedpath;
		std::string _soletypename;
	};

	class MessageEvent_SetUseAutoIfPatternNotFound : public MessageEventBase
	{
	public:
		MessageEvent_SetUseAutoIfPatternNotFound(bool useauto, std::string linkid = "")
			: MessageEventBase(MessageId::SetUseAutoIfPatternNotFound, linkid)
			, _useauto(useauto)
		{}
		bool useauto() const { return _useauto; }
	private:
		bool _useauto;
	};

	class MessageEvent_SetUsingCommonSpraySettingForVision : public MessageEventBase
	{
	public:
		MessageEvent_SetUsingCommonSpraySettingForVision(bool commonspraysetting, std::string linkid = "")
			: MessageEventBase(MessageId::SetUsingCommonSpraySettingForVision, linkid)
			, _commonspraysetting(commonspraysetting)
		{}
		bool commonspraysetting() const { return _commonspraysetting; }
	private:
		bool _commonspraysetting;
	};

	class MessageEvent_SetUsingCustomizedPathAlgorithmForVision : public MessageEventBase
	{
	public:
		MessageEvent_SetUsingCustomizedPathAlgorithmForVision(CustomizedPathAlgorithmType type, std::string linkid = "")
			: MessageEventBase(MessageId::SetUsingCustomizedPathAlgorithmForVision, linkid)
			, _type(type)
		{}
		CustomizedPathAlgorithmType getCustomizedPathAlgorithmType() const { return _type; }
	private:
		CustomizedPathAlgorithmType _type;
	};

    class MessageEvent_GetRobotPosition : public MessageEventBase
    {
    public:
        MessageEvent_GetRobotPosition(std::string linkid = "") : MessageEventBase(MessageId::GetRobotPosition, linkid) {}
    };

    class MessageEvent_GetSprayCounter : public MessageEventBase
    {
    public:
        MessageEvent_GetSprayCounter(std::string linkid = "") : MessageEventBase(MessageId::GetSprayCounter, linkid) {}
    };

    class MessageEvent_SetPathPlanningRecipe : public MessageEventBase
    {
    public:
        MessageEvent_SetPathPlanningRecipe(PathPlanningRecipe parameter, std::string value, std::string linkid = "")
            : MessageEventBase(MessageId::SetPathPlanningRecipe, linkid)
            , _parameter(parameter)
            , _value(std::move(value))
        {}
        PathPlanningRecipe parameter() const { return _parameter; }
        std::string value() const { return _value; }
    private:
        PathPlanningRecipe _parameter;
        std::string _value;
    };

    class MessageEvent_SetRobotHoming : public MessageEventBase
    {
    public:
        MessageEvent_SetRobotHoming(std::string linkid = "") : MessageEventBase(MessageId::SetRobotHoming, linkid) {}
    };

	class MessageEvent_SetPlasmaControl : public MessageEventBase
	{
	public:
		MessageEvent_SetPlasmaControl(PlasmaControlType type, PlasmaControlIO io, std::string linkid = "")
			: MessageEventBase(MessageId::SetPlasmaControl, linkid)
			, _type(type)
			, _io(io)
		{ }
		PlasmaControlType type() const { return _type; }
		PlasmaControlIO io() const { return _io; }
	private:
		PlasmaControlType _type;
		PlasmaControlIO _io;
	};

	class MessageEvent_SetRobotCollisionSimulation : public MessageEventBase
	{
	public:
		MessageEvent_SetRobotCollisionSimulation(std::string linkid = "") : MessageEventBase(MessageId::SetRobotCollisionSimulation, linkid) {}
	};

	class MessageEvent_SetRobotMeasureGlueAmount : public MessageEventBase
	{
	public:
		MessageEvent_SetRobotMeasureGlueAmount(RobotMeasureGunAmountType measure_type, uint32_t seconds, std::string linkid = "")
			: MessageEventBase(MessageId::SetRobotMeasureGlueAmount, linkid)
			, _measure_type(measure_type)
			, _seconds(seconds)
		{}
		RobotMeasureGunAmountType measure_type() const { return _measure_type; }
		uint32_t seconds() const { return _seconds; }
	private:
		RobotMeasureGunAmountType _measure_type;
		uint32_t _seconds;
	};

    class MessageEvent_SetCleanGun : public MessageEventBase
    {
    public:
        MessageEvent_SetCleanGun(std::string linkid = "") : MessageEventBase(MessageId::SetCleanGun, linkid) {}
    };

    class MessageEvent_SetSprayLine : public MessageEventBase
    {
    public:
        MessageEvent_SetSprayLine(bool firstloop, uint32_t millimeter, std::string linkid = "")
            : MessageEventBase(MessageId::SetSprayLine, linkid)
            , _firstloop(firstloop)
            , _height(millimeter)
        {}
        bool isFirstLoop() { return _firstloop; }
        // return the height for Gun in the unit of millimeter
        uint32_t height() const { return _height; }
    private:
        bool _firstloop;
        uint32_t _height;
    };

    class MessageEvent_SetRobotIdleTime : public MessageEventBase
    {
    public:
        MessageEvent_SetRobotIdleTime(uint32_t second, std::string linkid = "")
            : MessageEventBase(MessageId::SetRobotIdleTime, linkid)
            , _time(second)
        {}
        // return the time in the unit of second
        uint32_t time() const { return _time; }
    private:
        uint32_t _time;
    };

    class MessageEvent_SetRobotSprayCount : public MessageEventBase
    {
    public:
        MessageEvent_SetRobotSprayCount(uint32_t count, std::string linkid = "")
            : MessageEventBase(MessageId::SetRobotSprayCount, linkid)
            , _count(count)
        {}
        // return the number of time to trigger the gun cleaning feature
        uint32_t count() const { return _count; }
    private:
        uint32_t _count;
    };

    class MessageEvent_SetReleaseGlue : public MessageEventBase
    {
    public:
        MessageEvent_SetReleaseGlue(std::string linkid = "") : MessageEventBase(MessageId::SetReleaseGlue, linkid) {}
    };

    class MessageEvent_SetRobotMovingSpeed : public MessageEventBase
    {
    public:
        MessageEvent_SetRobotMovingSpeed(RobotSpeed speed, uint32_t edge_speed, uint32_t inside_speed, bool high_acc_edge_spray, std::string linkid = "")
            : MessageEventBase(MessageId::SetRobotMovingSpeed, linkid)
            , _speed(speed)
			, _edge_speed(edge_speed)
			, _inside_speed(inside_speed)
			, _high_acc_edge_spray(high_acc_edge_spray)
        {}
        RobotSpeed speed() const { return _speed; }
		uint32_t edge_speed() const { return _edge_speed; }
		uint32_t inside_speed() const { return _inside_speed; }
		bool high_acc_edge_spray() const { return _high_acc_edge_spray; }
    private:
        RobotSpeed _speed;
		uint32_t _edge_speed;
		uint32_t _inside_speed;
		bool _high_acc_edge_spray;
    };

    class MessageEvent_ResetDailySprayCounter : public MessageEventBase
    {
    public:
        MessageEvent_ResetDailySprayCounter(std::string linkid = "") : MessageEventBase(MessageId::ResetDailySprayCounter, linkid) {}
    };

    class MessageEvent_SetRobotAction : public MessageEventBase
    {
    public:
        MessageEvent_SetRobotAction(RobotAction action, std::string linkid = "")
            : MessageEventBase(MessageId::SetRobotAction, linkid)
            , _action(action)
        {}
        RobotAction action() const { return _action; }
    private:
        RobotAction _action;
    };

    class MessageEvent_SetRobotMove : public MessageEventBase
    {
    public:
        MessageEvent_SetRobotMove(RobotMove move, std::string linkid = "")
            : MessageEventBase(MessageId::SetRobotMove, linkid)
            , _move(move)
        {}
        RobotMove move() const { return _move; }
    private:
        RobotMove _move;
    };

	class MessageEvent_SetRobotSoakNozzle : public MessageEventBase
	{
	public:
		MessageEvent_SetRobotSoakNozzle(SoakNozzle status, std::string linkid = "")
			: MessageEventBase(MessageId::SetRobotSoakNozzle, linkid)
			, _status(status)
		{}
		SoakNozzle status() const { return _status; }
	private:
		SoakNozzle _status;
	};

	class MessageEvent_SetRobotMaintainGun : public MessageEventBase
	{
	public:
		MessageEvent_SetRobotMaintainGun(MaintainGun status, std::string linkid = "")
			: MessageEventBase(MessageId::SetRobotMaintainGun, linkid)
			, _status(status)
		{}
		MaintainGun status() const { return _status; }
	private:
		MaintainGun _status;
	};

	class MessageEvent_SetUsingCommonSpraySettingForRobot : public MessageEventBase
	{
	public:
		MessageEvent_SetUsingCommonSpraySettingForRobot(bool commonspraysetting, std::string linkid = "")
			: MessageEventBase(MessageId::SetUsingCommonSpraySettingForRobot, linkid)
			, _commonspraysetting(commonspraysetting)
		{}
		bool commonspraysetting() const { return _commonspraysetting; }
	private:
		bool _commonspraysetting;
	};

	class MessageEvent_SetUsingCustomizedPathAlgorithmForRobot : public MessageEventBase
	{
	public:
		MessageEvent_SetUsingCustomizedPathAlgorithmForRobot(CustomizedPathAlgorithmType type, std::string linkid = "")
			: MessageEventBase(MessageId::SetUsingCustomizedPathAlgorithmForRobot, linkid)
			, _type(type)
		{}
		CustomizedPathAlgorithmType getCustomizedPathAlgorithmType() const { return _type; }
	private:
		CustomizedPathAlgorithmType _type;
	};

    class MessageEvent_GetJigPointsError : public MessageEventBase
    {
    public:
        MessageEvent_GetJigPointsError(uint8_t lineid, std::string linkid = "") 
            : MessageEventBase(MessageId::GetJigPointsError, linkid) 
            , _lineid(lineid)
        {}
        uint8_t lineid() const { return _lineid; }
    private:
        uint8_t _lineid;
    };

    //--------------------- Notification type : from Workers pass-thru Manager to Shell ------------------------------------
	class MessageEvent_ReplyMeasureGlueAmount : public MessageEventBase
	{
	public:
		MessageEvent_ReplyMeasureGlueAmount(uint32_t amount, std::string linkid = "")
			: MessageEventBase(MessageId::ReplyMeasureGlueAmount, linkid)
			, _amount(amount)
		{}
		// return the time in the unit of millisecond
		uint32_t amount() const { return _amount; }
	private:
		uint32_t _amount; // unit in 1/10000
	};

	class MessageEvent_ReplyPlasmaControl : public MessageEventBase
	{
	public:
		MessageEvent_ReplyPlasmaControl(PlasmaControlType type, PlasmaControlIO io, std::string linkid = "")
			: MessageEventBase(MessageId::ReplyPlasmaControl, linkid)
			, _type(type)
			, _io(io)
		{}
		PlasmaControlType type() const { return _type; }
		PlasmaControlIO io() const { return _io; }
	private:
		PlasmaControlType _type;
		PlasmaControlIO _io;
	};

	class MessageEvent_NotifyCustomizedPathAlgorithmContour : public MessageEventBase
	{
	public:
		MessageEvent_NotifyCustomizedPathAlgorithmContour(uint8_t lineid, std::vector<Point3d> contour, std::string linkid = "")
			: MessageEventBase(MessageId::NotifyCustomizedPathAlgorithmContour, linkid)
			, _lineid(lineid)
			, _contour(std::move(contour))
		{}
		uint8_t lineid() const { return _lineid; }
		const std::vector<Point3d> & contour() const { return _contour; }
	private:
		uint8_t _lineid;
		std::vector<Point3d> _contour;
	};

    class MessageEvent_ReplyBigConveyorMinimumCycleTime : public MessageEventBase
    {
    public:
        MessageEvent_ReplyBigConveyorMinimumCycleTime(uint32_t millisecond, std::string linkid = "")
            : MessageEventBase(MessageId::ReplyBigConveyorMinimumCycleTime, linkid)
            , _millisecond(millisecond)
        {}
        // return the time in the unit of millisecond
        uint32_t time() const { return _millisecond; }
    private:
        uint32_t _millisecond;
    };

    class MessageEvent_ReplyOvenConveyorLengthTime : public MessageEventBase
    {
    public:
        MessageEvent_ReplyOvenConveyorLengthTime(uint8_t ovenid, uint32_t centimeter, uint32_t second, uint32_t soledistance, std::string linkid = "")
            : MessageEventBase(MessageId::ReplyOvenConveyorLengthTime, linkid)
            , _ovenid(ovenid)
            , _length(centimeter)
            , _time(second)
            , _soledistance(soledistance)
        {}
        // return the oven id
        uint8_t ovenid() const { return _ovenid; }
        // return the length in the unit of centimeter
        uint32_t length() const { return _length; }
        // return the time in the unit of second
        uint32_t time() const { return _time; }
        // return the minimum sole distance in oven in the unit of mm
        uint32_t soledistance() const { return _soledistance; }
    private:
        uint8_t _ovenid;
        uint32_t _length;
        uint32_t _time;
        uint32_t _soledistance;
    };

    class MessageEvent_ReplyRobotPosition : public MessageEventBase
    {
    public:
        MessageEvent_ReplyRobotPosition(Point3d position, std::string linkid = "")
            : MessageEventBase(MessageId::ReplyRobotPosition, linkid)
            , _position(position)
        {}
        const Point3d & position() const { return _position; }
    private:
        Point3d _position;
    };

    class MessageEvent_ReplySprayCounter : public MessageEventBase
    {
    public:
        MessageEvent_ReplySprayCounter(uint32_t daily, uint32_t total, uint32_t current, std::string linkid = "")
            : MessageEventBase(MessageId::ReplySprayCounter, linkid)
            , _daily(daily)
            , _total(total)
            ,_current(current)
        {}
        uint32_t daily() const { return _daily; }
        uint32_t total() const { return _total; }
        uint32_t current() const { return _current; }

    private:
        uint32_t _daily;
        uint32_t _total;
        uint32_t _current;
    };

    class MessageEvent_ReplyJigPointsError : public MessageEventBase
    {
    public:
        MessageEvent_ReplyJigPointsError(uint8_t lineid, std::vector<double> errors, std::string linkid = "")
            : MessageEventBase(MessageId::ReplyJigPointsError, linkid)
            , _lineid(lineid)
            , _errors(std::move(errors))
        {}
        uint8_t lineid() const { return _lineid; }
        const std::vector<double> & errors() const { return _errors; }
    private:
        uint8_t _lineid;
        std::vector<double> _errors;
    }; 

    class MessageEvent_NotifySetBigConveyorCycleTimeToMinimum : public MessageEventBase
    {
    public:
        MessageEvent_NotifySetBigConveyorCycleTimeToMinimum(uint32_t millisecond, std::string linkid = "")
            : MessageEventBase(MessageId::NotifySetBigConveyorCycleTimeToMinimum, linkid)
            , _millisecond(millisecond)
        {}
        uint32_t time() const { return _millisecond; }
    private:
        uint32_t _millisecond;
    };

    class MessageEvent_NotifyStationLinkFromNext : public MessageEventBase
    {
    public:
        MessageEvent_NotifyStationLinkFromNext(LinkAction action, std::string linkid = "")
            : MessageEventBase(MessageId::NotifyStationLinkFromNext, linkid)
            , _action(action)
        {}
        LinkAction action() const { return _action; }
    private:
        LinkAction _action;
    };

    class MessageEvent_NotifyStationLinkFromPrev : public MessageEventBase
    {
    public:
        MessageEvent_NotifyStationLinkFromPrev(LinkAction action, std::string linkid = "")
            : MessageEventBase(MessageId::NotifyStationLinkFromPrev, linkid)
            , _action(action)
        {}
        LinkAction action() const { return _action; }
    private:
        LinkAction _action;
    };

	class MessageEvent_NotifyStationUpdateRecipe : public MessageEventBase
	{
	public:
		MessageEvent_NotifyStationUpdateRecipe(std::string soletype, std::vector<std::string> settings, std::string linkid = "")
			: MessageEventBase(MessageId::NotifyStationUpdateRecipe, linkid)
			, _soletype(std::move(soletype))
			, _settings(std::move(settings))
		{}
		const std::string & soletype() const { return _soletype; }
		const std::vector<std::string> & settings() const { return _settings; }
	private:
		std::string _soletype;
		std::vector<std::string> _settings;
	};

	class MessageEvent_NotifyStationUpdateRecipeOK : public MessageEventBase
	{
	public:
		MessageEvent_NotifyStationUpdateRecipeOK(std::string linkid = "") : MessageEventBase(MessageId::NotifyStationUpdateRecipeOK, linkid) {}
	};

	class MessageEvent_NotifyStationUpdateRecipeFailure : public MessageEventBase
	{
	public:
		MessageEvent_NotifyStationUpdateRecipeFailure(std::string linkid = "") : MessageEventBase(MessageId::NotifyStationUpdateRecipeFailure, linkid) {}
	};

    class MessageEvent_NotifyConveyorPausedTimeoutAlarm : public MessageEventBase
    {
    public:
        MessageEvent_NotifyConveyorPausedTimeoutAlarm(std::string linkid = "") : MessageEventBase(MessageId::NotifyConveyorPausedTimeoutAlarm, linkid) {}
    };

    class MessageEvent_NotifyResetConveyorPausedTimeoutAlarmByNext : public MessageEventBase
    {
    public:
        MessageEvent_NotifyResetConveyorPausedTimeoutAlarmByNext(std::string linkid = "") : MessageEventBase(MessageId::NotifyResetConveyorPausedTimeoutAlarmByNext, linkid) {}
    };

    class MessageEvent_NotifyJigPoints : public MessageEventBase
    {
    public:
        MessageEvent_NotifyJigPoints(uint8_t lineid, std::vector<Point3d> jigpoints, std::string linkid = "")
            : MessageEventBase(MessageId::NotifyJigPoints, linkid)
            , _lineid(lineid)
            , _jigpoints(std::move(jigpoints))
        {}
        uint8_t lineid() const { return _lineid; }
        const std::vector<Point3d> & points() const { return _jigpoints; }
    private:
        uint8_t _lineid;
        std::vector<Point3d> _jigpoints;
    };

    class MessageEvent_NotifySoleContour : public MessageEventBase
    {
    public:
        MessageEvent_NotifySoleContour(uint8_t lineid, std::vector<Point3d> contour, std::string sole_type_name, double contour_length, bool left_sole, std::string linkid = "")
            : MessageEventBase(MessageId::NotifySoleContour, linkid)
            , _lineid(lineid)
            , _contour(std::move(contour))
			, _sole_type_name(sole_type_name)
			, _contour_length(contour_length)
			, _left_sole(left_sole)
        {}
        uint8_t lineid() const { return _lineid; }
        const std::vector<Point3d> & contour() const { return _contour; }
		std::string sole_type_name() const { return _sole_type_name; }
		double contour_length() const { return _contour_length; }
		bool left_sole() const { return _left_sole; }
    private:
        uint8_t _lineid;
        std::vector<Point3d> _contour;
		std::string _sole_type_name;
		double _contour_length;
		bool _left_sole;
    };

    class MessageEvent_NotifyJobTaken : public MessageEventBase
    {
    public:
        MessageEvent_NotifyJobTaken(uint8_t workerid, uint8_t lineid, std::string jobid, std::string linkid = "")
            : MessageEventBase(MessageId::NotifyJobTaken, linkid)
            , _workerid(workerid)
            , _lineid(lineid)
            , _jobid(std::move(jobid))
        {}
        uint8_t workerid() const { return _workerid; }
        uint8_t lineid() const { return _lineid; }
        const std::string & jobid() const { return _jobid; }
    private:
        uint8_t _workerid;
        uint8_t _lineid;
        std::string _jobid;
    };

    class MessageEvent_NotifyUncheckClearSole : public MessageEventBase
    {
    public:
        MessageEvent_NotifyUncheckClearSole(std::string linkid = "") : MessageEventBase(MessageId::NotifyUncheckClearSole, linkid) {}
    };

    class MessageEvent_NotifyZoriginAlignmentDone : public MessageEventBase
    {
    public:
        MessageEvent_NotifyZoriginAlignmentDone(uint8_t lineid, std::string linkid = "") : MessageEventBase(MessageId::NotifyZoriginAlignmentDone, linkid) 
        , _lineid(lineid)
        {}
        uint8_t lineid() const { return _lineid; }
    private:
        uint8_t _lineid;
    };

    /*************************************************************************************
     * Helper funtions
     *************************************************************************************/
     // make OMLP network identity string
    inline std::string makeIdentity(HeaderField id)
    {
        return std::string({
            (char)HeaderField::Signature,
            (char)HeaderField::Version,
            (char)HeaderField::Padding,
            (char)id
        });
    }

    // claim the OMLP network identity
    inline void claimIdentity(zmq::socket_t & socket, HeaderField id)
    {
        const char headline[HEADER_LENGTH] = {
            (char)HeaderField::Signature,
            (char)HeaderField::Version,
            (char)HeaderField::Padding,
            (char)id
        };
        socket.setsockopt(ZMQ_IDENTITY, &headline, HEADER_LENGTH);
    }

    // mapping workers ID to string Name
    inline std::string getWorkerName(HeaderField id)
    {
        switch (id)
        {
        // Shell is not a Worker, display its name just for logging convenience
        case HeaderField::IdShell:
            return std::string{ "Shell" };
        case HeaderField::IdDio:
            return std::string{ "Dio" };
        case HeaderField::IdSlider:
            return std::string{ "Slider" };
        case HeaderField::IdScanner:
            return std::string{ "Scanner" };
        case HeaderField::IdConveyor:
            return std::string{ "Conveyor" };
        case HeaderField::IdVision:
            return std::string{ "Vision" };
        case HeaderField::IdRobot:
            return std::string{ "Robot" };
        case HeaderField::IdDioMotion:
            return std::string{ "DioMotion" };
        default:
            return std::string{ "" };
        }
    }

    inline std::string getWorkerNameByLinkid(std::string & linkid)
    {
        return getWorkerName((HeaderField)linkid[3]);
    }

    // mapping workers Name to ID
    inline HeaderField getWorkerId(std::string & name)
    {
        if (name == "Dio")
            return HeaderField::IdDio;
        else if (name == "Slider")
            return HeaderField::IdSlider;
        else if (name == "Scanner")
            return HeaderField::IdScanner;
        else if (name == "Conveyor")
            return HeaderField::IdConveyor;
        else if (name == "Vision")
            return HeaderField::IdVision;
        else if (name == "Robot")
            return HeaderField::IdRobot;
        else if (name == "DioMotion")
            return HeaderField::IdDioMotion;
        else
            return HeaderField::NotExist;
    }

    // Check the message type byte. Note: for router socket, pop out the header string before this call.
    inline MessageId checkMessageType(zmq::multipart_t & message)
    {
        if (message.empty())
            return MessageId::InvalidType;

        try
        {
            uint8_t msgid = message.poptyp<uint8_t>();
            if (msgid < (uint8_t)MessageId::InvalidType)
                return (MessageId)msgid;
            else
                return MessageId::InvalidType;
        }
        catch (...)
        {
            return MessageId::InvalidType;
        }
    }

    // return an incoming message notification type according to message id
    inline Poco::Notification* makeIncomingNotification(zmq::multipart_t & message, MessageId msgtype, std::string linkid = "")
    {
        switch (msgtype)
        {
        //---------------------------------- Notification Acknowledgement type -------------------------------------------
        case MessageId::Ack:
        {
            if (message.size() < 1)
                return nullptr;
            try
            {
                MessageId msgid2ack = (MessageId)message.poptyp<uint8_t>();
                return new MessageEvent_Ack(msgid2ack, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }

        case MessageId::Nack:
        {
            if (message.size() < 2)
                return nullptr;
            try
            {
                MessageId msgid2nack = (MessageId)message.poptyp<uint8_t>();
                std::string strerror = message.popstr();
                return new MessageEvent_Nack(msgid2nack, strerror, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }

        //---------------------------------- System Command type -------------------------------------------
        case MessageId::CommandLink:
            return new MessageEvent_CommandLink(linkid);

        case MessageId::CommandInit:
        {
            if (message.size() < 2)
                return nullptr;
            try
            {
                OpMode opmode = (OpMode)message.poptyp<uint8_t>();
                MachineModel model = (MachineModel)message.poptyp<uint8_t>();
                return new MessageEvent_CommandInit(opmode, model, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }

        case MessageId::CommandRun:
            return new MessageEvent_CommandRun(linkid);

        case MessageId::CommandStop:
        {
            if (message.size() < 1)
                return nullptr;
            try
            {
                Tone tone = (Tone)message.poptyp<uint8_t>();
                return new MessageEvent_CommandStop(tone, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }

        case MessageId::CommandHalt:
        {
            if (message.size() < 1)
                return nullptr;
            try
            {
                Tone tone = (Tone)message.poptyp<uint8_t>();
                return new MessageEvent_CommandHalt(tone, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }

        case MessageId::CommandResume:
            return new MessageEvent_CommandResume(linkid);

        case MessageId::CommandReset:
            return new MessageEvent_CommandReset(linkid);

        case MessageId::CommandUnlink:
            return new MessageEvent_CommandUnlink(linkid);

        //---------------------------------- Heartbeat message pair -------------------------------------------
        case MessageId::HeartbeatPing:
            return new MessageEvent_HeartbeatPing(linkid);

        case MessageId::HeartbeatPong:
            return new MessageEvent_HeartbeatPong(linkid);

        //------------------------- Notification type : one-way system-wide general notification ---------------------
        case MessageId::NotifyCommandResult:
        {
            if (message.size() < 3)
                return nullptr;
            try
            {
                MessageId cmdtype = (MessageId)message.poptyp<uint8_t>();
                ResultCode code = (ResultCode)message.poptyp<int8_t>();
                std::string description = message.popstr();
                return new MessageEvent_NotifyCommandResult(cmdtype, code, description, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }

        case MessageId::NotifyError:
        {
            if (message.size() < 2)
                return nullptr;
            try
            {
                ResultCode code = (ResultCode)message.poptyp<int8_t>();
                std::string description = message.popstr();
                return new MessageEvent_NotifyError(code, description, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }

        //----------------------------------- Shell/MMI request type ------------------------------------
        case MessageId::ShellReqLinkSetup:
        {
            if (message.size() < 3)
                return nullptr;
            try
            {
                std::string authkey = message.popstr();
                OpMode opmode = (OpMode)message.poptyp<uint8_t>();
                MachineModel model = (MachineModel)message.poptyp<uint8_t>();
                return new ShellReq_LinkSetup(authkey, opmode, model, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }

        //-----------------------------------System Information Get/Reply pair ------------------------------------
        case MessageId::GetSystemStatus:
            return new MessageEvent_GetSystemStatus(linkid);

        case MessageId::ReplySystemStatus:
        {
            if (message.size() < 2)
                return nullptr;
            try
            {
                uint8_t state = message.poptyp<uint8_t>();
                OpMode opmode = (OpMode)message.poptyp<uint8_t>();
                return new MessageEvent_ReplySystemStatus(state, opmode, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }

        //------------------------------------ Version info Get/Reply pair ----------------------------------------
        case MessageId::GetVersion:
            return new MessageEvent_GetVersion(linkid);
        
        case MessageId::ReplyVersion:
        {
            if (message.size() < 1)
                return nullptr;
            try
            {
                std::string version = message.popstr();
                return new MessageEvent_ReplyVersion(version, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }

        //--------------------- Setup Types : from Shell pass-thru Manager to Workers ------------------------------------
        case MessageId::SetOutputDevice:
        {
            if (message.size() < 2)
                return nullptr;
            try
            {
                uint32_t device = message.poptyp<uint32_t>();
                uint8_t ioswitch = message.poptyp<uint8_t>();
                return new MessageEvent_SetOutputDevice(device, ioswitch, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }

        case MessageId::GetBigConveyorMinimumCycleTime:
            return new MessageEvent_GetBigConveyorMinimumCycleTime(linkid);

        case MessageId::GetOvenConveyorLengthTime:
        {
            if (message.size() < 1)
                return nullptr;
            try
            {
                uint8_t ovenid = message.poptyp<uint8_t>();
                return new MessageEvent_GetOvenConveyorLengthTime(ovenid, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }

        case MessageId::SetStationLinkPrev:
        {
            if (message.size() < 1)
                return nullptr;
            try
            {
                LinkAction action = (LinkAction)message.poptyp<uint8_t>();
                return new MessageEvent_SetStationLinkPrev(action, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }

		case MessageId::UpdateRecipeToSyncStation:
		{
			if (message.size() < 2)
				return nullptr;
			try
			{
				std::string soletype = message.popstr();
				uint32_t items = message.poptyp<uint32_t>();
				std::vector<std::string> settings;
				for (uint32_t i = 0; i < items; i++) {
					std::string value = message.popstr();
					settings.push_back(value);
				}
				return new MessageEvent_UpdateRecipeToSyncStation(soletype, settings, linkid);
			}
			catch (...)
			{
				return nullptr;
			}
		}

        case MessageId::SetClearSoleInProgress:
        {
            if (message.size() < 1)
                return nullptr;
            try
            {
                bool doclear = message.poptyp<uint8_t>() > 0;
                return new MessageEvent_SetClearSoleInProgress(doclear, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }

		case MessageId::SetAutoExposure:
		{
			if (message.size() < 1)
				return nullptr;
			try
			{
				bool autoexposure = message.poptyp<uint8_t>() > 0;
				return new MessageEvent_SetAutoExposure(autoexposure, linkid);
			}
			catch (...)
			{
				return nullptr;
			}
		}

		case MessageId::RunFrontOvenConveyor:
			return new MessageEvent_RunFrontOvenConveyor(linkid);

        case MessageId::SetOvenConveyorTime:
        {
            if (message.size() < 3)
                return nullptr;
            try
            {
                uint8_t ovenid = message.poptyp<uint8_t>();
                uint32_t second = message.poptyp<uint32_t>();
                uint32_t soledistance = message.poptyp<uint32_t>();
                return new MessageEvent_SetOvenConveyorTime(ovenid, second, soledistance, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }

        case MessageId::ResetConveyorPausedTimeoutAlarm:
            return new MessageEvent_ResetConveyorPausedTimeoutAlarm(linkid);

        case MessageId::SetSoleFeedingMode:
        {
            if (message.size() < 1)
                return nullptr;
            try
            {
                Feeding mode = (Feeding)message.poptyp<uint8_t>();
                return new MessageEvent_SetSoleFeedingMode(mode, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }

        case MessageId::SetBigConveyorCycleTime:
        {
            if (message.size() < 1)
                return nullptr;
            try
            {
                uint32_t millisecond = message.poptyp<uint32_t>();
                return new MessageEvent_SetBigConveyorCycleTime(millisecond, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }

        case MessageId::SetBigConveyorCleaning:
        {
            if (message.size() < 1)
                return nullptr;
            try
            {
                Action action = (Action)message.poptyp<uint8_t>();
                return new MessageEvent_SetBigConveyorCleaning(action, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }

        case MessageId::SetCalibrationLineID:
        {
            if (message.size() < 1)
                return nullptr;
            try
            {
                uint8_t lineid = message.poptyp<uint8_t>();
                return new MessageEvent_SetCalibrationLineID(lineid, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }

        case MessageId::SetSliderMovingSpeed:
        {
            if (message.size() < 1)
                return nullptr;
            try
            {
                SlidingSpeed speed = (SlidingSpeed)message.poptyp<uint8_t>();
                return new MessageEvent_SetSliderMovingSpeed(speed, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }

        case MessageId::SetSliderHoming:
            return new MessageEvent_SetSliderHoming(linkid);

		case MessageId::SetAlarmLevel:
		{
			if (message.size() < 1)
				return nullptr;
			try
			{
				uint8_t level = message.poptyp<uint8_t>();
				return new MessageEvent_SetAlarmLevel(level, linkid);
			}
			catch (...)
			{
				return nullptr;
			}
		}

		case MessageId::A3_SetConveyorPosition:
		{
			if (message.size() < 1)
				return nullptr;
			try
			{
				uint8_t position_id = message.poptyp<uint8_t>();
				return new MessageEvent_A3_SetConveyorPosition(position_id, linkid);
			}
			catch (...)
			{
				return nullptr;
			}
		}

		case MessageId::SetConveyorMaintainGun:
		{
			if (message.size() < 1)
				return nullptr;
			try
			{
				MaintainGun maintain = (MaintainGun)message.poptyp<uint8_t>();
				return new MessageEvent_SetConveyorMaintainGun(maintain, linkid);
			}
			catch (...)
			{
				return nullptr;
			}
		}

		case MessageId::SetRobotActionToConveyor:
		{
			if (message.size() < 1)
				return nullptr;
			try
			{
				RobotAction action = (RobotAction)message.poptyp<uint8_t>();
				return new MessageEvent_SetRobotActionToConveyor(action, linkid);
			}
			catch (...)
			{
				return nullptr;
			}
		}
		

		case MessageId::SetDioMeasureGlueAmount:
		{
			if (message.size() < 2)
				return nullptr;
			try
			{
				DioMeasureGunAmountType measure_type = (DioMeasureGunAmountType)message.poptyp<uint8_t>();
				uint32_t seconds = message.poptyp<uint32_t>();
				return new MessageEvent_SetDioMeasureGlueAmount(measure_type, seconds, linkid);
			}
			catch (...)
			{
				return nullptr;
			}
		}

        case MessageId::SetZoriginAlignment:
            return new MessageEvent_SetZoriginAlignment(linkid);

        case MessageId::SetScannerParameter:
        {
            if (message.size() < 2)
                return nullptr;
            try
            {
                ScanParameter parameter = (ScanParameter)message.poptyp<uint8_t>();
                std::string value = message.popstr();
                return new MessageEvent_SetScannerParameter(parameter, value, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }

        case MessageId::SetVisionRecipe:
        {
            if (message.size() < 2)
                return nullptr;
            try
            {
                VisionRecipe parameter = (VisionRecipe)message.poptyp<uint8_t>();
                std::string value = message.popstr();
                return new MessageEvent_SetVisionRecipe(parameter, value, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }

		case MessageId::SetAutoSoleType:
		{
			if (message.size() < 1)
				return nullptr;
			try
			{
				bool autosoletype = message.poptyp<uint8_t>() > 0;
				return new MessageEvent_SetAutoSoleType(autosoletype, linkid);
			}
			catch (...)
			{
				return nullptr;
			}
		}

		case MessageId::SetUseCustomizedPath:
		{
			if (message.size() < 2)
				return nullptr;
			try
			{
				bool usecustomizedpath = message.poptyp<uint8_t>() > 0;
				std::string soletypename = message.popstr();
				return new MessageEvent_SetUseCustomizedPath(usecustomizedpath, soletypename, linkid);
			}
			catch (...)
			{
				return nullptr;
			}
		}

		case MessageId::SetUseAutoIfPatternNotFound:
		{
			if (message.size() < 1)
				return nullptr;
			try
			{
				bool useauto = message.poptyp<uint8_t>() > 0;
				return new MessageEvent_SetUseAutoIfPatternNotFound(useauto, linkid);
			}
			catch (...)
			{
				return nullptr;
			}
		}

		case MessageId::SetUsingCommonSpraySettingForVision:
		{
			if (message.size() < 1)
				return nullptr;
			try
			{
				bool commonspraysetting = message.poptyp<uint8_t>() > 0;
				return new MessageEvent_SetUsingCommonSpraySettingForVision(commonspraysetting, linkid);
			}
			catch (...)
			{
				return nullptr;
			}
		}

		case MessageId::SetUsingCustomizedPathAlgorithmForVision:
		{
			if (message.size() < 1)
				return nullptr;
			try
			{
				CustomizedPathAlgorithmType type = (CustomizedPathAlgorithmType)message.poptyp<int32_t>();
				return new MessageEvent_SetUsingCustomizedPathAlgorithmForVision(type, linkid);
			}
			catch (...)
			{
				return nullptr;
			}
		}

        case MessageId::GetRobotPosition:
            return new MessageEvent_GetRobotPosition(linkid);

        case MessageId::GetSprayCounter:
            return new MessageEvent_GetSprayCounter(linkid);

        case MessageId::SetPathPlanningRecipe:
        {
            if (message.size() < 2)
                return nullptr;
            try
            {
                PathPlanningRecipe parameter = (PathPlanningRecipe)message.poptyp<uint8_t>();
                std::string value = message.popstr();
                return new MessageEvent_SetPathPlanningRecipe(parameter, value, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }

        case MessageId::SetRobotHoming:
            return new MessageEvent_SetRobotHoming(linkid);

		case MessageId::SetPlasmaControl:
		{
			if (message.size() < 2)
				return nullptr;
			try
			{
				PlasmaControlType type = (PlasmaControlType)message.poptyp<uint8_t>();
				PlasmaControlIO io = (PlasmaControlIO)message.poptyp<uint8_t>();
				return new MessageEvent_SetPlasmaControl(type, io, linkid);
			}
			catch (...)
			{
				return nullptr;
			}
		}

		case MessageId::SetRobotCollisionSimulation:
			return new MessageEvent_SetRobotCollisionSimulation(linkid);

		case MessageId::SetRobotMeasureGlueAmount:
		{
			if (message.size() < 2)
				return nullptr;
			try
			{
				RobotMeasureGunAmountType measure_type = (RobotMeasureGunAmountType)message.poptyp<uint8_t>();
				uint32_t seconds = message.poptyp<uint32_t>();
				return new MessageEvent_SetRobotMeasureGlueAmount(measure_type, seconds, linkid);
			}
			catch (...)
			{
				return nullptr;
			}
		}

        case MessageId::SetCleanGun:
            return new MessageEvent_SetCleanGun(linkid);

        case MessageId::SetSprayLine:
        {
            if (message.size() < 1)
                return nullptr;
            try
            {
                bool isFirstLoop = message.poptyp<uint8_t>() != 0;
                uint32_t millimeter = message.poptyp<uint32_t>();
                return new MessageEvent_SetSprayLine(isFirstLoop, millimeter, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }

        case MessageId::SetRobotIdleTime:
        {
            if (message.size() < 1)
                return nullptr;
            try
            {
                uint32_t second = message.poptyp<uint32_t>();
                return new MessageEvent_SetRobotIdleTime(second, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }

        case MessageId::SetRobotSprayCount:
        {
            if (message.size() < 1)
                return nullptr;
            try
            {
                uint32_t count = message.poptyp<uint32_t>();
                return new MessageEvent_SetRobotSprayCount(count, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }

        case MessageId::SetReleaseGlue:
            return new MessageEvent_SetReleaseGlue(linkid);

        case MessageId::SetRobotMovingSpeed:
        {
            if (message.size() < 4)
                return nullptr;
            try
            {
                RobotSpeed speed = (RobotSpeed)message.poptyp<uint8_t>();
				uint32_t edge_speed = message.poptyp<uint32_t>();
				uint32_t inside_speed = message.poptyp<uint32_t>();
				uint8_t high_acc_edge_spray = message.poptyp<uint8_t>();
                return new MessageEvent_SetRobotMovingSpeed(speed, edge_speed, inside_speed, high_acc_edge_spray == 1, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }

        case MessageId::ResetDailySprayCounter:
            return new MessageEvent_ResetDailySprayCounter(linkid);

        case MessageId::SetRobotAction:
        {
            if (message.size() < 1)
                return nullptr;
            try
            {
                RobotAction action = (RobotAction)message.poptyp<uint8_t>();
                return new MessageEvent_SetRobotAction(action, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }
        
        case MessageId::SetRobotMove:
        {
            if (message.size() < 1)
                return nullptr;
            try
            {
                RobotMove move = (RobotMove)message.poptyp<uint8_t>();
                return new MessageEvent_SetRobotMove(move, linkid);
            }
            catch (...)
            {
                return nullptr;
            }
        }

		case MessageId::SetRobotSoakNozzle:
		{
			if (message.size() < 1)
				return nullptr;
			try
			{
				SoakNozzle status = (SoakNozzle)message.poptyp<uint8_t>();
				return new MessageEvent_SetRobotSoakNozzle(status, linkid);
			}
			catch (...)
			{
				return nullptr;
			}
		}

		case MessageId::SetRobotMaintainGun:
		{
			if (message.size() < 1)
				return nullptr;
			try
			{
				MaintainGun status = (MaintainGun)message.poptyp<uint8_t>();
				return new MessageEvent_SetRobotMaintainGun(status, linkid);
			}
			catch (...)
			{
				return nullptr;
			}
		}

		case MessageId::SetUsingCommonSpraySettingForRobot:
		{
			if (message.size() < 1)
				return nullptr;
			try
			{
				bool commonspraysetting = message.poptyp<uint8_t>() > 0;
				return new MessageEvent_SetUsingCommonSpraySettingForRobot(commonspraysetting, linkid);
			}
			catch (...)
			{
				return nullptr;
			}
		}

		case MessageId::SetUsingCustomizedPathAlgorithmForRobot:
		{
			if (message.size() < 1)
				return nullptr;
			try
			{
				CustomizedPathAlgorithmType type = (CustomizedPathAlgorithmType)message.poptyp<int32_t>();
				return new MessageEvent_SetUsingCustomizedPathAlgorithmForRobot(type, linkid);
			}
			catch (...)
			{
				return nullptr;
			}
		}
		
        case MessageId::GetJigPointsError:
        {
            uint8_t lineid = message.poptyp<uint8_t>();
            return new MessageEvent_GetJigPointsError(lineid, linkid);
        }

        //--------------------- Notification type : from Workers pass-thru Manager to Shell ------------------------------------
        // Only Shell need to implement these incoming message event, Manager just an intermeidate deliverer

        default:
            return nullptr;
        }
    }

    // return an outgoing message from send notification
    // NOTE: for router socket, prepend the message with linkid header before or after this call
    inline MessageId makeOutgoingMessage(Poco::Notification::Ptr pNotify, zmq::multipart_t & message)
    {
        MessageId msgtype = MessageId::InvalidType;

        //------------------ Acknowledgement Types ----------------------------
        if (auto pevent = pNotify.cast<MessageEvent_Ack>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
            message.addtyp<uint8_t>((uint8_t)pevent->msgid2ack());
        }
        else if (auto pevent = pNotify.cast<MessageEvent_Nack>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
            message.addtyp<uint8_t>((uint8_t)pevent->msgid2nack());
            message.addstr(pevent->strerror());
        }
        //--------- System Command Types --------------------------------------
        else if (auto pevent = pNotify.cast<MessageEvent_CommandLink>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
        }
        else if (auto pevent = pNotify.cast<MessageEvent_CommandInit>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
            message.addtyp<uint8_t>((uint8_t)pevent->opmode());
            message.addtyp<uint8_t>((uint8_t)pevent->model());
        }
        else if (auto pevent = pNotify.cast<MessageEvent_CommandRun>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
        }
        else if (auto pevent = pNotify.cast<MessageEvent_CommandStop>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
            message.addtyp<uint8_t>((uint8_t)pevent->tone());
        }
        else if (auto pevent = pNotify.cast<MessageEvent_CommandHalt>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
            message.addtyp<uint8_t>((uint8_t)pevent->tone());
        }
        else if (auto pevent = pNotify.cast<MessageEvent_CommandResume>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
        }
        else if (auto pevent = pNotify.cast<MessageEvent_CommandReset>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
        }
        else if (auto pevent = pNotify.cast<MessageEvent_CommandUnlink>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
        }
        //---------------- Heartbeat Message Pair -----------------------------
        else if (auto pevent = pNotify.cast<MessageEvent_HeartbeatPing>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
        }
        else if (auto pevent = pNotify.cast<MessageEvent_HeartbeatPong>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
        }
        //----------- Notification type : one-way system-wide general notification -----------
        else if (auto pevent = pNotify.cast<MessageEvent_NotifyCommandResult>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
            message.addtyp<uint8_t>((uint8_t)pevent->cmdtype());
            message.addtyp<uint8_t>((uint8_t)pevent->code());
            message.addstr(pevent->description());
        }
        else if (auto pevent = pNotify.cast<MessageEvent_NotifyError>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
            message.addtyp<int8_t>((int8_t)pevent->code());
            message.addstr(pevent->description());
        }

        //----------------- Shell/MMI request type -----------------------------
        // Note: these types send from Shell/MMI only, no need to implement for Manager/Workers

        //----------------- System Information Get/Reply pair ------------------------
        else if (auto pevent = pNotify.cast<MessageEvent_GetSystemStatus>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
        }
        else if (auto pevent = pNotify.cast<MessageEvent_ReplySystemStatus>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
            message.addtyp<uint8_t>((uint8_t)pevent->state());
            message.addtyp<uint8_t>((uint8_t)pevent->opmode());
        }
        //----------------- Version Info Get/Reply pair ------------------------
        else if (auto pevent = pNotify.cast<MessageEvent_GetVersion>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
        }
        else if (auto pevent = pNotify.cast<MessageEvent_ReplyVersion>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
            message.addstr(pevent->version());
        }
        //----------------- Setup type : from Shell pass-thru Manager to Workers -----------------------------------
        // Note: outgoing setup type handled by makeTransferIfNotForManager() in Manager, and Workers handle incomings only

        //--------------------- Notification type : from Workers pass-thru Manager to Shell ------------------------------------
		else if (auto pevent = pNotify.cast<MessageEvent_ReplyMeasureGlueAmount>())
		{
			msgtype = pevent->msgid();
			message.addtyp<uint8_t>((uint8_t)msgtype);
			message.addtyp<uint32_t>(pevent->amount());
		}
        else if (auto pevent = pNotify.cast<MessageEvent_ReplyBigConveyorMinimumCycleTime>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
            message.addtyp<uint32_t>(pevent->time());
        }
        else if (auto pevent = pNotify.cast<MessageEvent_ReplyOvenConveyorLengthTime>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
            message.addtyp<uint8_t>(pevent->ovenid());
            message.addtyp<uint32_t>(pevent->length());
            message.addtyp<uint32_t>(pevent->time());
            message.addtyp<uint32_t>(pevent->soledistance());
        }
        else if (auto pevent = pNotify.cast<MessageEvent_ReplyRobotPosition>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
            message.addmem(&pevent->position(), sizeof Point3d);
        }
		else if (auto pevent = pNotify.cast<MessageEvent_ReplyPlasmaControl>())
		{
			msgtype = pevent->msgid();
			message.addtyp<uint8_t>((uint8_t)msgtype);
			message.addtyp<uint8_t>((uint8_t)pevent->type());
			message.addtyp<uint8_t>((uint8_t)pevent->io());
		}
		else if (auto pevent = pNotify.cast<MessageEvent_NotifyCustomizedPathAlgorithmContour>())
		{
			msgtype = pevent->msgid();
			message.addtyp<uint8_t>((uint8_t)msgtype);
			message.addtyp<uint8_t>(pevent->lineid());
			message.addtyp<uint32_t>((uint32_t)pevent->contour().size());
			message.addmem(pevent->contour().data(), sizeof(Point3d) * pevent->contour().size());
		}
        else if (auto pevent = pNotify.cast<MessageEvent_ReplySprayCounter>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
            message.addtyp<uint32_t>(pevent->daily());
            message.addtyp<uint32_t>(pevent->total());
            message.addtyp<uint32_t>(pevent->current());
        }
        else if (auto pevent = pNotify.cast<MessageEvent_ReplyJigPointsError>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
            message.addtyp<uint8_t>(pevent->lineid());
            message.addtyp<uint32_t>((uint32_t)pevent->errors().size());
            message.addmem(pevent->errors().data(), sizeof(double) * pevent->errors().size());
        }
        else if (auto pevent = pNotify.cast<MessageEvent_NotifySetBigConveyorCycleTimeToMinimum>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
            message.addtyp<uint32_t>(pevent->time());
        }
        else if (auto pevent = pNotify.cast<MessageEvent_NotifyStationLinkFromNext>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
            message.addtyp<uint8_t>((uint8_t)pevent->action());
        }
        else if (auto pevent = pNotify.cast<MessageEvent_NotifyStationLinkFromPrev>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
            message.addtyp<uint8_t>((uint8_t)pevent->action());
        }
		else if (auto pevent = pNotify.cast<MessageEvent_NotifyStationUpdateRecipe>())
		{
			msgtype = pevent->msgid();
			message.addtyp<uint8_t>((uint8_t)msgtype);
			message.addstr(pevent->soletype());
			uint32_t items = (uint32_t)pevent->settings().size();
			message.addtyp<uint32_t>(items);
			for (uint32_t i = 0; i < items; i++)
				message.addstr(pevent->settings()[i]);
		}
		else if (auto pevent = pNotify.cast<MessageEvent_NotifyStationUpdateRecipeOK>())
		{
			msgtype = pevent->msgid();
			message.addtyp<uint8_t>((uint8_t)msgtype);
		}
		else if (auto pevent = pNotify.cast<MessageEvent_NotifyStationUpdateRecipeFailure>())
		{
			msgtype = pevent->msgid();
			message.addtyp<uint8_t>((uint8_t)msgtype);
		}
        else if (auto pevent = pNotify.cast<MessageEvent_NotifyConveyorPausedTimeoutAlarm>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
        }
        else if (auto pevent = pNotify.cast<MessageEvent_NotifyResetConveyorPausedTimeoutAlarmByNext>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
        }
        else if (auto pevent = pNotify.cast<MessageEvent_NotifyJigPoints>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
            message.addtyp<uint8_t>(pevent->lineid());
            message.addtyp<uint32_t>((uint32_t)pevent->points().size());
            message.addmem(pevent->points().data(), sizeof(Point3d) * pevent->points().size());
        }
        else if (auto pevent = pNotify.cast<MessageEvent_NotifySoleContour>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
            message.addtyp<uint8_t>(pevent->lineid());
			message.addstr(pevent->sole_type_name());
			message.addtyp<uint32_t>((uint32_t)(pevent->contour_length() * 100));
			message.addtyp<uint8_t>(pevent->left_sole() ? 1 : 0);
            message.addtyp<uint32_t>((uint32_t)pevent->contour().size());
            message.addmem(pevent->contour().data(), sizeof(Point3d) * pevent->contour().size());
        }
        else if (auto pevent = pNotify.cast<MessageEvent_NotifyJobTaken>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
            message.addtyp<uint8_t>(pevent->workerid());
            message.addtyp<uint8_t>(pevent->lineid());
            message.addstr(pevent->jobid());
        }
        else if (auto pevent = pNotify.cast<MessageEvent_NotifyUncheckClearSole>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
        }
        else if (auto pevent = pNotify.cast<MessageEvent_NotifyZoriginAlignmentDone>())
        {
            msgtype = pevent->msgid();
            message.addtyp<uint8_t>((uint8_t)msgtype);
            message.addtyp<uint8_t>(pevent->lineid());
        }

        return msgtype;
    }

    // (Utility For Manager) clone the message and prepend proper linkid if the message should directly transfer to Worker or Shell
    // Note: the MessageId field is poped out before this call, and must pass it as the first parameter here
    inline zmq::multipart_t makeTransferIfNotForManager(MessageId msgtype, zmq::multipart_t & message, bool hasDioMotion)
    {
        zmq::multipart_t msgXfer;

        switch (msgtype)
        {
        // for Dio
		case MessageId::SetAlarmLevel:
        case MessageId::SetOutputDevice:
		case MessageId::SetDioMeasureGlueAmount:
        {
            msgXfer = message.clone();
            msgXfer.pushtyp<uint8_t>((uint8_t)msgtype);
            std::string linkid = hasDioMotion ? makeIdentity(HeaderField::IdDioMotion) : makeIdentity(HeaderField::IdDio);
            msgXfer.pushstr(linkid);
            break;
        }

        // for Conveyor
		case MessageId::SetConveyorMaintainGun:
		case MessageId::SetRobotActionToConveyor:
        case MessageId::GetBigConveyorMinimumCycleTime:
        case MessageId::GetOvenConveyorLengthTime:
        case MessageId::SetStationLinkPrev:
		case MessageId::UpdateRecipeToSyncStation:
        case MessageId::SetClearSoleInProgress:
		case MessageId::SetAutoExposure:
		case MessageId::RunFrontOvenConveyor:
        case MessageId::SetOvenConveyorTime:
        case MessageId::ResetConveyorPausedTimeoutAlarm:
        case MessageId::SetSoleFeedingMode:
        case MessageId::SetBigConveyorCycleTime:
        case MessageId::SetBigConveyorCleaning:
        case MessageId::SetCalibrationLineID:
		case MessageId::A3_SetConveyorPosition:
        {
            msgXfer = message.clone();
            msgXfer.pushtyp<uint8_t>((uint8_t)msgtype);
            std::string linkid = hasDioMotion ? makeIdentity(HeaderField::IdDioMotion) : makeIdentity(HeaderField::IdConveyor);
            msgXfer.pushstr(linkid);
            break;
        }

        // for Slider
        case MessageId::SetSliderMovingSpeed:
        case MessageId::SetSliderHoming:
        case MessageId::SetZoriginAlignment:
        {
            msgXfer = message.clone();
            msgXfer.pushtyp<uint8_t>((uint8_t)msgtype);
            std::string linkid = hasDioMotion ? makeIdentity(HeaderField::IdDioMotion) : makeIdentity(HeaderField::IdSlider);
            msgXfer.pushstr(linkid);
            break;
        }

        // for Scanner
        case MessageId::SetScannerParameter:
        {
            msgXfer = message.clone();
            msgXfer.pushtyp<uint8_t>((uint8_t)msgtype);
            msgXfer.pushstr(makeIdentity(HeaderField::IdScanner));
            break;
        }

        // for Vision 
        case MessageId::SetVisionRecipe:
		case MessageId::SetAutoSoleType:
		case MessageId::SetUseCustomizedPath:
		case MessageId::SetUseAutoIfPatternNotFound:
		case MessageId::SetUsingCommonSpraySettingForVision:
		case MessageId::SetUsingCustomizedPathAlgorithmForVision:
        {
            msgXfer = message.clone();
            msgXfer.pushtyp<uint8_t>((uint8_t)msgtype);
            msgXfer.pushstr(makeIdentity(HeaderField::IdVision));
            break;
        }

        // for Robot
        case MessageId::GetRobotPosition:
        case MessageId::GetSprayCounter:
        case MessageId::SetPathPlanningRecipe:
        case MessageId::SetRobotHoming:
		case MessageId::SetPlasmaControl:
		case MessageId::SetRobotCollisionSimulation:
		case MessageId::SetRobotMeasureGlueAmount:
        case MessageId::SetCleanGun:
        case MessageId::SetSprayLine:
        case MessageId::SetRobotIdleTime:
        case MessageId::SetRobotSprayCount:
        case MessageId::SetReleaseGlue:
        case MessageId::SetRobotMovingSpeed:
        case MessageId::ResetDailySprayCounter:
        case MessageId::SetRobotAction:
        case MessageId::SetRobotMove:
		case MessageId::SetRobotSoakNozzle:
		case MessageId::SetRobotMaintainGun:
		case MessageId::SetUsingCommonSpraySettingForRobot:
		case MessageId::SetUsingCustomizedPathAlgorithmForRobot:
        case MessageId::GetJigPointsError:
        {
            msgXfer = message.clone();
            msgXfer.pushtyp<uint8_t>((uint8_t)msgtype);
            msgXfer.pushstr(makeIdentity(HeaderField::IdRobot));
            break;
        }

        // for Shell
		case MessageId::ReplyMeasureGlueAmount:
        case MessageId::ReplyBigConveyorMinimumCycleTime:
        case MessageId::ReplyOvenConveyorLengthTime:
        case MessageId::ReplyRobotPosition:
		case MessageId::ReplyPlasmaControl:
		case MessageId::NotifyCustomizedPathAlgorithmContour:
        case MessageId::ReplySprayCounter:
        case MessageId::ReplyJigPointsError:
        case MessageId::NotifySetBigConveyorCycleTimeToMinimum:
        case MessageId::NotifyStationLinkFromNext:
        case MessageId::NotifyStationLinkFromPrev:
		case MessageId::NotifyStationUpdateRecipe:
		case MessageId::NotifyStationUpdateRecipeOK:
		case MessageId::NotifyStationUpdateRecipeFailure:
        case MessageId::NotifyConveyorPausedTimeoutAlarm:
        case MessageId::NotifyResetConveyorPausedTimeoutAlarmByNext:
        case MessageId::NotifyJigPoints:
        case MessageId::NotifySoleContour:
        case MessageId::NotifyJobTaken:
        case MessageId::NotifyUncheckClearSole:
        case MessageId::NotifyZoriginAlignmentDone:
        {
            msgXfer = message.clone();
            msgXfer.pushtyp<uint8_t>((uint8_t)msgtype);
            msgXfer.pushstr(makeIdentity(HeaderField::IdShell));
            break;
        }

        default:
            // leave msgXfer empty for those message types not meant to transfer
            break;
        } //switch

        return msgXfer;
    }
} // omlp namespace
