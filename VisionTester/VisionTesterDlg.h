
// VisionTesterDlg.h : 標頭檔
//

#pragma once

#include <zmq_addon.hpp>
#include "afxwin.h"

enum StateType : BYTE {
	None = 0,
	Online,
	Ready,
	Run,
	Halt,
};

// CVisionTesterDlg 對話方塊
class CVisionTesterDlg : public CDialogEx
{
private:
	zmq::context_t context;
	zmq::socket_t router;
	zmq::context_t _zmqctx;
	zmq::socket_t _pushToVision;
	zmq::context_t _zmqctx1;
	zmq::context_t _zmqctx2;
	zmq::socket_t _pullFromVision;
	zmq::socket_t _pullFromRobot;

	StateType m_state;

	BOOL m_bFirstRun = TRUE;

	void SetSettings();

// 建構
public:
	CVisionTesterDlg(CWnd* pParent = NULL);	// 標準建構函式

	std::string generateJobId();

// 對話方塊資料
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_VISIONTESTER_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支援


// 程式碼實作
protected:
	HICON m_hIcon;

	// 產生的訊息對應函式
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonInit();
	afx_msg void OnBnClickedButtonRun();
	afx_msg void OnBnClickedButtonDetect();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedButtonStop();
	afx_msg void OnBnClickedButtonSet();
	BOOL m_bSpikeNoiseSuppression;
	afx_msg void OnBnClickedCheckSpikeNoiseSuppression();
	afx_msg void OnBnClickedRadioInterpolation();
	afx_msg void OnBnClickedRadioNeighborSubstitution();
	int m_nDeadZoneInpainting;
	int m_nPoints;
	int m_nToeHeelLength;
	double m_dfViewHeight;
	double m_dfFeatureThreshold;
	int m_nSmoothCoefficient;
	int m_nToeCutting;
	afx_msg void OnEnKillfocusEditPoints();
	afx_msg void OnEnKillfocusEditToeheel();
	afx_msg void OnEnKillfocusEditViewHeight();
	afx_msg void OnEnKillfocusEditFeatureThreshold();
	afx_msg void OnEnKillfocusEditSmoothCoefficient();
	afx_msg void OnEnKillfocusEditToeCutting();
	afx_msg LRESULT onDetectionFinish(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT onDetectionFail(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT onRobotFinish(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT onRobotFail(WPARAM wParam, LPARAM lParam);
	BOOL m_bFilterWideEdge;
	afx_msg void OnBnClickedCheckFilterWideEdge();
	BOOL m_bAutoPoints;
	BOOL m_bThinSole;
	afx_msg void OnBnClickedCheckAutoPoints();
	afx_msg void OnBnClickedCheckThinSole();
	float m_fFirstRadius;
	float m_fOtherRadius;
	afx_msg void OnEnKillfocusEditFirstRadius();
	afx_msg void OnEnKillfocusOtherRadius();
	BOOL m_bAutoViewHeight;
	afx_msg void OnBnClickedCheckAutoViewHeight();
	CComboBox m_cbEdgeSprayCycles;
	int m_nEdgeSprayCycles;
	afx_msg void OnCbnSelchangeComboEdgeSprayCycles();
	BOOL m_bAutoCutting;
	afx_msg void OnBnClickedCheckAutoCutting();
	int m_nSprayAngle;
	BOOL m_bAutoSprayAngle;
	afx_msg void OnEnKillfocusEditSprayAngle();
	afx_msg void OnBnClickedCheckAutoSprayAngle();
	float m_fFirstHeight;
	float m_fOtherHeight;
	afx_msg void OnEnKillfocusEditFirstHeight();
	afx_msg void OnEnKillfocusEditOtherHeight();
	BOOL m_bMidway;
	BOOL m_bFull;
	afx_msg void OnBnClickedCheckMidWay();
	afx_msg void OnBnClickedCheckFull();
	BOOL m_bContinuously;
	afx_msg void OnBnClickedCheckContinuously();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	BOOL m_bSwitchSoleType;
	BOOL m_bUseAutoIfNotFound;
	afx_msg void OnBnClickedCheckSwitchSoleType();
	afx_msg void OnBnClickedCheckUseAutoIfNotFound();
	CString m_sSoleTypeName;
	afx_msg void OnBnClickedRadioInTheFront();
	afx_msg void OnBnClickedRadioInTheBack();
	int m_nInTheFront;
	BOOL m_bZonePath;
	afx_msg void OnBnClickedCheckZonePath();
	CComboBox m_cbSoleType;
	int m_nSoleType;
	afx_msg void OnCbnSelchangeComboSoleType();
	CButton m_chkZonePath;
	CComboBox m_cbCustomizedPathAlg;
	afx_msg void OnCbnSelchangeComboCustomizedPathAlg();
	int m_nCustomizedPathAlg;
	BOOL m_bCustomizedPathAlg;
	afx_msg void OnBnClickedCheckCustomizedPathAkg();
};
