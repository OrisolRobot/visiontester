//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by VisionTester.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_VISIONTESTER_DIALOG         102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDD_DIALOG_NEXT_SCANNER_DATA    130
#define IDC_BUTTON_INIT                 1000
#define IDC_BUTTON_RUN                  1001
#define IDC_BUTTON_STOP                 1002
#define IDC_BUTTON_DETECT               1003
#define IDC_BUTTON_SET                  1004
#define IDC_CHECK_SPIKE_NOISE_SUPPRESSION 1005
#define IDC_RADIO_INTERPOLATION         1006
#define IDC_RADIO_NEIGHBOR_SUBSTITUTION 1007
#define IDC_EDIT_POINTS                 1008
#define IDC_EDIT_TOEHEEL                1009
#define IDC_EDIT_VIEW_HEIGHT            1010
#define IDC_EDIT_FEATURE_THRESHOLD      1011
#define IDC_EDIT_SMOOTH_COEFFICIENT     1012
#define IDC_EDIT_SMOOTH_COEFFICIENT2    1013
#define IDC_EDIT_TOE_CUTTING            1013
#define IDC_CHECK_SPIKE_NOISE_SUPPRESSION2 1014
#define IDC_CHECK_FILTER_WIDE_EDGE      1014
#define IDC_CHECK_THIN_SOLE             1015
#define IDC_CHECK_AUTO_POINTS           1016
#define IDC_EDIT_FIRST_RADIUS           1017
#define IDC_OTHER_RADIUS                1018
#define IDC_CHECK_AUTO_VIEW_HEIGHT      1019
#define IDC_COMBO_EDGE_SPRAY_CYCLES     1020
#define IDC_CHECK_AUTO_CUTTING          1021
#define IDC_EDIT_SPRAY_ANGLE            1022
#define IDC_CHECK_AUTO_SPRAY_ANGLE      1023
#define IDC_EDIT_FIRST_HEIGHT           1024
#define IDC_EDIT_OTHER_HEIGHT           1025
#define IDC_CHECK_MID_WAY               1026
#define IDC_CHECK_FULL                  1027
#define IDC_CHECK_CONTINUOUSLY          1028
#define IDC_STATIC_DATA_PATH            1029
#define IDC_STATIC_SCANNER_DATA_FILENAME 1030
#define IDC_BUTTON_NEXT_DETECT          1031
#define IDC_BUTTON_SKIP                 1032
#define IDC_CHECK_SWITCH_SOLE_TYPE      1032
#define IDC_CHECK_USE_AUTO_IF_NOT_FOUND 1033
#define IDC_EDIT_SOLE_TYPE_NAME         1034
#define IDC_RADIO_IN_THE_FRONT          1035
#define IDC_RADIO_IN_THE_BACK           1036
#define IDC_CHECK_ZONE_PATH             1037
#define IDC_COMBO_SOLE_TYPE             1038
#define IDC_COMBO_CUSTOMIZED_PATH_ALG   1039
#define IDC_CHECK_CUSTOMIZED_PATH_AKG   1040

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1041
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
