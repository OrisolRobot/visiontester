#pragma once

#include "resource.h"

// CNextScannerDataDlg dialog

class CNextScannerDataDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CNextScannerDataDlg)

public:
	CNextScannerDataDlg(CString sFilename, CWnd* pParent = NULL);   // standard constructor
	virtual ~CNextScannerDataDlg();

	BOOL m_bDetect = FALSE;

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_NEXT_SCANNER_DATA };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CString m_sFilename;
	afx_msg void OnBnClickedButtonNextDetect();
	afx_msg void OnBnClickedButtonSkip();
};
