/*
	File:
		Dio.h
	Description:
		Abstract base class to access digital I/O
	Copyright:
		Orisol Asia Ltd.
*/
#pragma once
#include <cstdint>

enum class DioMeasureGunAmountType : uint8_t
{
	AirDrain = 0,
	GlueWeight = 1
};

enum class VoltageType : uint8_t
{
	Voltage = 0,
	Current
};

enum class IoSwitch : bool
{
	Off = false,
	On = true
};

enum class DiDevice : uint32_t
{
	// robot working 1
	RobotWork1						= 0x00000001,
	// robot working 2
	RobotWork2						= 0x00000002,
	// a3 robot left working
	A3_RobotLeftWork				= 0x00000002,
	// robot working 3
	RobotGunClean					= 0x00000004,
	// a3 robot right working
	A3_RobotRightWork				= 0x00000004,
	// robot power
	RobotPower						= 0x00000008,
	// exhaust power
	ExhaustPower					= 0x00000010,
	// slider power
	SliderPower						= 0x00000020,
	// big conveyor power
	BigConveyorPower				= 0x00000040,
	// small conveyor power
	SmallConveyorPower				= 0x00000080,
	// oven conveyor power
	OvenConveyorPower				= 0x00000100,
	// emergency stop
	EmergencyFree					= 0x00000200,
	// door opened
	DoorClosed						= 0x00000400,
	// robot remote mode
	RobotRemoteMode					= 0x00000800,
	// Smog alarm
	SmogFree						= 0x00002000,
	FactoryAirPressure				= 0x00400000,
	MMIWatchBits = RobotWork1 | RobotWork2 | RobotGunClean | A3_RobotLeftWork | A3_RobotRightWork | RobotPower | ExhaustPower |
	SliderPower | BigConveyorPower | SmallConveyorPower | OvenConveyorPower | EmergencyFree | DoorClosed | RobotRemoteMode | 
	SmogFree | FactoryAirPressure,
	// spray cover left
	SprayCoverLeft					= 0x00010000,
	// spray cover right
	SprayCoverRight					= 0x00080000,
	// income sole sensor 1
	IncomeSoleSensor1				= 0x01000000,
	// income sole sensor 2, for the previous station
	IncomeSoleSensor2				= 0x02000000,
	// outgo sole sensor, for the next station
	OutgoSoleSensor					= 0x02000000,
	// small conveyor front sensor 1
	SmallConveyorFrontSensor1		= 0x01000000,
	// small conveyor end sensor 1
	SmallConveyorEndSensor1			= 0x02000000,
	// small conveyor middle sensor 1
	SmallConveyorMiddleSensor1		= 0x04000000,
	// small conveyor front sensor
	SmallConveyorFrontSensor		= 0x20000000,
	// small conveyor end sensor
	SmallConveyorEndSensor			= 0x40000000,
	// small conveyor middle sensor
	SmallConveyorMiddleSensor		= 0x80000000,
	// Robot LEL Level1 Alarm
	RobotLELLevel1Alarm				= 0x00010000,
	// Robot LEL Level2 Alarm
	RobotLELLevel2Alarm				= 0x00020000,
	// Robot LEL Level3 Alarm
	RobotLELLevel3Alarm				= 0x00040000,
	// Rear Oven LEL Level1 Alarm
	RearOvenLELLevel1Alarm			= 0x00080000,
	// Rear Oven LEL Level2 Alarm
	RearOvenLELLevel2Alarm			= 0x00100000,
	// Rear Oven LEL Level3 Alarm
	RearOvenLELLevel3Alarm			= 0x00200000,
	// Tank Glue Sensor Warning
	TankGlueSensor_Warning			= 0x00100000,
	// Tank Glue Sensor
	TankGlueSensor					= 0x00200000,
	RearOvenOutputSensor			= 0x08000000,
	IAQCSensor1						= 0x00004000,	//??????
	IAQCSensor2						= 0x00008000	//??????
};

enum class DoDevice : uint32_t
{
	// oven heating power
	OvenHeatingPowerOff			= 0x00000100,
	// exhaust power
	ExhaustPower				= 0x00100000,
	// slider power
	SliderPower					= 0x00200000,
	// big conveyor power
	BigConveyorPower			= 0x00400000,
	// small conveyor power
	SmallConveyorPower			= 0x00800000,
	// small conveyor1 power
	SmallConveyorPower1			= 0x01000000,
	// front oven conveyor power
	FrontOvenConveyorPower		= 0x01000000,
	// rear oven conveyor power
	RearOvenConveyorPower		= 0x02000000,
	// Brush Motor Power
	BrushMotorPower				= 0x02000000,
	// left sole light indicator
	LeftSoleLightIndicator		= 0x01000000,
	// right sole light indicator
	RightSoleLightIndicator		= 0x02000000,
	// flash light red
	FlashLightRed				= 0x10000000,
	// flash light yellow
	FlashLightYellow			= 0x20000000,
	// flash light green
	FlashLightGreen				= 0x40000000,
	// flash light beep
	FlashLightBeep				= 0x80000000,
	IAQCNG						= 0x04000000	//??????
};

enum class AlarmLevel : uint8_t
{
	CurrentLevel	= 0,
	None			= 1,
	Level1			= 2,
	Level2			= 3,
	Level3			= 4
};

// the number definition of DioResult may not be the same with those found in PCI_L122_Err or MNETError.h
enum class DioResult : int32_t
{
	NoError = 0,
	ERR_NoCardFound = -1,
	ERR_SetRingConfig = -2,
	ERR_GetStartRingNum = -3,
	ERR_InvalidRingNo = -4,
	ERR_GetRingActiveTable = -5,
	ERR_NoSlaveDevice = -6,
	ERR_StartRing = -7,
	ERR_OtherRing = -8,
	ERR_InvalidDevice = -9,
	ERR_FileIO = -10
};

class Dio
{
public:
	virtual ~Dio() {}
	virtual DioResult version(std::string & version) = 0;
	virtual DioResult initialize() = 0;
	virtual DioResult uninitialize() = 0;
	virtual DioResult readAll(uint32_t& di_states) = 0;
	virtual DioResult writeAll(uint32_t do_states) = 0;
	virtual DioResult readDevice(DiDevice device, IoSwitch & state) = 0;
	virtual DioResult writeDevice(DoDevice device, IoSwitch state) = 0;
	virtual DioResult readDevice(uint16_t ring_no, uint16_t slave_no, uint8_t port_no, uint8_t bit_no, IoSwitch & state) = 0;
	virtual DioResult writeDevice(uint16_t ring_no, uint16_t slave_no, uint8_t port_no, uint8_t bit_no, IoSwitch state) = 0;
	virtual DioResult getVoltage(uint16_t channel_no, double & value, VoltageType type = VoltageType::Voltage) = 0;
	static IoSwitch isOn(uint32_t di_states, DiDevice device)
	{
		if ((di_states & (uint32_t)device) > 0)
			return IoSwitch::On;
		else
			return IoSwitch::Off;
	};
};

