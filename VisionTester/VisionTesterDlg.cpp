
// VisionTesterDlg.cpp : 實作檔
//

#include "stdafx.h"
#include "VisionTester.h"
#include "VisionTesterDlg.h"
#include "NextScannerDataDlg.h"
#include "afxdialogex.h"

#include <vector>
#include <chrono>
#include <sstream>
#include "omlp.hpp"
#include "WorkflowScenario.hpp"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define DETECTION_FINISH	(WM_USER+1)
#define DETECTION_FAIL		(WM_USER+2)
#define ROBOT_FINISH		(WM_USER+3)
#define ROBOT_FAIL			(WM_USER+4)

using std::chrono::system_clock;
using std::ostringstream;

std::string sErrorDescription;
std::vector<std::string> vFilenameList;
size_t idxCur;

// 對 App About 使用 CAboutDlg 對話方塊

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 對話方塊資料
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支援

// 程式碼實作
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CVisionTesterDlg 對話方塊



CVisionTesterDlg::CVisionTesterDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_VISIONTESTER_DIALOG, pParent)
	, context(1)
	, router(context, zmq::socket_type::router)
	, _zmqctx(1)
	, _pushToVision(_zmqctx, zmq::socket_type::push)
	, _zmqctx1(1)
	, _pullFromVision(_zmqctx1, zmq::socket_type::pull)
	, _pullFromRobot(_zmqctx2, zmq::socket_type::pull)
	, m_bSpikeNoiseSuppression(FALSE)
	, m_nDeadZoneInpainting(0)
	, m_nPoints(80)
	, m_nToeHeelLength(30)
	, m_dfViewHeight(0)
	, m_dfFeatureThreshold(0.8)
	, m_nSmoothCoefficient(100)
	, m_nToeCutting(0)
	, m_bFilterWideEdge(FALSE)
	, m_bAutoPoints(TRUE)
	, m_bThinSole(FALSE)
	, m_fFirstRadius(3)
	, m_fOtherRadius(6)
	, m_bAutoViewHeight(TRUE)
	, m_nEdgeSprayCycles(0)
	, m_bAutoCutting(FALSE)
	, m_nSprayAngle(30)
	, m_bAutoSprayAngle(TRUE)
	, m_fFirstHeight(10)
	, m_fOtherHeight(20)
	, m_bMidway(TRUE)
	, m_bFull(TRUE)
	, m_bContinuously(TRUE)
	, m_bSwitchSoleType(FALSE)
	, m_bUseAutoIfNotFound(FALSE)
	, m_sSoleTypeName(_T(""))
	, m_nInTheFront(0)
	, m_bZonePath(FALSE)
	, m_nSoleType(0)
	, m_nCustomizedPathAlg(0)
	, m_bCustomizedPathAlg(FALSE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CVisionTesterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHECK_SPIKE_NOISE_SUPPRESSION, m_bSpikeNoiseSuppression);
	DDX_Radio(pDX, IDC_RADIO_INTERPOLATION, m_nDeadZoneInpainting);
	DDX_Text(pDX, IDC_EDIT_POINTS, m_nPoints);
	DDV_MinMaxInt(pDX, m_nPoints, 20, 200);
	DDX_Text(pDX, IDC_EDIT_TOEHEEL, m_nToeHeelLength);
	DDV_MinMaxInt(pDX, m_nToeHeelLength, 30, 120);
	DDX_Text(pDX, IDC_EDIT_VIEW_HEIGHT, m_dfViewHeight);
	DDV_MinMaxDouble(pDX, m_dfViewHeight, -50, 50);
	DDX_Text(pDX, IDC_EDIT_FEATURE_THRESHOLD, m_dfFeatureThreshold);
	DDV_MinMaxDouble(pDX, m_dfFeatureThreshold, 0.1, 1);
	DDX_Text(pDX, IDC_EDIT_SMOOTH_COEFFICIENT, m_nSmoothCoefficient);
	DDV_MinMaxInt(pDX, m_nSmoothCoefficient, 1, 5000);
	DDX_Text(pDX, IDC_EDIT_TOE_CUTTING, m_nToeCutting);
	DDV_MinMaxInt(pDX, m_nToeCutting, 0, 50);
	DDX_Check(pDX, IDC_CHECK_FILTER_WIDE_EDGE, m_bFilterWideEdge);
	DDX_Check(pDX, IDC_CHECK_AUTO_POINTS, m_bAutoPoints);
	DDX_Check(pDX, IDC_CHECK_THIN_SOLE, m_bThinSole);
	DDX_Text(pDX, IDC_EDIT_FIRST_RADIUS, m_fFirstRadius);
	DDV_MinMaxFloat(pDX, m_fFirstRadius, 0.1f, 15.f);
	DDX_Text(pDX, IDC_OTHER_RADIUS, m_fOtherRadius);
	DDV_MinMaxFloat(pDX, m_fOtherRadius, 1.f, 20.f);
	DDX_Check(pDX, IDC_CHECK_AUTO_VIEW_HEIGHT, m_bAutoViewHeight);
	DDX_Control(pDX, IDC_COMBO_EDGE_SPRAY_CYCLES, m_cbEdgeSprayCycles);
	DDX_CBIndex(pDX, IDC_COMBO_EDGE_SPRAY_CYCLES, m_nEdgeSprayCycles);
	DDX_Check(pDX, IDC_CHECK_AUTO_CUTTING, m_bAutoCutting);
	DDX_Text(pDX, IDC_EDIT_SPRAY_ANGLE, m_nSprayAngle);
	DDV_MinMaxInt(pDX, m_nSprayAngle, 0, 45);
	DDX_Check(pDX, IDC_CHECK_AUTO_SPRAY_ANGLE, m_bAutoSprayAngle);
	DDX_Text(pDX, IDC_EDIT_FIRST_HEIGHT, m_fFirstHeight);
	DDV_MinMaxFloat(pDX, m_fFirstHeight, 0.1f, 80.f);
	DDX_Text(pDX, IDC_EDIT_OTHER_HEIGHT, m_fOtherHeight);
	DDV_MinMaxFloat(pDX, m_fOtherHeight, 0.1f, 80.f);
	DDX_Check(pDX, IDC_CHECK_MID_WAY, m_bMidway);
	DDX_Check(pDX, IDC_CHECK_FULL, m_bFull);
	DDX_Check(pDX, IDC_CHECK_CONTINUOUSLY, m_bContinuously);
	DDX_Check(pDX, IDC_CHECK_SWITCH_SOLE_TYPE, m_bSwitchSoleType);
	DDX_Check(pDX, IDC_CHECK_USE_AUTO_IF_NOT_FOUND, m_bUseAutoIfNotFound);
	DDX_Text(pDX, IDC_EDIT_SOLE_TYPE_NAME, m_sSoleTypeName);
	DDX_Radio(pDX, IDC_RADIO_IN_THE_FRONT, m_nInTheFront);
	DDX_Check(pDX, IDC_CHECK_ZONE_PATH, m_bZonePath);
	DDX_Control(pDX, IDC_COMBO_SOLE_TYPE, m_cbSoleType);
	DDX_CBIndex(pDX, IDC_COMBO_SOLE_TYPE, m_nSoleType);
	DDX_Control(pDX, IDC_CHECK_ZONE_PATH, m_chkZonePath);
	DDX_Control(pDX, IDC_COMBO_CUSTOMIZED_PATH_ALG, m_cbCustomizedPathAlg);
	DDX_CBIndex(pDX, IDC_COMBO_CUSTOMIZED_PATH_ALG, m_nCustomizedPathAlg);
	DDX_Check(pDX, IDC_CHECK_CUSTOMIZED_PATH_AKG, m_bCustomizedPathAlg);
}

BEGIN_MESSAGE_MAP(CVisionTesterDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_INIT, &CVisionTesterDlg::OnBnClickedButtonInit)
	ON_BN_CLICKED(IDC_BUTTON_RUN, &CVisionTesterDlg::OnBnClickedButtonRun)
	ON_BN_CLICKED(IDC_BUTTON_DETECT, &CVisionTesterDlg::OnBnClickedButtonDetect)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_STOP, &CVisionTesterDlg::OnBnClickedButtonStop)
	ON_BN_CLICKED(IDC_BUTTON_SET, &CVisionTesterDlg::OnBnClickedButtonSet)
	ON_BN_CLICKED(IDC_CHECK_SPIKE_NOISE_SUPPRESSION, &CVisionTesterDlg::OnBnClickedCheckSpikeNoiseSuppression)
	ON_BN_CLICKED(IDC_RADIO_INTERPOLATION, &CVisionTesterDlg::OnBnClickedRadioInterpolation)
	ON_BN_CLICKED(IDC_RADIO_NEIGHBOR_SUBSTITUTION, &CVisionTesterDlg::OnBnClickedRadioNeighborSubstitution)
	ON_EN_KILLFOCUS(IDC_EDIT_POINTS, &CVisionTesterDlg::OnEnKillfocusEditPoints)
	ON_EN_KILLFOCUS(IDC_EDIT_TOEHEEL, &CVisionTesterDlg::OnEnKillfocusEditToeheel)
	ON_EN_KILLFOCUS(IDC_EDIT_VIEW_HEIGHT, &CVisionTesterDlg::OnEnKillfocusEditViewHeight)
	ON_EN_KILLFOCUS(IDC_EDIT_FEATURE_THRESHOLD, &CVisionTesterDlg::OnEnKillfocusEditFeatureThreshold)
	ON_EN_KILLFOCUS(IDC_EDIT_SMOOTH_COEFFICIENT, &CVisionTesterDlg::OnEnKillfocusEditSmoothCoefficient)
	ON_EN_KILLFOCUS(IDC_EDIT_TOE_CUTTING, &CVisionTesterDlg::OnEnKillfocusEditToeCutting)
	ON_MESSAGE(DETECTION_FINISH, onDetectionFinish)
	ON_MESSAGE(DETECTION_FAIL, onDetectionFail)
	ON_MESSAGE(ROBOT_FINISH, onRobotFinish)
	ON_MESSAGE(ROBOT_FAIL, onRobotFail)
	ON_BN_CLICKED(IDC_CHECK_FILTER_WIDE_EDGE, &CVisionTesterDlg::OnBnClickedCheckFilterWideEdge)
	ON_BN_CLICKED(IDC_CHECK_AUTO_POINTS, &CVisionTesterDlg::OnBnClickedCheckAutoPoints)
	ON_BN_CLICKED(IDC_CHECK_THIN_SOLE, &CVisionTesterDlg::OnBnClickedCheckThinSole)
	ON_EN_KILLFOCUS(IDC_EDIT_FIRST_RADIUS, &CVisionTesterDlg::OnEnKillfocusEditFirstRadius)
	ON_EN_KILLFOCUS(IDC_OTHER_RADIUS, &CVisionTesterDlg::OnEnKillfocusOtherRadius)
	ON_BN_CLICKED(IDC_CHECK_AUTO_VIEW_HEIGHT, &CVisionTesterDlg::OnBnClickedCheckAutoViewHeight)
	ON_CBN_SELCHANGE(IDC_COMBO_EDGE_SPRAY_CYCLES, &CVisionTesterDlg::OnCbnSelchangeComboEdgeSprayCycles)
	ON_BN_CLICKED(IDC_CHECK_AUTO_CUTTING, &CVisionTesterDlg::OnBnClickedCheckAutoCutting)
	ON_EN_KILLFOCUS(IDC_EDIT_SPRAY_ANGLE, &CVisionTesterDlg::OnEnKillfocusEditSprayAngle)
	ON_BN_CLICKED(IDC_CHECK_AUTO_SPRAY_ANGLE, &CVisionTesterDlg::OnBnClickedCheckAutoSprayAngle)
	ON_EN_KILLFOCUS(IDC_EDIT_FIRST_HEIGHT, &CVisionTesterDlg::OnEnKillfocusEditFirstHeight)
	ON_EN_KILLFOCUS(IDC_EDIT_OTHER_HEIGHT, &CVisionTesterDlg::OnEnKillfocusEditOtherHeight)
	ON_BN_CLICKED(IDC_CHECK_MID_WAY, &CVisionTesterDlg::OnBnClickedCheckMidWay)
	ON_BN_CLICKED(IDC_CHECK_FULL, &CVisionTesterDlg::OnBnClickedCheckFull)
	ON_BN_CLICKED(IDC_CHECK_CONTINUOUSLY, &CVisionTesterDlg::OnBnClickedCheckContinuously)
	ON_BN_CLICKED(IDOK, &CVisionTesterDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CVisionTesterDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_CHECK_SWITCH_SOLE_TYPE, &CVisionTesterDlg::OnBnClickedCheckSwitchSoleType)
	ON_BN_CLICKED(IDC_CHECK_USE_AUTO_IF_NOT_FOUND, &CVisionTesterDlg::OnBnClickedCheckUseAutoIfNotFound)
	ON_BN_CLICKED(IDC_RADIO_IN_THE_FRONT, &CVisionTesterDlg::OnBnClickedRadioInTheFront)
	ON_BN_CLICKED(IDC_RADIO_IN_THE_BACK, &CVisionTesterDlg::OnBnClickedRadioInTheBack)
	ON_BN_CLICKED(IDC_CHECK_ZONE_PATH, &CVisionTesterDlg::OnBnClickedCheckZonePath)
	ON_CBN_SELCHANGE(IDC_COMBO_SOLE_TYPE, &CVisionTesterDlg::OnCbnSelchangeComboSoleType)
	ON_CBN_SELCHANGE(IDC_COMBO_CUSTOMIZED_PATH_ALG, &CVisionTesterDlg::OnCbnSelchangeComboCustomizedPathAlg)
	ON_BN_CLICKED(IDC_CHECK_CUSTOMIZED_PATH_AKG, &CVisionTesterDlg::OnBnClickedCheckCustomizedPathAkg)
END_MESSAGE_MAP()


// CVisionTesterDlg 訊息處理常式

BOOL CVisionTesterDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 將 [關於...] 功能表加入系統功能表。

	// IDM_ABOUTBOX 必須在系統命令範圍之中。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 設定此對話方塊的圖示。當應用程式的主視窗不是對話方塊時，
	// 框架會自動從事此作業
	SetIcon(m_hIcon, TRUE);			// 設定大圖示
	SetIcon(m_hIcon, FALSE);		// 設定小圖示

	// TODO: 在此加入額外的初始設定

	m_state = StateType::None;

	router.bind("tcp://127.0.0.1:6801");

	_pushToVision.bind("tcp://127.0.0.1:6851");
	//_pullFromVision.connect("tcp://127.0.0.1:6860");
	_pullFromRobot.connect("tcp://127.0.0.1:6870");

	m_cbEdgeSprayCycles.AddString(L"Auto");
	m_cbEdgeSprayCycles.AddString(L"1");
	m_cbEdgeSprayCycles.AddString(L"2");
	m_cbEdgeSprayCycles.AddString(L"3");
	m_cbEdgeSprayCycles.AddString(L"4");
	m_cbEdgeSprayCycles.SetCurSel(m_nEdgeSprayCycles);

	m_cbCustomizedPathAlg.AddString(L"General");
	m_cbCustomizedPathAlg.AddString(L"Crocs_LiteRide");
	m_cbCustomizedPathAlg.AddString(L"Two Areas");
	m_cbCustomizedPathAlg.AddString(L"MIZUNO_Wave");

	m_cbCustomizedPathAlg.SetCurSel(m_nCustomizedPathAlg);
	m_cbCustomizedPathAlg.EnableWindow(FALSE);

	SetTimer(1, 200, NULL);

	if (GetPrivateProfileIntW(L"VISION", L"RepairDeadZone", 1, L"C:\\osrParam\\OrisolMachineSetting.ini") == 1) {
		GetDlgItem(IDC_RADIO_INTERPOLATION)->EnableWindow(FALSE);
		GetDlgItem(IDC_RADIO_NEIGHBOR_SUBSTITUTION)->EnableWindow(FALSE);
	}

	GetDlgItem(IDC_EDIT_POINTS)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDIT_VIEW_HEIGHT)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDIT_SPRAY_ANGLE)->EnableWindow(FALSE);

	GetDlgItem(IDC_BUTTON_RUN)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON_STOP)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON_DETECT)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON_SET)->EnableWindow(FALSE);
	GetDlgItem(IDC_STATIC_DATA_PATH)->SetWindowText(L"Input Scanner Data: D:\\Temp\\*.ply");

	GetDlgItem(IDC_CHECK_USE_AUTO_IF_NOT_FOUND)->EnableWindow(FALSE);

	CString sSearchingFiles = _T("C:\\osrParam\\OrisolSpraySetting\\*.info");
	_wfinddata64i32_t info_file;
	intptr_t hFile;
	if ((hFile = _wfindfirst(sSearchingFiles, &info_file)) == -1)
		;
	else
	{
		do
		{
			CString s = info_file.name;
			s = s.Left(s.GetLength() - 5);
			m_cbSoleType.AddString(s);
		} while (_wfindnext(hFile, &info_file) == 0);
		//_findclose(hFile);
		if (m_cbSoleType.GetCount() > 0) {
			m_nSoleType = 0;
			m_cbSoleType.SetCurSel(m_nSoleType);
		}
	}

	m_cbSoleType.EnableWindow(FALSE);

	return TRUE;  // 傳回 TRUE，除非您對控制項設定焦點
}

void CVisionTesterDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果將最小化按鈕加入您的對話方塊，您需要下列的程式碼，
// 以便繪製圖示。對於使用文件/檢視模式的 MFC 應用程式，
// 框架會自動完成此作業。

void CVisionTesterDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 繪製的裝置內容

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 將圖示置中於用戶端矩形
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 描繪圖示
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 當使用者拖曳最小化視窗時，
// 系統呼叫這個功能取得游標顯示。
HCURSOR CVisionTesterDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CVisionTesterDlg::OnBnClickedButtonInit()
{
	// TODO: 在此加入控制項告知處理常式程式碼

	AfxGetApp()->DoWaitCursor(1);

	GetDlgItem(IDC_BUTTON_INIT)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON_RUN)->EnableWindow(TRUE);
	GetDlgItem(IDC_BUTTON_SET)->EnableWindow(TRUE);

	m_state = StateType::Ready;

	zmq::multipart_t msgOutgoing;

	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::CommandLink);
	bool ret = msgOutgoing.send(router, ZMQ_DONTWAIT);

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdRobot));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::CommandLink);
	ret = msgOutgoing.send(router, ZMQ_DONTWAIT);

	Sleep(1000);

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::CommandInit);
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::OpMode::Standalone_Operation);
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MachineModel::LegoSole_Y);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdRobot));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::CommandInit);
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::OpMode::Standalone_Operation);
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MachineModel::LegoSole_Y);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	Sleep(3000);

	AfxGetApp()->DoWaitCursor(0);
}


void CVisionTesterDlg::OnBnClickedButtonRun()
{
	// TODO: 在此加入控制項告知處理常式程式碼

	AfxGetApp()->DoWaitCursor(1);

	GetDlgItem(IDC_BUTTON_RUN)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON_SET)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON_STOP)->EnableWindow(TRUE);
	GetDlgItem(IDC_BUTTON_DETECT)->EnableWindow(TRUE);

	if (m_bFirstRun) {
		SetSettings();
		m_bFirstRun = FALSE;
	}

	m_state = StateType::Run;

	zmq::multipart_t msgOutgoing;

	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::CommandRun);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdRobot));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::CommandRun);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	Sleep(4000);

	AfxGetApp()->DoWaitCursor(0);
}


void CVisionTesterDlg::OnBnClickedButtonStop()
{
	// TODO: 在此加入控制項告知處理常式程式碼

	AfxGetApp()->DoWaitCursor(1);

	GetDlgItem(IDC_BUTTON_STOP)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON_DETECT)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON_SET)->EnableWindow(TRUE);
	GetDlgItem(IDC_BUTTON_RUN)->EnableWindow(TRUE);
	GetDlgItem(IDC_CHECK_CONTINUOUSLY)->EnableWindow(TRUE);

	m_state = StateType::Ready;

	zmq::multipart_t msgOutgoing;

	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::CommandStop);
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::Tone::Soft);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdRobot));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::CommandStop);
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::Tone::Soft);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	Sleep(3000);

	AfxGetApp()->DoWaitCursor(0);
}


void CVisionTesterDlg::OnBnClickedButtonDetect()
{
	// TODO: 在此加入控制項告知處理常式程式碼

	GetDlgItem(IDC_BUTTON_DETECT)->EnableWindow(FALSE);

	CloudMeta meta;
	std::vector<Point3d> vPts;
	std::string sPlyFilename;

	if (!m_bContinuously) {
		sPlyFilename = "d:\\temp\\scanner_data.ply";
		vPts = PlyFile::readPlyToPoint3d(sPlyFilename, meta);

		if (vPts.size() == 0) {
			GetDlgItem(IDC_BUTTON_DETECT)->EnableWindow(TRUE);
			return;
		}
	}

	vFilenameList.clear();

	if (m_bContinuously) {
		_wfinddata64i32_t info_file;
		intptr_t hFile;

		CString sSearchingFiles = _T("D:\\Temp\\*.ply");

		if ((hFile = _wfindfirst(sSearchingFiles, &info_file)) == -1)
			;
		else
		{
			char szPath[MAX_PATH];
			do
			{
				CString s = info_file.name;
				s = L"D:\\Temp\\" + s;
				wcstombs(szPath, s.GetBuffer(), MAX_PATH);
				std::string sss = szPath;
				vFilenameList.push_back(sss);
			} while (_wfindnext(hFile, &info_file) == 0);
		}

		if (vFilenameList.size() == 0) {
			GetDlgItem(IDC_BUTTON_DETECT)->EnableWindow(TRUE);
			return;
		}

		wchar_t wzData[MAX_PATH];
		idxCur = 0;
		bool stop = false;
		do {
			mbstowcs(wzData, vFilenameList[idxCur].c_str(), MAX_PATH);
			CString sFilename = wzData;
			CNextScannerDataDlg dlgData(sFilename);
			if (dlgData.DoModal() == IDCANCEL)
				stop = true;
			else {
				if (dlgData.m_bDetect) {
					sPlyFilename = vFilenameList[idxCur];
					vPts = PlyFile::readPlyToPoint3d(sPlyFilename, meta);
					GetDlgItem(IDC_STATIC_DATA_PATH)->SetWindowText(L"Input Scanner Data: " + sFilename);
					break;
				}
				else {
					idxCur++;
					if (idxCur >= vFilenameList.size())
						stop = true;
				}
			}
		} while (!stop);
		if (stop) {
			GetDlgItem(IDC_BUTTON_DETECT)->EnableWindow(TRUE);
			return;
		}
	}

	GetDlgItem(IDC_CHECK_CONTINUOUSLY)->EnableWindow(FALSE);

	AfxGetApp()->DoWaitCursor(1);

	meta.datetime = generateJobId();

	std::wstring wPlyFilename(sPlyFilename.begin(), sPlyFilename.end());
	std::wstring wsDateTimie(meta.datetime.begin(), meta.datetime.end());
	CString sNewPlyFilename;
	sNewPlyFilename.Format(L"D:\\LGS\\DataDump\\%s-scanner-rawdata.ply", wsDateTimie.c_str());
	CopyFileW(wPlyFilename.c_str(), sNewPlyFilename.GetBuffer(), FALSE);

	zmq::multipart_t flowOutgoing;
	omlp::makeWork_ScanData(flowOutgoing, 0, false, meta, vPts);
	flowOutgoing.send(_pushToVision, ZMQ_DONTWAIT);
}


void CVisionTesterDlg::OnBnClickedButtonSet()
{
	// TODO: 在此加入控制項告知處理常式程式碼

	AfxGetApp()->DoWaitCursor(1);

	m_bFirstRun = FALSE;

	SetSettings();

	AfxGetApp()->DoWaitCursor(0);
}


void CVisionTesterDlg::SetSettings()
{
	zmq::multipart_t msgOutgoing;
	std::string str;
	char szValue[32];

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetAutoSoleType);
	if (m_bSwitchSoleType) {
		msgOutgoing.addtyp<bool>(true);
		msgOutgoing.send(router, ZMQ_DONTWAIT);
		Sleep(200);

		msgOutgoing.clear();
		msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
		msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetUseAutoIfPatternNotFound);
		msgOutgoing.addtyp<bool>(m_bUseAutoIfNotFound ? true : false);
		msgOutgoing.send(router, ZMQ_DONTWAIT);
	}
	else {
		msgOutgoing.addtyp<bool>(false);
		msgOutgoing.send(router, ZMQ_DONTWAIT);
	}

	Sleep(200);

	if (!m_bSwitchSoleType) {
		CString sSoleType;
		m_cbSoleType.GetLBText(m_nSoleType, sSoleType);
		std::wstring wSoleType = sSoleType.GetBuffer();
		std::string _soleType(wSoleType.begin(), wSoleType.end());

		msgOutgoing.clear();
		msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
		msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetUseCustomizedPath);
		msgOutgoing.addtyp<uint8_t>((uint8_t)(m_bZonePath ? 1 : 0));
		msgOutgoing.addstr(_soleType);
		msgOutgoing.send(router, ZMQ_DONTWAIT);

		Sleep(200);
	}

	int32_t nCustomizedPathAlg = (int32_t)CustomizedPathAlgorithmType::None;
	if (m_bCustomizedPathAlg)
		nCustomizedPathAlg = m_nCustomizedPathAlg;

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetUsingCustomizedPathAlgorithmForVision);
	msgOutgoing.addtyp<int32_t>(nCustomizedPathAlg);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	Sleep(200);

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdRobot));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetUsingCustomizedPathAlgorithmForRobot);
	msgOutgoing.addtyp<int32_t>(nCustomizedPathAlg);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	Sleep(200);

	itoa(m_nPoints, szValue, 10);
	str = szValue;

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetVisionRecipe);
	msgOutgoing.addtyp<uint8_t>((uint8_t)VisionRecipe::NumberOfDownSamplePoint);
	msgOutgoing.addstr(str);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	Sleep(200);

	itoa(m_nToeHeelLength, szValue, 10);
	str = szValue;

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetVisionRecipe);
	msgOutgoing.addtyp<uint8_t>((uint8_t)VisionRecipe::ToeHeelSectionLength);
	msgOutgoing.addstr(str);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	Sleep(200);

	_gcvt(m_dfViewHeight, 3, szValue);
	str = szValue;

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetVisionRecipe);
	msgOutgoing.addtyp<uint8_t>((uint8_t)VisionRecipe::ViewHeight);
	msgOutgoing.addstr(str);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	Sleep(200);

	_gcvt(m_dfFeatureThreshold, 3, szValue);
	str = szValue;

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetVisionRecipe);
	msgOutgoing.addtyp<uint8_t>((uint8_t)VisionRecipe::FeatureThreshold);
	msgOutgoing.addstr(str);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	Sleep(200);

	itoa(m_nSmoothCoefficient, szValue, 10);
	str = szValue;

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetVisionRecipe);
	msgOutgoing.addtyp<uint8_t>((uint8_t)VisionRecipe::SmoothFactor);
	msgOutgoing.addstr(str);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	Sleep(200);

	itoa(m_nToeCutting, szValue, 10);
	str = szValue;

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetVisionRecipe);
	msgOutgoing.addtyp<uint8_t>((uint8_t)VisionRecipe::ToeCutting);
	msgOutgoing.addstr(str);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	Sleep(200);

	itoa(m_bAutoCutting, szValue, 10);
	str = szValue;

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetVisionRecipe);
	msgOutgoing.addtyp<uint8_t>((uint8_t)VisionRecipe::AutoToeCutting);
	msgOutgoing.addstr(str);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	Sleep(200);

	itoa(m_nInTheFront == 0 ? 1 : 0, szValue, 10);
	str = szValue;

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetVisionRecipe);
	msgOutgoing.addtyp<uint8_t>((uint8_t)VisionRecipe::ToeCuttingDir);
	msgOutgoing.addstr(str);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	Sleep(200);

	itoa(m_bSpikeNoiseSuppression, szValue, 10);
	str = szValue;

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetVisionRecipe);
	msgOutgoing.addtyp<uint8_t>((uint8_t)VisionRecipe::SpikeNoiseSuppression);
	msgOutgoing.addstr(str);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	Sleep(200);

	itoa(m_bFilterWideEdge, szValue, 10);
	str = szValue;

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetVisionRecipe);
	msgOutgoing.addtyp<uint8_t>((uint8_t)VisionRecipe::FilterWideWall);
	msgOutgoing.addstr(str);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	Sleep(200);

	itoa(m_bAutoPoints, szValue, 10);
	str = szValue;

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetVisionRecipe);
	msgOutgoing.addtyp<uint8_t>((uint8_t)VisionRecipe::AutoPoints);
	msgOutgoing.addstr(str);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	Sleep(200);

	itoa(m_bThinSole, szValue, 10);
	str = szValue;

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetVisionRecipe);
	msgOutgoing.addtyp<uint8_t>((uint8_t)VisionRecipe::ThinSole);
	msgOutgoing.addstr(str);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	Sleep(200);

	int fill_holes_with_fit = 1;
	if (m_nDeadZoneInpainting == 1)
		fill_holes_with_fit = 0;
	itoa(fill_holes_with_fit, szValue, 10);
	str = szValue;

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetVisionRecipe);
	msgOutgoing.addtyp<uint8_t>((uint8_t)VisionRecipe::FillHolesWithFit);
	msgOutgoing.addstr(str);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	Sleep(200);

	char szValue1[_CVTBUFSIZE];
	_gcvt((double)m_fFirstRadius, 5, szValue1);
	str = szValue1;

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetVisionRecipe);
	msgOutgoing.addtyp<uint8_t>((uint8_t)VisionRecipe::SprayRadius1st);
	msgOutgoing.addstr(str);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdRobot));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetPathPlanningRecipe);
	msgOutgoing.addtyp<uint8_t>((uint8_t)PathPlanningRecipe::SprayRadius1st);
	msgOutgoing.addstr(str);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	Sleep(200);

	_gcvt((double)m_fOtherRadius, 5, szValue1);
	str = szValue1;

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetVisionRecipe);
	msgOutgoing.addtyp<uint8_t>((uint8_t)VisionRecipe::SprayRadiusOther);
	msgOutgoing.addstr(str);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdRobot));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetPathPlanningRecipe);
	msgOutgoing.addtyp<uint8_t>((uint8_t)PathPlanningRecipe::SprayRadiusOther);
	msgOutgoing.addstr(str);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	Sleep(200);

	_gcvt((double)m_fFirstHeight, 5, szValue1);
	str = szValue1;

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdRobot));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetPathPlanningRecipe);
	msgOutgoing.addtyp<uint8_t>((uint8_t)PathPlanningRecipe::ToolAwayDistance1st);
	msgOutgoing.addstr(str);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	Sleep(200);

	_gcvt((double)m_fOtherHeight, 5, szValue1);
	str = szValue1;

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdRobot));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetPathPlanningRecipe);
	msgOutgoing.addtyp<uint8_t>((uint8_t)PathPlanningRecipe::ToolAwayDistanceOther);
	msgOutgoing.addstr(str);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	Sleep(200);

	itoa(m_bAutoViewHeight, szValue, 10);
	str = szValue;

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetVisionRecipe);
	msgOutgoing.addtyp<uint8_t>((uint8_t)VisionRecipe::AutoViewHeight);
	msgOutgoing.addstr(str);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	Sleep(200);

	itoa(m_nEdgeSprayCycles, szValue, 10);
	str = szValue;

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetVisionRecipe);
	msgOutgoing.addtyp<uint8_t>((uint8_t)VisionRecipe::EdgeSprayCycles);
	msgOutgoing.addstr(str);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdRobot));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetPathPlanningRecipe);
	msgOutgoing.addtyp<uint8_t>((uint8_t)PathPlanningRecipe::EdgeSprayCycles);
	msgOutgoing.addstr(str);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	Sleep(200);

	itoa(m_bAutoSprayAngle, szValue, 10);
	str = szValue;

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetVisionRecipe);
	msgOutgoing.addtyp<uint8_t>((uint8_t)VisionRecipe::AutoSprayAngle);
	msgOutgoing.addstr(str);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdRobot));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetPathPlanningRecipe);
	msgOutgoing.addtyp<uint8_t>((uint8_t)PathPlanningRecipe::AutoSprayAngle);
	msgOutgoing.addstr(str);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	Sleep(200);

	itoa(m_nSprayAngle, szValue, 10);
	str = szValue;

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdRobot));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetPathPlanningRecipe);
	msgOutgoing.addtyp<uint8_t>((uint8_t)PathPlanningRecipe::SprayAngle);
	msgOutgoing.addstr(str);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	Sleep(200);

	int pathtype = 0;
	if (m_bMidway)
		pathtype |= 0x04;
	if (m_bFull)
		pathtype |= 0x08;
	itoa(pathtype, szValue, 10);
	str = szValue;

	msgOutgoing.clear();
	msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdRobot));
	msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::SetPathPlanningRecipe);
	msgOutgoing.addtyp<uint8_t>((uint8_t)PathPlanningRecipe::PathType);
	msgOutgoing.addstr(str);
	msgOutgoing.send(router, ZMQ_DONTWAIT);

	Sleep(200);
}


void CVisionTesterDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此加入您的訊息處理常式程式碼和 (或) 呼叫預設值

	//zmq::multipart_t flowIncoming;
	//if (flowIncoming.recv(_pullFromVision, ZMQ_DONTWAIT)) {
	//	omlp::FlowId flowid = omlp::checkWorkflowType(flowIncoming);
	//	// received workflow to start path planning
	//	if (flowid == omlp::FlowId::VisionData)
	//		PostMessage(DETECTION_FINISH);
	//}

	zmq::multipart_t msgIncoming;
	if (msgIncoming.recv(router, ZMQ_DONTWAIT)) {
		std::string linkid = msgIncoming.popstr();
		omlp::MessageId msgtype = omlp::checkMessageType(msgIncoming);
		if (msgtype == omlp::MessageId::NotifyError) {
			omlp::ResultCode rc = (omlp::ResultCode)msgIncoming.poptyp<int8_t>();
			sErrorDescription = msgIncoming.popstr();
			if (omlp::makeIdentity(omlp::HeaderField::IdVision) == linkid)
				PostMessage(DETECTION_FAIL, (WPARAM)rc);
			else
				PostMessage(ROBOT_FAIL, (WPARAM)rc);
		}
		else if (msgtype == omlp::MessageId::NotifySoleContour) {
			PostMessage(DETECTION_FINISH);
			uint8_t lineid = msgIncoming.poptyp<uint8_t>();
			std::string soletypename = msgIncoming.popstr();
			double contourlength = ((double)(msgIncoming.poptyp<uint32_t>())) / 100;
			bool leftsole = msgIncoming.poptyp<uint8_t>() == 1 ? true : false;
			uint32_t count = msgIncoming.poptyp<uint32_t>();
			m_nPoints = (int)count;
			if (soletypename != "") {
				std::wstring wsoletypename(soletypename.begin(), soletypename.end());
				m_sSoleTypeName = wsoletypename.c_str();
			}
			else
				m_sSoleTypeName = L"";
			UpdateData(FALSE);
		}
		else if (msgtype == omlp::MessageId::NotifyCustomizedPathAlgorithmContour) {
			PostMessage(DETECTION_FINISH);
			uint8_t lineid = msgIncoming.poptyp<uint8_t>();
			uint32_t count = msgIncoming.poptyp<uint32_t>();
			m_nPoints = (int)count;
			UpdateData(FALSE);
		}
	}

	zmq::multipart_t flowIncoming2;
	if (flowIncoming2.recv(_pullFromRobot, ZMQ_DONTWAIT)) {
		omlp::FlowId flowid = omlp::checkWorkflowType(flowIncoming2);
		// received workflow to start path planning
		if (flowid == omlp::FlowId::ConveyorStandby)
			PostMessage(ROBOT_FINISH);
	}

	CDialogEx::OnTimer(nIDEvent);
}


void CVisionTesterDlg::OnBnClickedCheckSpikeNoiseSuppression()
{
	// TODO: 在此加入控制項告知處理常式程式碼

	UpdateData(TRUE);
}


void CVisionTesterDlg::OnBnClickedRadioInterpolation()
{
	// TODO: 在此加入控制項告知處理常式程式碼

	UpdateData(TRUE);
}


void CVisionTesterDlg::OnBnClickedRadioNeighborSubstitution()
{
	// TODO: 在此加入控制項告知處理常式程式碼

	UpdateData(TRUE);
}


void CVisionTesterDlg::OnEnKillfocusEditPoints()
{
	// TODO: 在此加入控制項告知處理常式程式碼

	UpdateData(TRUE);
}


void CVisionTesterDlg::OnEnKillfocusEditToeheel()
{
	// TODO: 在此加入控制項告知處理常式程式碼

	UpdateData(TRUE);
}


void CVisionTesterDlg::OnEnKillfocusEditViewHeight()
{
	// TODO: 在此加入控制項告知處理常式程式碼

	UpdateData(TRUE);
}


void CVisionTesterDlg::OnEnKillfocusEditFeatureThreshold()
{
	// TODO: 在此加入控制項告知處理常式程式碼

	UpdateData(TRUE);
}


void CVisionTesterDlg::OnEnKillfocusEditSmoothCoefficient()
{
	// TODO: 在此加入控制項告知處理常式程式碼

	UpdateData(TRUE);
}


void CVisionTesterDlg::OnEnKillfocusEditToeCutting()
{
	// TODO: 在此加入控制項告知處理常式程式碼

	UpdateData(TRUE);
}


std::string CVisionTesterDlg::generateJobId()
{
	time_t t = system_clock::to_time_t(system_clock::now());
	tm* tmNow = std::localtime(&t);
	ostringstream outstr;
	outstr << std::put_time(tmNow, "%Y%m%d%H%M%S");
	return outstr.str();
}


LRESULT CVisionTesterDlg::onDetectionFinish(WPARAM wParam, LPARAM lParam)
{
	AfxGetApp()->DoWaitCursor(0);

	MessageBox(_T("Vision Path Finish!!!"));

	if (m_bContinuously) {
		idxCur++;
		if (idxCur >= vFilenameList.size()) {
			GetDlgItem(IDC_BUTTON_DETECT)->EnableWindow(TRUE);
			GetDlgItem(IDC_CHECK_CONTINUOUSLY)->EnableWindow(TRUE);
			GetDlgItem(IDC_STATIC_DATA_PATH)->SetWindowText(L"Input Scanner Data: D:\\Temp\\*.ply");
			return 0;
		}

		CloudMeta meta;
		std::vector<Point3d> vPts;
		wchar_t wzData[MAX_PATH];
		bool stop = false;
		do {
			mbstowcs(wzData, vFilenameList[idxCur].c_str(), MAX_PATH);
			CString sFilename = wzData;
			CNextScannerDataDlg dlgData(sFilename);
			if (dlgData.DoModal() == IDCANCEL)
				stop = true;
			else {
				if (dlgData.m_bDetect) {
					vPts = PlyFile::readPlyToPoint3d(vFilenameList[idxCur], meta);
					GetDlgItem(IDC_STATIC_DATA_PATH)->SetWindowText(L"Input Scanner Data: " + sFilename);
					break;
				}
				else {
					idxCur++;
					if (idxCur >= vFilenameList.size())
						stop = true;
				}
			}
		} while (!stop);
		if (stop) {
			GetDlgItem(IDC_BUTTON_DETECT)->EnableWindow(TRUE);
			GetDlgItem(IDC_CHECK_CONTINUOUSLY)->EnableWindow(TRUE);
			GetDlgItem(IDC_STATIC_DATA_PATH)->SetWindowText(L"Input Scanner Data: D:\\Temp\\*.ply");
			return 0;
		}

		AfxGetApp()->DoWaitCursor(1);

		meta.datetime = generateJobId();
		zmq::multipart_t flowOutgoing;
		omlp::makeWork_ScanData(flowOutgoing, 0, false, meta, vPts);
		flowOutgoing.send(_pushToVision, ZMQ_DONTWAIT);
	}
	else {
		GetDlgItem(IDC_BUTTON_DETECT)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_CONTINUOUSLY)->EnableWindow(TRUE);
	}

	return 0;
}


LRESULT CVisionTesterDlg::onRobotFinish(WPARAM wParam, LPARAM lParam)
{
	MessageBox(_T("Robot Path Done!!!"));

	return 0;
}


LRESULT CVisionTesterDlg::onDetectionFail(WPARAM wParam, LPARAM lParam)
{
	AfxGetApp()->DoWaitCursor(0);

	omlp::ResultCode rc = (omlp::ResultCode)wParam;

	CString sMsg;
	if (rc == omlp::ResultCode::ERR_VisionFailedToGeneratePath)
		sMsg = _T("Can't generate path.");
	else if (rc == omlp::ResultCode::ERR_VisionSoleOverScanArea)
		sMsg = _T("Sole is out of scan area.");
	else if (rc == omlp::ResultCode::ERR_VisionSolePatternNotFound)
		sMsg = _T("Sole pattern is not found.");
	else if (rc == omlp::ResultCode::ERR_VisionConveyorLowerThanLimit)
		sMsg = _T("The conveyor plane is lower than the limit.");
	else if (rc == omlp::ResultCode::ERR_VisionConveyorPlaneNotFound)
		sMsg = _T("The conveyor plane is not found.");
	else if (rc == omlp::ResultCode::ERR_VisionCrashToProcess)
		sMsg = _T("Vision crashes.");
	else if (rc == omlp::ResultCode::ERR_VisionSolePatternNotMatch)
		sMsg = _T("The sole data can not match to patterns in database.");
	else if (rc == omlp::ResultCode::ERR_SettingFileNotExist) {
		sMsg = _T("The setting file does not exist.") + (CString)(_T(" - "));
		std::wstring wSettingFile(sErrorDescription.begin(), sErrorDescription.end());
		sMsg += wSettingFile.c_str();
	}
	else
		sMsg.Format(L"error code %d", (int)rc);

	sMsg = _T("Vision Failed - ") + sMsg;

	MessageBox(sMsg);

	GetDlgItem(IDC_BUTTON_DETECT)->EnableWindow(TRUE);
	GetDlgItem(IDC_CHECK_CONTINUOUSLY)->EnableWindow(TRUE);
	if (m_bContinuously)
		GetDlgItem(IDC_STATIC_DATA_PATH)->SetWindowText(L"Input Scanner Data: D:\\Temp\\*.ply");

	return 0;
}


LRESULT CVisionTesterDlg::onRobotFail(WPARAM wParam, LPARAM lParam)
{
	omlp::ResultCode rc = (omlp::ResultCode)wParam;

	CString sMsg;
	if (rc == omlp::ResultCode::ERR_CantProduceRobotPath)
		sMsg = _T("Can't generate robot path.");
	else if (rc == omlp::ResultCode::ERR_RobotInterfaceFailed)
		sMsg = _T("Robot interface failure.");
	else if (rc == omlp::ResultCode::ERR_RobotPathUploadingFailure)
		sMsg = _T("Robot path uploading failure.");
	else if (rc == omlp::ResultCode::ERR_RobotCrashToProcess)
		sMsg = _T("Robot crashes.");
	else
		sMsg.Format(L"error code %d", (int)rc);

	sMsg = _T("Robot Failed - ") + sMsg;

	MessageBox(sMsg);

	return 0;
}


void CVisionTesterDlg::OnBnClickedCheckFilterWideEdge()
{
	// TODO: 在此加入控制項告知處理常式程式碼

	UpdateData(TRUE);
}


void CVisionTesterDlg::OnBnClickedCheckAutoPoints()
{
	// TODO: Add your control notification handler code here

	UpdateData(TRUE);

	if (m_bAutoPoints)
		GetDlgItem(IDC_EDIT_POINTS)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_EDIT_POINTS)->EnableWindow(TRUE);
}


void CVisionTesterDlg::OnBnClickedCheckThinSole()
{
	// TODO: Add your control notification handler code here

	UpdateData(TRUE);
}


void CVisionTesterDlg::OnEnKillfocusEditFirstRadius()
{
	// TODO: Add your control notification handler code here

	UpdateData(TRUE);
}


void CVisionTesterDlg::OnEnKillfocusOtherRadius()
{
	// TODO: Add your control notification handler code here

	UpdateData(TRUE);
}


void CVisionTesterDlg::OnBnClickedCheckAutoViewHeight()
{
	// TODO: Add your control notification handler code here

	UpdateData(TRUE);

	if (m_bAutoViewHeight)
		GetDlgItem(IDC_EDIT_VIEW_HEIGHT)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_EDIT_VIEW_HEIGHT)->EnableWindow(TRUE);
}


void CVisionTesterDlg::OnCbnSelchangeComboEdgeSprayCycles()
{
	// TODO: Add your control notification handler code here

	UpdateData(TRUE);
}


void CVisionTesterDlg::OnBnClickedCheckAutoCutting()
{
	// TODO: Add your control notification handler code here

	UpdateData(TRUE);

	if (m_bAutoCutting)
		GetDlgItem(IDC_EDIT_TOE_CUTTING)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_EDIT_TOE_CUTTING)->EnableWindow(TRUE);
}


void CVisionTesterDlg::OnEnKillfocusEditSprayAngle()
{
	// TODO: Add your control notification handler code here

	UpdateData(TRUE);
}


void CVisionTesterDlg::OnBnClickedCheckAutoSprayAngle()
{
	// TODO: Add your control notification handler code here

	UpdateData(TRUE);

	if (m_bAutoSprayAngle)
		GetDlgItem(IDC_EDIT_SPRAY_ANGLE)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_EDIT_SPRAY_ANGLE)->EnableWindow(TRUE);
}


void CVisionTesterDlg::OnEnKillfocusEditFirstHeight()
{
	// TODO: Add your control notification handler code here

	UpdateData(TRUE);
}


void CVisionTesterDlg::OnEnKillfocusEditOtherHeight()
{
	// TODO: Add your control notification handler code here

	UpdateData(TRUE);
}


void CVisionTesterDlg::OnBnClickedCheckMidWay()
{
	// TODO: Add your control notification handler code here

	UpdateData(TRUE);
}


void CVisionTesterDlg::OnBnClickedCheckFull()
{
	// TODO: Add your control notification handler code here

	UpdateData(TRUE);
}


void CVisionTesterDlg::OnBnClickedCheckContinuously()
{
	// TODO: Add your control notification handler code here

	UpdateData(TRUE);

	if (m_bContinuously)
		GetDlgItem(IDC_STATIC_DATA_PATH)->SetWindowText(L"Input Scanner Data: D:\\Temp\\*.ply");
	else
		GetDlgItem(IDC_STATIC_DATA_PATH)->SetWindowText(L"Input Scanner Data: D:\\Temp\\scanner_data.ply");
}


void CVisionTesterDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here

	ShowWindow(SW_HIDE);

	zmq::multipart_t msgOutgoing;

	switch (m_state) {
	case StateType::None:
		msgOutgoing.clear();
		msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
		msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::CommandLink);
		msgOutgoing.send(router, ZMQ_DONTWAIT);

		msgOutgoing.clear();
		msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdRobot));
		msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::CommandLink);
		msgOutgoing.send(router, ZMQ_DONTWAIT);

		Sleep(1000);

		msgOutgoing.clear();
		msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
		msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::CommandUnlink);
		msgOutgoing.send(router, ZMQ_DONTWAIT);

		msgOutgoing.clear();
		msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdRobot));
		msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::CommandUnlink);
		msgOutgoing.send(router, ZMQ_DONTWAIT);
		break;
	case StateType::Run:
		msgOutgoing.clear();
		msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
		msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::CommandStop);
		msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::Tone::Soft);
		msgOutgoing.send(router, ZMQ_DONTWAIT);

		msgOutgoing.clear();
		msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdRobot));
		msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::CommandStop);
		msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::Tone::Soft);
		msgOutgoing.send(router, ZMQ_DONTWAIT);

		Sleep(1000);
	case StateType::Ready:
		msgOutgoing.clear();
		msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
		msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::CommandReset);
		msgOutgoing.send(router, ZMQ_DONTWAIT);

		msgOutgoing.clear();
		msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdRobot));
		msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::CommandReset);
		msgOutgoing.send(router, ZMQ_DONTWAIT);

		Sleep(1000);

		msgOutgoing.clear();
		msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
		msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::CommandUnlink);
		msgOutgoing.send(router, ZMQ_DONTWAIT);

		msgOutgoing.clear();
		msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdRobot));
		msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::CommandUnlink);
		msgOutgoing.send(router, ZMQ_DONTWAIT);

		break;
	}

	Sleep(1500);

	CDialogEx::OnOK();
}


void CVisionTesterDlg::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here

	ShowWindow(SW_HIDE);

	zmq::multipart_t msgOutgoing;

	switch (m_state) {
	case StateType::None:
		msgOutgoing.clear();
		msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
		msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::CommandLink);
		msgOutgoing.send(router, ZMQ_DONTWAIT);

		msgOutgoing.clear();
		msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdRobot));
		msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::CommandLink);
		msgOutgoing.send(router, ZMQ_DONTWAIT);

		Sleep(1000);

		msgOutgoing.clear();
		msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
		msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::CommandUnlink);
		msgOutgoing.send(router, ZMQ_DONTWAIT);

		msgOutgoing.clear();
		msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdRobot));
		msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::CommandUnlink);
		msgOutgoing.send(router, ZMQ_DONTWAIT);
		break;
	case StateType::Run:
		msgOutgoing.clear();
		msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
		msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::CommandStop);
		msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::Tone::Soft);
		msgOutgoing.send(router, ZMQ_DONTWAIT);

		msgOutgoing.clear();
		msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdRobot));
		msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::CommandStop);
		msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::Tone::Soft);
		msgOutgoing.send(router, ZMQ_DONTWAIT);

		Sleep(1000);
	case StateType::Ready:
		msgOutgoing.clear();
		msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
		msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::CommandReset);
		msgOutgoing.send(router, ZMQ_DONTWAIT);

		msgOutgoing.clear();
		msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdRobot));
		msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::CommandReset);
		msgOutgoing.send(router, ZMQ_DONTWAIT);

		Sleep(1000);

		msgOutgoing.clear();
		msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdVision));
		msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::CommandUnlink);
		msgOutgoing.send(router, ZMQ_DONTWAIT);

		msgOutgoing.clear();
		msgOutgoing.pushstr(omlp::makeIdentity(omlp::HeaderField::IdRobot));
		msgOutgoing.addtyp<uint8_t>((uint8_t)omlp::MessageId::CommandUnlink);
		msgOutgoing.send(router, ZMQ_DONTWAIT);

		break;
	}

	Sleep(1500);

	CDialogEx::OnCancel();
}


void CVisionTesterDlg::OnBnClickedCheckSwitchSoleType()
{
	// TODO: Add your control notification handler code here

	UpdateData(TRUE);

	if (m_bSwitchSoleType) {
		GetDlgItem(IDC_CHECK_USE_AUTO_IF_NOT_FOUND)->EnableWindow(TRUE);
		m_cbSoleType.EnableWindow(FALSE);
		m_chkZonePath.EnableWindow(FALSE);
	}
	else {
		m_chkZonePath.EnableWindow(TRUE);
		if (m_bZonePath)
			m_cbSoleType.EnableWindow(TRUE);
		else
			m_cbSoleType.EnableWindow(FALSE);
	}
}


void CVisionTesterDlg::OnBnClickedCheckUseAutoIfNotFound()
{
	// TODO: Add your control notification handler code here

	UpdateData(TRUE);
}


void CVisionTesterDlg::OnBnClickedRadioInTheFront()
{
	// TODO: Add your control notification handler code here

	UpdateData(TRUE);
}


void CVisionTesterDlg::OnBnClickedRadioInTheBack()
{
	// TODO: Add your control notification handler code here

	UpdateData(TRUE);
}


void CVisionTesterDlg::OnBnClickedCheckZonePath()
{
	// TODO: Add your control notification handler code here

	UpdateData(TRUE);

	if (m_bZonePath)
		m_cbSoleType.EnableWindow(TRUE);
	else
		m_cbSoleType.EnableWindow(FALSE);
}


void CVisionTesterDlg::OnCbnSelchangeComboSoleType()
{
	// TODO: Add your control notification handler code here

	UpdateData(TRUE);
}


void CVisionTesterDlg::OnCbnSelchangeComboCustomizedPathAlg()
{
	// TODO: Add your control notification handler code here

	UpdateData(TRUE);
}


void CVisionTesterDlg::OnBnClickedCheckCustomizedPathAkg()
{
	// TODO: Add your control notification handler code here

	UpdateData(TRUE);

	if (m_bCustomizedPathAlg)
		m_cbCustomizedPathAlg.EnableWindow(TRUE);
	else
		m_cbCustomizedPathAlg.EnableWindow(FALSE);
}
