// NextScannerDataDlg.cpp : implementation file
//

#include "stdafx.h"
#include "VisionTester.h"
#include "NextScannerDataDlg.h"
#include "afxdialogex.h"


// CNextScannerDataDlg dialog

IMPLEMENT_DYNAMIC(CNextScannerDataDlg, CDialogEx)

CNextScannerDataDlg::CNextScannerDataDlg(CString sFilename, CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_DIALOG_NEXT_SCANNER_DATA, pParent)
	, m_sFilename(sFilename)
{

}

CNextScannerDataDlg::~CNextScannerDataDlg()
{
}

void CNextScannerDataDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_STATIC_SCANNER_DATA_FILENAME, m_sFilename);
}


BEGIN_MESSAGE_MAP(CNextScannerDataDlg, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON_NEXT_DETECT, &CNextScannerDataDlg::OnBnClickedButtonNextDetect)
	ON_BN_CLICKED(IDC_BUTTON_SKIP, &CNextScannerDataDlg::OnBnClickedButtonSkip)
END_MESSAGE_MAP()


// CNextScannerDataDlg message handlers


BOOL CNextScannerDataDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  Add extra initialization here

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}


void CNextScannerDataDlg::OnBnClickedButtonNextDetect()
{
	// TODO: Add your control notification handler code here

	m_bDetect = TRUE;

	CDialogEx::OnOK();
}


void CNextScannerDataDlg::OnBnClickedButtonSkip()
{
	// TODO: Add your control notification handler code here

	CDialogEx::OnOK();
}
