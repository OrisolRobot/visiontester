/*
	File:
		Robot.h
	Description:
		Abstract base class to communicate with robot controller
	Copyright:
		Orisol Asia Ltd.
*/
#pragma once
#include <string>
#include <Eigen/Core>

// path planning parameters from user recipe selection
enum class PathPlanningRecipe : uint8_t
{
	PathType,
	SprayRadius1st,
	SprayRadiusOther,
	ToolAwayDistance1st,
	ToolAwayDistanceOther,
	SprayAngle,
	OverSprayDistance,
	OffsetX1,
	OffsetY1,
	OffsetZ1,
	OffsetRx1,
	OffsetRy1,
	OffsetRz1,
	OffsetX2,
	OffsetY2,
	OffsetZ2,
	OffsetRx2,
	OffsetRy2,
	OffsetRz2,
	FullSprayingMode,	// 0: Orisol mode, 1: Chiayi mode
	AutoSprayAngle,
	EdgeSprayCycles // 0: auto, 1~5 cycles
};

// bitwise combination values for PathType of recipe
enum class PathType : uint8_t
{
	OneLoop   = 0x01,
	TwoLoop   = 0x02,
	MiddleWay = 0x04,
	Fill      = 0x08
};

// Message Parameter: robot moving speed levels (1 byte)
enum class RobotSpeed : uint8_t
{
	Lowest = 0,
	Lower = 1,
	Middle = 2,
	Higher = 3,
	Highest = 4,
	NotExist
};

// Message Parameter: robot action types (1 byte)
enum class RobotAction : uint8_t
{
	NoAction = 0,
	MoveWithoutSpray = 1,
	Normal
};

enum class RobotMove : uint8_t
{
	Pause = 0,
	Continue = 1
};

enum class SoakNozzle : uint8_t
{
	On = 0,
	Off = 1
};

enum class MaintainGun : uint8_t
{
	On = 0,
	Off = 1
};

enum class RobotMeasureGunAmountType : uint8_t
{
	AirDrain = 0,
	GlueWeight = 1
};

// Message Parameter: plasma control type
enum class PlasmaControlType : uint8_t
{
	Air = 0,
	Plasma = 1,
	Run = 2
};

enum class PlasmaControlIO : uint8_t
{
	On = 0,
	Off = 1
};

enum class CustomizedPathAlgorithmType : int32_t
{
	None = -1,
	IA_Group = 0,
	Crocs_LiteRide = 1,
	SoleAdded_TwoArea = 2,
	MIZUNO_Wave=3
};

enum class RobotResult
{
	ERR_PosWrite=-23,
	ERR_GoWrite = -22,
	ERR_GoRead=-21,
	ERR_RegisterRead = -20,
	ERR_RegisterWrite=-19,
	ERR_RemoveFile = -18,
	ERR_PosDataFailed=-17,
	ERR_PosRead=-16,
	ERR_RealWrite=-15,
	ERR_DoubleWrite=-14,
	ERR_ByteWrite=-13,
	ERR_IORead=-12,
	ERR_RealRead=-11,
	ERR_DoubleRead=-10,
	ERR_ByteRead=-9,
	ERR_Command=-8,
	ERR_PareSetup = -7,
	ERR_RobotStop=-6,
	ERR_IOWrite=-5,
	ERR_JobSelect=-4,
	ERR_JobStart=-3,
	ERR_LoadFile=-2,
	ERR_Link=-1,
	NoError = 0
};

class Robot
{
public:
	virtual ~Robot() {}
	virtual RobotResult GetVersion(std::string & version) = 0;
	virtual RobotResult Initialize(const char * robot_ip, int robot_model) = 0;
	virtual RobotResult RobotConnect() = 0;
	virtual RobotResult RobotDisconnect() = 0;
	virtual RobotResult RobotStart() = 0;
	virtual RobotResult RobotPause() = 0;
	virtual RobotResult RobotContinue() = 0;
	virtual RobotResult RobotStop() = 0;
	virtual RobotResult RobotLoadFile(std::string str_name) = 0;
	virtual RobotResult RobotPareSetup(int index, double number) = 0;
	virtual RobotResult RobotCommand(int index) = 0;
	virtual RobotResult RobotDeleteAllJob()=0;
	virtual RobotResult RobotReturnHome()=0;
	virtual RobotResult RobotDataRead(int index, unsigned short instance, float &read_data)=0;
	virtual RobotResult RobotDataWrite(int index, unsigned short instance, float write_data)=0;
	virtual RobotResult GetRobotPosition(Eigen::MatrixXd & POS)=0;
	virtual RobotResult GetRobotPulse(Eigen::MatrixXd & POS) = 0;

	virtual RobotResult SetPosVar(unsigned short instance, Eigen::MatrixXd POS)=0;
	virtual RobotResult RobotCleanGun()=0;
	virtual RobotResult LineSpraySecondLoop(float Height2) = 0;
	virtual RobotResult LineSprayFirstLoop(float Height1) = 0;
};